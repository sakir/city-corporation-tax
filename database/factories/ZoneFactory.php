<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Zone;
use Faker\Generator as Faker;

$factory->define(Zone::class, function (Faker $faker) {

    $google_map_data = array(
                                'lat' => $faker->latitude,
                                'lon' => $faker->longitude,
                                'zoom' => 14
                             );
    $google_map_data = json_encode($google_map_data);

    return [
        'name' => $faker->streetName,
        'google_map_data' => $google_map_data,
        'created_by' => 1,
        'last_updated_by' => 1,
        'status' => rand(0,1)
    ];
});
