<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    $google_map_data = array(
                                'lat' => $faker->latitude,
                                'lon' => $faker->longitude,
                                'zoom' => 14
                             );
    $google_map_data = json_encode($google_map_data);
    $opening_amounts = array(0,100,200,300,400,500,600,700,800,900);
    $is_reseller = array(0,0,0,0,0,0,0,1);
    $is_reseller = $is_reseller[rand(0,7)];
    $status = array('active','active','active','inactive','pending');
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber . ',' . $faker->phoneNumber,
        'email' => $faker->unique()->email,
        'nid' => $faker->unique()->numberBetween(10000000000000000, 99999999999999999),
        'photo' => 'profile-pic.jpg',
        'address' => $faker->address,
        'google_map_data' => $google_map_data,
        'location_photo' => 'customer-home-location-'. rand(1,3).'.jpg',
        'zone_id' => rand(1, 30),
        'occupation' => $faker->sentence(2, true),
        'opening_amount' => $opening_amounts[rand(0,9)],
        'opening_amount_due' => $opening_amounts[rand(0,5)],
        'bill_amount' => 1000,
        'connection_srv_bill' => $opening_amounts[rand(0,9)],
        'connection_srv_bill_due' => $opening_amounts[rand(0,5)],
        'connection_date' => $faker->dateTimeBetween('-4 years', '+3 months'),
        'package_id' => rand(1, 4),
        'discount_amount' => $opening_amounts[rand(0,3)],
        'ip' => $faker->ipv4,
        'is_reseller' => 0,
        'reseller_id' => 0,
        'created_by' => 1,
        'status' => $status[rand(0,3)]
    ];
});
