<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {

    $google_map_data = array(
        'lat' => $faker->latitude,
        'lon' => $faker->longitude,
        'zoom' => 14
    );
    $google_map_data = json_encode($google_map_data);

    return [
        'user_id' => User::all()->random()->id,
        'phone' => $faker->phoneNumber,
        'nid' => $faker->unique()->numberBetween(10000000000000000, 99999999999999999),
        'photo' => 'profile-pic.jpg',
        'address' => $faker->address,
        'google_map_data' => $google_map_data,
        'facebook' => $faker->address,
        'skype' => $faker->address,
        'whatsapp' => $faker->address,
        'imo' => $faker->address,
        'more_detail' => $faker->text(rand(20,100)),
        'zone_id' => rand(1, 30),
        'joining_date' => $faker->dateTimeBetween('-4 years', 'now'),
        'designation' => 'General Staff',
        'created_by' => 1,
        'status' => rand(0,1)
    ];
});
