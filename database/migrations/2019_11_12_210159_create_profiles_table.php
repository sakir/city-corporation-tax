<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('phone',128);
            $table->string('nid',128);
            $table->string('photo');
            $table->string('address');
            $table->text('google_map_data');
            $table->string('facebook', 150);
            $table->string('skype', 150);
            $table->string('whatsapp', 150);
            $table->string('imo', 150);
            $table->text('more_detail');
            $table->unsignedBigInteger('zone_id');
            $table->date('joining_date');
            $table->string('designation',50);
            $table->unsignedBigInteger('created_by');
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
