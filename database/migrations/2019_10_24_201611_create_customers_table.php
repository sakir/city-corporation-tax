<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',128);
            $table->string('phone',128);
            $table->string('email',128);
            $table->string('nid',28);
            $table->string('photo');
            $table->string('address');
            $table->text('google_map_data');
            $table->string('location_photo');
            $table->unsignedBigInteger('zone_id');
            $table->string('occupation',128);
            $table->decimal('opening_amount', 10, 2);
            $table->decimal('opening_amount_due', 10, 2);
            $table->decimal('bill_amount', 10, 2);
            $table->decimal('connection_srv_bill', 10, 2);
            $table->decimal('connection_srv_bill_due', 10, 2);
            $table->dateTime('connection_date');
            $table->unsignedBigInteger('package_id');
            $table->decimal('discount_amount', 10, 2);
            $table->string('ip',30);
            $table->boolean('is_reseller')->default(0);
            $table->unsignedBigInteger('reseller_id')->default(0);
            $table->unsignedBigInteger('created_by');
            $table->string('status',30);
            $table->timestamps();

			$table->foreign('zone_id')->references('id')->on('zones');
			$table->foreign('package_id')->references('id')->on('packages');
			$table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
