<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pkg_title', 60);
            $table->string('speed', 36);
            $table->decimal('price',  8, 2);
            $table->string('duration', 36);
            $table->text('description');
            $table->string('banner');
            $table->smallInteger('order_sl');
            $table->unsignedBigInteger('created_by');
            $table->boolean('status')->default(1);
            $table->timestamps();

			$table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
