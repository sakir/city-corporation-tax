


Make Controllers:
php artisan make:controller Dashboard/ZoneController --resource
php artisan make:controller Dashboard/EmployeesController --resource





Migration database tables: 
php artisan make:migration create_zones_table --create=zones
php artisan make:migration create_employees_table --create=employees
php artisan make:migration create_packages_table --create=packages
php artisan make:migration create_customers_table --create=customers

php artisan make:migration create_profiles_table --create=profiles




php artisan make:seeder ZonesTableSeeder
php artisan make:seeder ProfilesTableSeeder
php artisan make:seeder PackagesTableSeeder
php artisan make:seeder CustomersTableSeeder



php artisan make:factory ZoneFactory
php artisan make:factory ProfilesFactory
php artisan make:factory PackageFactory
php artisan make:factory CustomerFactory



php artisan db:seed --class=ZonesTableSeeder
php artisan db:seed --class=ProfilesTableSeeder
php artisan db:seed --class=PackagesTableSeeder
php artisan db:seed --class=CustomersTableSeeder


php artisan make:migration create_resellers_and_customers_table --create=resellers_and_customers



php artisan migrate:refresh --path=/database/migrations/2019_10_24_201611_create_customers_table.php