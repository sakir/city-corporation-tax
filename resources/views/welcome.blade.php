@extends('layouts.general')

@section('content')



  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-5 mb-md-0">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
          <h2 style="color: #6059f6;">Gazipur City Corporation</h2>
          <h3>Tongi, Zone: 1 - 8</h3>
          <h6>Tax managment application </h6>
          <a class="button button-hero mt-4" href="login">Dashboard login</a>
        </div>
        <div class="col-7">
          <div class="search-wrapper">
            <h3>Search Holding</h3>
				
            <form class="" action="#">
              <div class="form-row">
					<div class="col">
						<label class="">Zone Select</label>
						<select class="form-control" name="">
							<option value=""> -- Zone Select -- </option>
							<option value="1">Zone 1</option>
							<option value="2">Zone 2</option>
							<option value="3">Zone 3</option>
							<option value="4">Zone 4</option>
							<option value="5">Zone 5</option>
							<option value="6">Zone 6</option>
							<option value="7">Zone 7</option>
							<option value="8">Zone 8</option>
						</select>
					</div>
					<div class="col">
						<label class="">Ward Select</label>
						<select class="form-control" name="">
							<option value=""> -- Ward Select -- </option>
							<option value="1">Ward 1</option>
							<option value="2">Ward 2</option>
							<option value="3">Ward 3</option>
							<option value="4">Ward 4</option>
							<option value="5">Ward 5</option>
							<option value="6">Ward 6</option>
							<option value="7">Ward 7</option>
							<option value="8">Ward 8</option>
						</select>
					</div>
              </div>
				  <br>
              <div class="form-row">
						<div class="col">
							<label class="">Holding Number</label>
							<input type="text" class="form-control text-center form-control-lg" placeholder="#">
						</div>
						<div class="col">
							<label class="">&nbsp; </label>
							<button class="button btn btn-block border-0" type="submit"><i class="ti-search"></i> Search </button>
						</div>
              </div>
                  
					<br>
                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br><br><br>

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="http://gazipur-citycorpo.lara/dashboard/images/favicon.png">
                    <img src="http://gazipur-citycorpo.lara/dashboard/images/gazipur-city-zones.jpg" width="400px">
                    <p>গাজীপুর সিটি কর্পোরেশন </p>
                    <p>পূর্ণাঙ্গ  <b>কর ব্যাবস্থাপনা</b> সফটওয়্যার</p>
                </div>
            </div>
        </div>
		  
@endsection
