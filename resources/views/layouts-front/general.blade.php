<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="icon" href="{{ url('/') }}/front/img/Fevicon.png" type="image/png">


	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}front/img/favicon.png">

	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/') }}front/img/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/') }}front/img/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/') }}front/img/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/') }}front/img/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/') }}front/img/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/') }}front/img/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/') }}front/img/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/') }}front/img/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/') }}front/img/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/') }}front/img/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/') }}front/img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/') }}front/img/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}front/img/favicons/favicon-16x16.png">
	<link rel="manifest" href="{{ asset('/') }}front/img/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('/') }}front/img/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">



  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/linericon/style.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/owl-carousel/owl.carousel.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/flat-icon/font/flaticon.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/vendors/nice-select/nice-select.css">

  <link rel="stylesheet" href="{{ url('/') }}/front/css/style.css">
  <link rel="stylesheet" href="{{ url('/') }}/front/css/custom.css">
</head>
<body class="bg-shape">

  <!--================ Header Menu Area start =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container box_1620">
          <a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item active"><a class="nav-link" href="{{ url('/') }}">Home</a></li> 
              <li class="nav-item"><a class="nav-link" href="{{ url('/') }}front/all_holdings">All holdings</a></li> 
              
            </ul>

            <div class="nav-right text-center text-lg-right py-4 py-lg-0">
              <a class="button" href="<?= url('/login') ?>">Dashboard login</a>
            </div>
          </div> 
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->



		@yield('content')



  <!-- ================ start footer Area ================= -->
  <footer class="footer-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-6 col-sm-6">
          <div class="single-footer-widget mail-chimp">
            <img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" style="width: 150px;" class="">
          </div>
        </div>				
        <div class="col-lg-3  col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>About Agency</h6>
            <p>
              The world has become so fast paced that people don’t want to stand by reading a page of information to be  they would much rather look at a presentation and understand
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>Navigation Links</h6>
            <div class="row">
              <div class="col">
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Feature</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">Portfolio</a></li>
                </ul>
              </div>
              <div class="col">
                <ul>
                  <li><a href="#">Team</a></li>
                  <li><a href="#">Pricing</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>										
            </div>							
          </div>
        </div>							
        <div class="col-lg-3  col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>Newsletter</h6>
            <p>
              For business professionals caught between high OEM price and mediocre print and graphic output.									
            </p>								
            <div id="mc_embed_signup">
              <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscription relative">
                <div class="input-group d-flex flex-row">
                  <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                  <button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
                </div>									
                <div class="mt-10 info"></div>
              </form>
            </div>
          </div>
        </div>		
      </div>

      <div class="footer-bottom">
        <div class="row align-items-center">
          <p class="col-lg-8 col-sm-12 footer-text m-0 text-center text-lg-left"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          <div class="col-lg-4 col-sm-12 footer-social text-center text-lg-right">
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-dribbble"></i></a>
            <a href="#"><i class="fab fa-behance"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- ================ End footer Area ================= -->




  <script src="{{ url('/') }}/front/vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="{{ url('/') }}/front/vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="{{ url('/') }}/front/vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="{{ url('/') }}/front/vendors/nice-select/jquery.nice-select.min.js"></script>
  <script src="{{ url('/') }}/front/js/jquery.ajaxchimp.min.js"></script>
  <script src="{{ url('/') }}/front/js/mail-script.js"></script>
	<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script src="{{ url('/') }}/front/js/skrollr.min.js"></script>
  
		@yield('js_script')

	<script>
		 (function (window, document) {
			  var loader = function () {
					var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
					script.src = "https://sandbox.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7);
					tag.parentNode.insertBefore(script, tag);
			  };

			  window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
		 })(window, document);
	</script>
</body>
</html>