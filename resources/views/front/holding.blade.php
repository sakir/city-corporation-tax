@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
        </div>
        <div class="col-8">

          <div class="search-wrapper">
					<h2 class="card-title text-center"> জোনঃ <b>{{ BngConv::en2bn($selected_zone_no) }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{{ BngConv::en2bn($selected_ward_no) }}</b></h2>

					<!-- ===== Alert Message showing ====== -->
					@if(Session::has('success'))
						 <p class="alert alert-{{ 'success' }}">{!! Session::get('success') !!}</p>
					@endif


          </div>
        </div>
      </div>

      <div class="row card">
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">নির্দিস্ট হোল্ডিং খুঁজুন</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">সকল হোল্ডিং সমূহের তালিকা</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <div class="row">
                        <div class="col">
                            <label for="area_id">মহল্লা: </label>
                            <select name="area_id" id="area_id" class="form-control">
                                <option> -- Select -- </option>
                                @foreach( $areas as $ar_k => $ar_v )
                                    <option value="{{ $ar_v->area_id }}" >{{ $ar_v->area_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="road_id">রোড /  সড়ক নাম: </label>
                            <select name="road_id" id="road_id" class="form-control">
                                <option> -- Select -- </option>
                                @foreach( $roads as $rd_k => $rd_v )
                                    <option value="{{ $rd_v->road_id }}" >{{ $rd_v->road_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="road_id">হোল্ডিং: </label>
                            <input type="text" class="form-control" name="holding_id">
                        </div>
                        <div class="col">
                            <label for="road_id"> &nbsp; </label>
                            <button class="btn btn-block btn-primary" type="submit"> Search </button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <br>
                    <table id="holdings_table" data-select="" class="table table-striped table-hover col_4_right col_5_right col_6_right col_1ast_right" style="width: 100%;">
                        <thead>
									<tr class="bg-info text-white">
										 <th>SL</th>
										 <th>হোল্ডিং</th>
										 <th>মালিকের নাম</th>
										 <th>সম্পদ-টাকা</th>
										 <th><i class="fa fa-percent" aria-hidden="true"></i></th>
										 <th>ওয়ার্ড-জোন</th>
										 <th></th>
									</tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
									<tr class="bg-info text-white">
										 <th>SL</th>
										 <th>হোল্ডিং</th>
										 <th>মালিকের নাম</th>
										 <th>সম্পদ-টাকা</th>
										 <th><i class="fa fa-percent" aria-hidden="true"></i></th>
										 <th>ওয়ার্ড-জোন</th>
										 <th></th>
									</tr>
                        </tfoot>
                    </table>

                </div>
            </div>

        </div>
      </div>

    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection



@section('js_script')

	@php $dt_ajax_action = URL('/') . '/dt-ajax-all-holdings/';  @endphp
	@if( $selected_zone_no != '' && $selected_ward_no == '' )
		@php $dt_ajax_action = URL('/') . '/dt-ajax-all-holdings/' . $selected_zone_no;  @endphp
	@elseif( $selected_zone_no != '' && $selected_ward_no != '')
		@php $dt_ajax_action = URL('/') . '/dt-ajax-all-holdings/' . $selected_zone_no . '/' . $selected_ward_no;  @endphp
	@endif
	
    <script>
        //    alert(url + "settings/dt-ajax-all-wards);
        function load_holdings(tbl_ID) {
				
            var data = [
                {"data": "sl"},
                {"data": "holding_no"},
                {"data": "owner_name"},
                {"data": "yearly_amount"},
                {"data": "yearly_percentage"},
                {"data": "ward_zone"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = '{{ $dt_ajax_action ?? '' }}';

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_ward_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this ward!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_holdings('#holdings_table');

    </script>


@endsection
