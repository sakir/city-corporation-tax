@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
        </div>
        <div class="col-8">
					
          <div class="search-wrapper">
					<h2 class="card-title text-center"> জোনঃ <b>{{ BngConv::en2bn($holding->zone) }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{{ BngConv::en2bn($holding->ward_number) }}</b></h2>

					<!-- ===== Alert Message showing ====== -->
					@if(Session::has('success'))
						 <p class="alert alert-{-{ 'success' }}">{!! Session::get('success') !!}</p>
					@endif

				
          </div>
        </div>
      </div>
		
		
				<!--
					<pre>
					<?php // print_r($holding); ?>
					</pre>
				-->
					<div class="row">
							<div class="col-8" >
				
									<div class="row">
										<div class="col">
											<label for="area_id">মহল্লা: </label>
											<strong class="form-control font-weight-bold"> {{ $holding->area_name }}  </strong>
										</div>
										<div class="col">
											<label for="road_id">রোড /  সড়ক নাম: </label>
											<strong class="form-control font-weight-bold"> {{ $holding->road_name }} </strong>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-4">
											<div class="form-group">
												<label for="holding_no">হোল্ডিং নং</label>
												<strong class="form-control form-control-lg text-center font-weight-bold border border-dark" > {{ $holding->zone }} {{ $holding->ward_number }} {{ $holding->holding_no }}</strong>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col">
											<label for="owner_name">মালিকের নাম: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->owner_name }}</strong>
										</div>
										<div class="col">
											<label for="owner_fathers_name">মালিকের পিতার নাম: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->owner_fathers_name }}</strong>
										</div>
									</div>
									<br>
									<div class="form-row">
										<div class="col">
											<label for="phone">ফোন: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->phone }}</strong>
										</div>
										<div class="col">
											<label for="email">ইমেইল: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->email }}</strong>
										</div>
									
										<div class="col">
											<label for="nid">জাতীয় পরিচয় পত্র আইডি : </label>
											  <strong class="form-control font-weight-bold">{{ $holding->nid }}</strong>
										</div>
									</div>
									<br>

									<label for="description">আরো বিস্তারিত/বিবরনঃ  </label><br>
									<div class="border border-secondary p-2">
										<strong class=""> {{ $holding->description }}</strong>
									</div>
									<br>
									
									<!--
									<button class="btn btn-block btn-lg btn-primary"> Submit to Save </button>
									-->
									
									<?php 
									/*
									echo '<pre>';
										print_r($holding);
									echo '</pre>'; 
									*/
									?>
								
								
						</div>
				
				<div class="col-4" >
			
						<h3>ম্যাপ লোকেশন: </h3>
						<hr>
						<div class="form-row">
						  <div class="col-5">
								<label for="latitude">Latitude</label>
								<input type="text" id="latitude" name="latitude"
										 class="form-control form-control-sm code_text"
										 placeholder="#Latitude" value="" readonly>
						  </div>
						  <div class="col-5">
								<label for="longitude">Longitude</label>
								<input type="text" id="longitude" name="longitude"
										 class="form-control form-control-sm code_text"
										 placeholder="#Longitude" readonly>
						  </div>
						  <div class="col-2">
								<label for="zoom">Zoom</label>
								<input type="text" id="zoom" name="zoom" class="form-control form-control-sm code_text"
										 placeholder="#Zoom"
										 readonly>
						  </div>
					 </div>
					 <div id="map_canvas" style="width: 100%; height: 400px;"></div>
					<hr>
				</div>
			</div>
			<div class="row">

				<table class="table table-striped ">
				@foreach( $fyear_asset_records as $fyear_records_k => $fyear_records_v )
					
						<tr>
							<td>
								<p class="h3 border border-dark text-center p-2">{{ $fyear_records_v['fyear'] }}</p>
								
								<p class="h5"> মোট ধার্যকৃত ট্যেক্সঃ <span class="badge badge-primary">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount']) }}</span> টাকা</p>
								<p class="h6"> মোট পরিশোধিত <span class="badge badge-success">{{ BngConv::en2bn($fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা</p>
								<p class="h6"> মোট অপরিশোধিত <span class="badge badge-danger">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount'] - $fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা <a href="#" class="btn btn-outline-primary btn-sm">অনলাইনে পরিশোধ করুন</a></p> 
								<div class="progress  bg-danger">
								  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{ $fyear_records_v['payment_percentage'] }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<br>
								@if( count($fyear_records_v['fyear_tax_payment_records']) )
								<table class="table table-sm">
									<thead>
										<tr>
											<th colspan="2">ট্যেক্স পরিশোধ তালিকাঃ  </th>
										</tr>
										<tr>
											<th>সময়</th>
											<th>টাকা</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $fyear_records_v['fyear_tax_payment_records'] as $paid_k => $paid_v )
											<tr>
												<td><em>{{ $paid_v->payment_datetime }} </em> <span class="badge badge-secondary">{{ $paid_v->payment_by }} </span> </td>
												<td> {{ BngConv::en2bn(round($paid_v->payment_amount*1, 2)) }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
								
								@endif
							</td>
							<td>
								@foreach( $fyear_records_v['asset_records'] as $asset_k => $asset_v )
								
									<div class="card border mb-2">
									  <div class="card-body">
											<p class="h6">
												<span class="badge badge-secondary">{{ $asset_v->asset_category_title }}</span> | 
												<span class="badge badge-secondary">{{ $asset_v->asset_cat_class_title }}</span> | 
												<span class="badge badge-secondary">{{ $asset_v->asset_cat_using_title }}</span>
											</p>
											<p class="h6"> 
												স্কয়ার ফিট <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->square_feet) }}</span> | 
												প্রতি স্কয়ার ফিট মুল্যমান :  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->squarefeet_rate) }} টাকা </span> | 
												ফ্লোর/ফ্ল্যাট সংখ্যা:  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->floor_quantity) }} টি </span> 
											</p>
											<p class="h6"> 
												সর্বমোট সম্পত্তি টাকা পরিমানঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_asset_amount) }} টাকা </span> | 
												সম্পত্তির ওপর কর ধার্যকৃত শতাংশঃ  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->percentage_rate) }} % </span>
											</p>
											<p class="h5">এই সম্পত্তির ধার্যকৃত ট্যেক্সঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_percentage_rate_payable) }}</span> টাকা</p>
										
									  </div>
									</div>
								@endforeach
							</td>
						</tr>
					
					{{-- print_r($fyear_records_v) --}}
					
				@endforeach
				</table>
				
					<?php 
				//	echo '<pre>';
				//	print_r($payment_history); 
				//	echo '</pre>';
					?>
					<br>
			</div>		
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection




@section('js_script')
    <script>
		function asset_sqrft_and_ret_amt_calculate(sqrFeet, assetCatClass){
			var totalAmt = (sqrFeet * 1) * (assetCatClass * 1);
			$('#asset_amount').val(totalAmt);
		}
		function gandTotalAmt(totalAmt, assetUnitQty){
			var grandTotalAmt = (totalAmt * 1) * (assetUnitQty * 1);
			$('#asset_gtotal_amt').val(grandTotalAmt);
		}
		
		function percentageWithGandTotalAmt(gTotalAmt, percentage){
			var percentageAmt = (gTotalAmt * percentage) / 100;
			$('#tax_gtotal_amt').val(percentageAmt);
		}
		
		
	//	$('#per_sql_feet').val( $('#asset_cat_class').find(':selected').data('squarefeet_rate') );
	//	$('.inp_percentage').val( $('#asset_category').find(':selected').data('percentage') );
		asset_sqrft_and_ret_amt_calculate($('#sqr_feet').val(), $('#per_sql_feet').val());
		gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );
		percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
		
		
		
		/* ================= Google Map ================= */
		
        var geocoder;
        var map;
        var marker;
        var defZoom = 14;
        var defLat = 23.9413210;
        var defLon = 90.3844680;

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
            //    icon: {
            //        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
            //        anchor: new google.maps.Point(63, 63)
            //    },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
               //     icon: {
               //         url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
               //         anchor: new google.maps.Point(63, 63)
               //     },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize"></script>
@endsection