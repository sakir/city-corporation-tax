@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
        </div>
        <div class="col-8">

          <div class="search-wrapper">
              <h2 class="card-title text-center"> জোনঃ <b>{{ $holding->zone_name }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{{ $holding->ward_name }}</b></h2>
              <hr>

              <div class="row">
                  <div class="col">
                      <label for="area_id">মহল্লা: </label>
                      <strong class="form-control font-weight-bold"> {{ $holding->area_name }} </strong>
                  </div>
                  <div class="col">
                      <label for="road_id">রোড /  সড়ক নাম: </label>
                      <strong class="form-control font-weight-bold"> {{ $holding->road_name }} </strong>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-4">
                      <div class="form-group">
                          <label for="holding_no">হোল্ডিং নং</label>
                          <strong class="form-control form-control-lg text-center font-weight-bold border border-dark" >{{ $holding->holding_no }}</strong>
                      </div>
                  </div>
              </div>
              <div class="form-row">
                  <div class="col">
                      <label for="owner_name">মালিকের নাম: </label>
                      <strong class="form-control font-weight-bold">{{ $holding->owner_name }}</strong>
                  </div>
                  <div class="col">
                      <label for="owner_fathers_name">মালিকের পিতার নাম: </label>
                      <strong class="form-control font-weight-bold">{{ $holding->owner_fathers_name }}</strong>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col">
                      <div class="form-group">
                          <label for="holding_no">পেমেন্ট টাকা</label>
                          <div class="input-group mb-3">
                                <input type="number" class="form-control form-control-lg text-center font-weight-bold border border-dark" value="{!! isset($_GET['amt']) ? $_GET['amt'] : 0 !!}" readonly >
                              <div class="input-group-append">
                                  <span class="input-group-text"> <b> টাকা </b> </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col">
                      <div class="form-group">
                          <label for="holding_no"> অর্থ বছর </label>
                          <input type="text" class="form-control form-control-lg text-center font-weight-bold border border-dark" value="{!! isset($_GET['fy']) ? $_GET['fy'] : 0 !!}" readonly >

                          </div>
                      </div>
                  </div>
              <button type="submit" class="btn btn-lg btn-block btn-primary"> অনলাইন পেমেন্ট করুন এখনই </button>
              </div>
          </div>
        </div>
      </div>

  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection



@section('js_script')

@endsection
