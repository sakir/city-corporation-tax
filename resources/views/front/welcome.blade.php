@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
          <h2 style="color: #6059f6;">Gazipur City Corporation</h2>
          <h3>Tongi, Zone: 1 - 8</h3>
          <h6>Tax managment application </h6>
          <a class="button button-hero mt-4" href="login">Dashboard login</a>
        </div>
        <div class="col-8">
					
          <div class="search-wrapper">
				<div class="row">
						@foreach($zones as $zone_k => $zone_v)
						  <div class="col-lg-6 col-sm-6">
								<div class="card">
									 <div class="card-body">
										  <a href="{{ route('wards.under.zone', ['zid'=>$zone_v->id]) }}" class="btn btn-outline-primary btn-block btn-lg"> <i> Zone <b>{{ $zone_v->name }}</b> </i></a>
									 </div>
								</div>
								<br>
						  </div>
						@endforeach
				</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection
