@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
        </div>
        <div class="col-8">
					
          <div class="search-wrapper">
					<h2 class="card-title text-center"> জোনঃ <b>{{ $holding->zone_name }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{{ $holding->ward_name }}</b></h2>

					<!-- ===== Alert Message showing ====== -->
					@if(Session::has('success'))
						 <p class="alert alert-{{ 'success' }}">{!! Session::get('success') !!}</p>
					@endif

				
          </div>
        </div>
      </div>
		
		
				<!--
					<pre>
					<?php // print_r($holding); ?>
					</pre>
				-->
					<div class="row">
							<div class="col-8" >
				
									<div class="row">
										<div class="col">
											<label for="area_id">মহল্লা: </label>
											<strong class="form-control font-weight-bold"> {{ $holding->area_name }} </strong>
										</div>
										<div class="col">
											<label for="road_id">রোড /  সড়ক নাম: </label>
											<strong class="form-control font-weight-bold"> {{ $holding->road_name }} </strong>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-4">
											<div class="form-group">
												<label for="holding_no">হোল্ডিং নং</label>
												<strong class="form-control form-control-lg text-center font-weight-bold border border-dark" >{{ $holding->holding_no }}</strong>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col">
											<label for="owner_name">মালিকের নাম: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->owner_name }}</strong>
										</div>
										<div class="col">
											<label for="owner_fathers_name">মালিকের পিতার নাম: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->owner_fathers_name }}</strong>
										</div>
									</div>
									<br>
									<div class="form-row">
										<div class="col">
											<label for="phone">ফোন: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->phone }}</strong>
										</div>
										<div class="col">
											<label for="email">ইমেইল: </label>
											  <strong class="form-control font-weight-bold">{{ $holding->email }}</strong>
										</div>
									
										<div class="col">
											<label for="nid">জাতীয় পরিচয় পত্র আইডি : </label>
											  <strong class="form-control font-weight-bold">{{ $holding->nid }}</strong>
										</div>
									</div>
									<br>
									<div class="container-fluid border">
									<br>
										<h4><b>২য় পঞ্চবার্ষিকী [2018 - 2022]  এর ধার্য কৃত রেট অনুশারেঃ</b></h4><hr>
										<div class="form-row">
											<div class="col">
												<label for="asset_category">স্থাপনার ধরন : </label>
												<select name="asset_category" id="asset_category" class="form-control form-control-lg">
													<option value="{{ $holding->asset_category_id }}">{{ $holding->asset_category_title }} </option>
												</select>												
											</div>
											<div class="col">
												<label for="asset_cat_class">স্থাপনার অবস্থার ধরন : </label>
												<select name="asset_cat_class" id="asset_cat_class" class="form-control form-control-lg">
													<option value="{{ $holding->asset_cat_class_id }}">{{ $holding->asset_cat_class_title }} </option>
												</select>
												<script>document.getElementById('asset_cat_class').value = '{{ $holding->asset_cat_class_id }}';</script>
												<br><br>
												<label for="measurement_of_home">প্রতি স্কয়ার ফিট মুল্যমান : </label>
												<div class="input-group mb-3">
												  <input type="number" class="form-control form-control-lg text-right" name="per_sql_feet" id="per_sql_feet" value="{{ $per_square_feet_ret->per_square_feet_rate }}" readonly>
												  <div class="input-group-append">
													 <span class="input-group-text"> <b>টাকা</b> </span>
												  </div>
												</div>
												<!--
												  <pre>@php
														echo print_r($asset_category_class);
												  @endphp</pre>
												-->
											</div>
											<div class="col">
												<label for="asset_category">স্থাপনার ব্যেবহার ধরন : </label>
												<select name="asset_category" id="asset_category" class="form-control form-control-lg">
													<option value="{{ $holding->asset_cat_using_id }}">{{ $holding->asset_cat_using_title }} </option>
												</select>												
											</div>
										</div>
										<br>
										<div class="form-row">
											<div class="col">
												<label for="sqr_feet">মোট স্কয়ার ফিট: </label>
												<div class="input-group mb-3">
												  <input type="number" class="form-control text-right" name="sqr_feet" id="sqr_feet" value="{{ $holding->sqr_feet }}" readonly>
												  <div class="input-group-append">
													 <span class="input-group-text"> <b> sqr.Feet</b> </span>
												  </div>
												</div>
											</div>
											<div class="col-2">
												<label for="asset_qty"> &nbsp; </label>
												<button class="btn btn-block btn-lg"><b> = </b></button>
											</div>
											<div class="col">
												<label for="asset_amount">মোট সম্পদ  টাকা পরিমানঃ </label>
												<div class="input-group mb-3">
												  <input type="number" class="form-control text-right" name="asset_amount" id="asset_amount" readonly>
												  <div class="input-group-append">
													 <span class="input-group-text"> <b>টাকা</b> </span>
												  </div>
												</div>
											</div>
										</div>
										<br>
										<div class="form-row">
											<div class="col">
												<label for="asset_qty">ফ্লোর/ফ্ল্যাট সংখ্যা: </label>
												<div class="input-group mb-3">
												  <div class="input-group-append">
													 <span class="input-group-text"> <b>X</b> </span>
												  </div>
													<input type="number" class="form-control form-control-lg text-center font-weight-bold" name="asset_qty" id="asset_qty" value="{{ $holding->asset_qty }}" readonly>
												</div>
											</div>
											<div class="col-2">
											</div>
											<div class="col">
												<label for="asset_gtotal_amt">সর্বমোট  টাকা পরিমানঃ </label>
												<div class="input-group mb-3">
												  <input type="number" class="form-control form-control-lg text-right" name="asset_gtotal_amt" id="asset_gtotal_amt" readonly>
												  <div class="input-group-append">
													 <span class="input-group-text"> <b>টাকা</b> </span>
												  </div>
												</div>
											</div>
										</div>
										<br>
										<div class="form-row">
											<div class="container-fluid border border-dark pt-4 pb-3">
												<div class="form-row">
													<div class="col">
														<label for="inp_percentage_b">কর ধার্যকৃত শতাংশঃ  </label>
														<div class="input-group border border-dark mb-3">
														  <input type="number" class="form-control form-control-lg text-right inp_percentage" name="inp_percentage" id="inp_percentage_b" readonly value="{{ $holding->percentage }}">
														  <div class="input-group-append">
															 <span class="input-group-text"> &nbsp; <i class="fa fa-percent" aria-hidden="true"></i> &nbsp; </span>
														  </div>
														</div>
													</div>
													<div class="col-2 text-center">
														<label for="inp_percentage_b"> &nbsp; </label>
														<p>হারে</p> 
													</div>
													<div class="col">
														<label for="tax_gtotal_amt">প্রদেয় কর  টাকাঃ </label>
														<div class="input-group border border-dark mb-3">
														  <input type="number" class="form-control form-control-lg text-right" name="tax_gtotal_amt" id="tax_gtotal_amt" value="{{ $holding->yearly_amount }}" readonly>
														  <div class="input-group-append">
															 <span class="input-group-text"> <b>টাকা</b> </span>
														  </div>
														</div>
														[ বাৎসরিক কর ]
													</div>
												</div>
												
												<a href="{{ route('to.pay', ['id'=>$holding->id,'amount'=> $holding->yearly_amount ,'fiscal_year'=>'2019-2020']) }}" class="btn btn-lg btn-block btn-info" name=""> অনলাইনে কর পরিশোধ করুন </a>
												<p class="text-center">অনলাইনে কর পরিশোধ করতে পারেন এখনি। <br>আপনার মুল্যবান সময় ও শ্রম সাশ্রয় করুন</p>
											</div>
										</div>
									</div>
									<br>
									
									<!--
									<button class="btn btn-block btn-lg btn-primary"> Submit to Save </button>
									-->
									
									<?php 
									/*
									echo '<pre>';
										print_r($holding);
									echo '</pre>'; 
									*/
									?>
								
								
						</div>
				
				<div class="col-4" >
			
						<h3>ম্যাপ লোকেশন: </h3>
						<hr>
						<div class="form-row">
						  <div class="col-5">
								<label for="latitude">Latitude</label>
								<input type="text" id="latitude" name="latitude"
										 class="form-control form-control-sm code_text"
										 placeholder="#Latitude" value="" readonly>
						  </div>
						  <div class="col-5">
								<label for="longitude">Longitude</label>
								<input type="text" id="longitude" name="longitude"
										 class="form-control form-control-sm code_text"
										 placeholder="#Longitude" readonly>
						  </div>
						  <div class="col-2">
								<label for="zoom">Zoom</label>
								<input type="text" id="zoom" name="zoom" class="form-control form-control-sm code_text"
										 placeholder="#Zoom"
										 readonly>
						  </div>
					 </div>
					 <div id="map_canvas" style="width: 100%; height: 400px;"></div>
					 <br>	
						<label for="description">আরো বিস্তারিত/বিবরনঃ  </label><br>
						<strong class="border border-secondary p-2"> {{ $holding->description }}</strong>
					<hr>
				</div>
			</div>
			<div class="row">

					<h3>Payment History</h3>
					<?php 
					echo '<pre>';
					print_r($payment_history); 
					echo '</pre>';
					?>
					<br>
					<?php 
						$grand_due = 0;
						foreach( $payment_history as $ph_y_k => $ph_y_v ){ 
						
							$have_to_pay = (($holding->yearly_amount * 1) * ($holding_current_percentage * 1)) / 100;
							$total_payed_amt = 0;
							foreach( $ph_y_v as $ph_k => $ph_v ){ 
								$total_payed_amt += $ph_v->payment_amount;
							}
							$grand_due += $have_to_pay - $total_payed_amt;
						}
					 ?>
						<table id="" class="table text-white bg-secondary table-striped col_1st_left col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th>Total Due:</th>
										 <th><b><?=$grand_due?></b> Tk.</th>
									</tr>
							  </thead>
						</table>
					<?php 
						$grand_due = 0;
						foreach( $payment_history as $ph_y_k => $ph_y_v ){ ?>		
						
						<table id="payment_history_table" class="table table-striped table-bordered table-sm col_2_right col_1ast_right mb-0">	
							  <thead class="text-white bg-secondary">							
									<tr class="text-white bg-secondary">
										 <th class="pb-0" colspan="2"><h4 class="font-italic text-center text-white mb-0"><?=$ph_y_k?></h4></th>
									</tr>
									<tr>
										 <th><small>Date</small></th>
										 <th><small>Amount</small></th>
									</tr>
							  </thead>
							  <tbody>
								<?php 
									$have_to_pay = (($holding->yearly_amount * 1) * ($holding_current_percentage * 1)) / 100;
									$total_payed_amt = 0;
									foreach( $ph_y_v as $ph_k => $ph_v ){ 
										$total_payed_amt += $ph_v->payment_amount;
									?>									
									<tr>
										 <td><small><?= date( 'd-M-Y', strtotime( $ph_v->payment_datetime ) ) ?></small></td>
										 <td><?= $ph_v->payment_amount ?> Tk.</td>
									</tr>
								<?php	} ?>
							  </tbody>
						 </table>
						
						<table id="" class="table bg-secondary text-white table-striped table-bordered table-sm col_1st_right col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th><small>Have to pay</small></th>
										 <th><small>Payed Amount</small></th>
										 <th><small>Due</small></th>
									</tr>
							  </thead>
							  <tbody>								
									<tr class="text-white bg-secondary">
										 <td><?= $have_to_pay ?> Tk.</td>
										 <td><?= $total_payed_amt ?> Tk.</td>
										 <td><?= $have_to_pay - $total_payed_amt ?> Tk.</td>
									</tr>
							  </tbody>
						 </table>
						
					<?php	
						$grand_due += $have_to_pay - $total_payed_amt;
						} ?>
					
						<table id="" class="table text-white bg-secondary table-striped col_1st_left col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th>Total Due:</th>
										 <th><b><?=$grand_due?></b> Tk.</th>
									</tr>
							  </thead>
						</table>
			</div>


		
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection




@section('js_script')
    <script>
		function asset_sqrft_and_ret_amt_calculate(sqrFeet, assetCatClass){
			var totalAmt = (sqrFeet * 1) * (assetCatClass * 1);
			$('#asset_amount').val(totalAmt);
		}
		function gandTotalAmt(totalAmt, assetUnitQty){
			var grandTotalAmt = (totalAmt * 1) * (assetUnitQty * 1);
			$('#asset_gtotal_amt').val(grandTotalAmt);
		}
		
		function percentageWithGandTotalAmt(gTotalAmt, percentage){
			var percentageAmt = (gTotalAmt * percentage) / 100;
			$('#tax_gtotal_amt').val(percentageAmt);
		}
		
		
	//	$('#per_sql_feet').val( $('#asset_cat_class').find(':selected').data('squarefeet_rate') );
	//	$('.inp_percentage').val( $('#asset_category').find(':selected').data('percentage') );
		asset_sqrft_and_ret_amt_calculate($('#sqr_feet').val(), $('#per_sql_feet').val());
		gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );
		percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
		
		
		
		/* ================= Google Map ================= */
		
        var geocoder;
        var map;
        var marker;
        var defZoom = 14;
        var defLat = 23.9413210;
        var defLon = 90.3844680;

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
            //    icon: {
            //        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
            //        anchor: new google.maps.Point(63, 63)
            //    },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
               //     icon: {
               //         url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
               //         anchor: new google.maps.Point(63, 63)
               //     },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize"></script>
@endsection