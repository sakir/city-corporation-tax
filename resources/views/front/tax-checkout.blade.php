@extends('layouts-front.general')

@section('content')


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

        <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
        </div>
        <div class="col-8">

          <div class="search-wrapper">
              <h2 class="card-title text-center"> জোনঃ <b>{-{ $holding->zone_name }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{-{ $holding->ward_number }}</b></h2>
              <hr>

              <div class="row">
                  <div class="col">
                      <label for="area_id">মহল্লা: </label>
                      <strong class="form-control font-weight-bold"> {-{ $holding->area_name }} </strong>
                  </div>
                  <div class="col">
                      <label for="road_id">রোড /  সড়ক নাম: </label>
                      <strong class="form-control font-weight-bold"> {-{ $holding->road_name }} </strong>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-4">
                      <div class="form-group">
                          <label for="holding_no">হোল্ডিং নং</label>
                          <strong class="form-control form-control-lg text-center font-weight-bold border border-dark" >{-{ $holding->holding_no }}</strong>
                      </div>
                  </div>
              </div>
              <div class="form-row">
                  <div class="col">
                      <label for="owner_name">মালিকের নাম: </label>
                      <strong class="form-control font-weight-bold">{-{ $holding->owner_name }}</strong>
                  </div>
                  <div class="col">
                      <label for="owner_fathers_name">মালিকের পিতার নাম: </label>
                      <strong class="form-control font-weight-bold">{-{ $holding->owner_fathers_name }}</strong>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col">
                      <div class="form-group">
                          <label for="holding_no">পেমেন্ট টাকা</label>
                          <div class="input-group mb-3">
                                <input type="number" class="form-control form-control-lg text-center font-weight-bold border border-dark" value="{!! isset($_GET['amt']) ? $_GET['amt'] : 0 !!}" readonly >
                              <div class="input-group-append">
                                  <span class="input-group-text"> <b> টাকা </b> </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col">
                      <div class="form-group">
                          <label for="holding_no"> অর্থ বছর </label>
                          <input type="text" class="form-control form-control-lg text-center font-weight-bold border border-dark" value="{!! isset($_GET['fy']) ? $_GET['fy'] : 0 !!}" readonly >

                          </div>
                      </div>
                  </div>
              <button type="submit" class="btn btn-lg btn-block btn-primary"> অনলাইন পেমেন্ট করুন এখনই </button>
              </div>
          </div>
        </div>


        <hr>
        @php
            /* PHP */
            $post_data = array();
            $post_data['store_id'] = "allex5e13792d31125";
            $post_data['store_passwd'] = "allex5e13792d31125@ssl";
            $post_data['total_amount'] = isset($_GET['amt']) ? $_GET['amt'] : 0;
            $post_data['currency'] = "BDT";
            $post_data['tran_id'] = "SSLCZ_TEST_".uniqid();
            $post_data['success_url'] = route('holding.taxpay-success') . '?holding_id=' . 45;
            $post_data['fail_url'] = "http://localhost/new_sslcz_gw/fail.php";
            $post_data['cancel_url'] = "http://localhost/new_sslcz_gw/cancel.php";
            # $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

            # EMI INFO
            $post_data['emi_option'] = "1";
            $post_data['emi_max_inst_option'] = "9";
            $post_data['emi_selected_inst'] = "9";

            # CUSTOMER INFORMATION
            $post_data['cus_name'] = '$holding->owner_name';
            $post_data['cus_email'] = "test@test.com";
            $post_data['cus_add1'] = "Dhaka";
            $post_data['cus_add2'] = "Dhaka";
            $post_data['cus_city'] = "Dhaka";
            $post_data['cus_state'] = "Dhaka";
            $post_data['cus_postcode'] = "1000";
            $post_data['cus_country'] = "Bangladesh";
            $post_data['cus_phone'] = "01711111111";
            $post_data['cus_fax'] = "01711111111";

            # SHIPMENT INFORMATION
            $post_data['ship_name'] = "testallexw36d";
            $post_data['ship_add1 '] = "Dhaka";
            $post_data['ship_add2'] = "Dhaka";
            $post_data['ship_city'] = "Dhaka";
            $post_data['ship_state'] = "Dhaka";
            $post_data['ship_postcode'] = "1000";
            $post_data['ship_country'] = "Bangladesh";

            # OPTIONAL PARAMETERS
            $post_data['fiscal_year'] = isset($_GET['fy']) ? $_GET['fy'] : '';
            $post_data['cus_holding_id '] = '$holding->id';
            $post_data['value_a'] = isset($_GET['fy']) ? $_GET['fy'] : '';
            $post_data['value_b'] = '$holding->id';
            $post_data['value_c'] = "ref003";
            $post_data['value_d'] = "ref004";

            # CART PARAMETERS
            $post_data['cart'] = json_encode(array(
                array("product"=>"DHK TO BRS AC A1","amount"=>"200.00"),
                array("product"=>"DHK TO BRS AC A2","amount"=>"200.00"),
                array("product"=>"DHK TO BRS AC A3","amount"=>"200.00"),
                array("product"=>"DHK TO BRS AC A4","amount"=>"200.00")
            ));
            $post_data['product_amount'] = "100";
            $post_data['vat'] = "5";
            $post_data['discount_amount'] = "5";
            $post_data['convenience_fee'] = "3";











            $direct_api_url = "https://sandbox.sslcommerz.com/gwprocess/v3/api.php";

            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $direct_api_url );
            curl_setopt($handle, CURLOPT_TIMEOUT, 30);
            curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($handle, CURLOPT_POST, 1 );
            curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


            $content = curl_exec($handle );

            $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

            if($code == 200 && !( curl_errno($handle))) {
                curl_close( $handle);
                $sslcommerzResponse = $content;
            } else {
                curl_close( $handle);
                echo "FAILED TO CONNECT WITH SSLCOMMERZ API";
                exit;
            }

            # PARSE THE JSON RESPONSE
            $sslcz = json_decode($sslcommerzResponse, true );

            if(isset($sslcz['GatewayPageURL']) && $sslcz['GatewayPageURL']!="" ) {
                    # THERE ARE MANY WAYS TO REDIRECT - Javascript, Meta Tag or Php Header Redirect or Other
                    # echo "<script>window.location.href = '". $sslcz['GatewayPageURL'] ."';</script>";
                echo "<meta http-equiv='refresh' content='0;url=".$sslcz['GatewayPageURL']."'>";
                # header("Location: ". $sslcz['GatewayPageURL']);
                exit;
            } else {
                echo "JSON Data parsing error!";
            }



        @endphp



    </div>

  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection



@section('js_script')

@endsection
