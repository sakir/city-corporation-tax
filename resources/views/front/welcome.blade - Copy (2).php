@extends('layouts-front.general2')

@section('content')


  <main id="main">


    <!--==========================
      Services Section
    ============================-->
    <section id="intro" class="section-bg pt-5">
		<br><br><br>
      <div class="container">
        <div class="row">

			  <div class="col-md-5 intro-info">
			  
					<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
				 <h3><span>Gazipur City Corporation</span></h3>
				 <h3>Tongi, Zone: 1 - 8</h3>
				 <h6>Tax managment application </h6>
				 <a class="btn-get-started scrollto" href="login">Dashboard login</a>
				 
			  </div>
	  
			  <div class="col-md-7 " id="faq">
					  
				  <ul id="faq-list" class="wow fadeInUp">
		  
					@foreach($zones as $zone_k => $zone_v)
					 <li class="">
						<a data-toggle="collapse" class="collapsed" href="#faq{{ $zone_v->id }}"> Zone <b>{{ $zone_v->name }}</b> <i class="ion-android-remove"></i></a>
						<div id="faq{{ $zone_v->id }}" class="collapse" data-parent="#faq-list">
						  <p>
							 Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
						  </p>
						</div>
					 </li>

					@endforeach
					 <li>
						<a data-toggle="collapse" href="#faq2" class="collapsed">Feugiat scelerisque varius morbi enim nunc faucibus a pellentesque? <i class="ion-android-remove"></i></a>
						<div id="faq2" class="collapse" data-parent="#faq-list">
						  <p>
							 Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
						  </p>
						</div>
					 </li>

				  </ul>
					<div class="row">
							@foreach($zones as $zone_k => $zone_v)
							  <div class="col-lg-6 col-sm-6">
									<div class="card">
										 <div class="card-body">
											  <a href="{{ route('wards.under-zone', ['zid'=>$zone_v->id]) }}" class="btn btn-outline-primary btn-block btn-lg"> <i> Zone <b>{{ $zone_v->name }}</b> </i></a>
										 </div>
									</div>
									<br>
							  </div>
							@endforeach
					</div>
			  </div>
        </div>

      </div>
    </section><!-- #services -->



    <!--==========================
      About Us Section
    ============================-->
    <section id="about">

      <div class="container">
        <div class="row">

          <div class="col-lg-5 col-md-6">
            <div class="about-img">
              <img src="{{ asset('/') }}front2/img/about-img.jpg" alt="">
            </div>
          </div>

          <div class="col-lg-7 col-md-6">
            <div class="about-content">
              <h2>About Us</h2>
              <h3>Odio sed id eos et laboriosam consequatur eos earum soluta.</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <p>Aut dolor id. Sint aliquam consequatur ex ex labore. Et quis qui dolor nulla dolores neque. Aspernatur consectetur omnis numquam quaerat. Sed fugiat nisi. Officiis veniam molestiae. Et vel ut quidem alias veritatis repudiandae ut fugit. Est ut eligendi aspernatur nulla voluptates veniam iusto vel quisquam. Fugit ut maxime incidunt accusantium totam repellendus eum error. Et repudiandae eum iste qui et ut ab alias.</p>
              <ul>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ion-android-checkmark-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </section><!-- #about -->



  </main>



  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner magic-ball">
    <div class="container">

      <div class="row align-items-center text-center text-md-left">
        <div class="col-4 mb-md-0 text-center">
				<img src="http://gazipur-citycorpo.local/assets/front/img/gct-logo.png" class="">
          <h2 style="color: #6059f6;">Gazipur City Corporation</h2>
          <h3>Tongi, Zone: 1 - 8</h3>
          <h6>Tax managment application </h6>
          <a class="button button-hero mt-4" href="login">Dashboard login</a>
        </div>
        <div class="col-8">
		  
		  
          <div class="search-wrapper">
				<div class="row">
						@foreach($zones as $zone_k => $zone_v)
						  <div class="col-lg-6 col-sm-6">
								<div class="card">
									 <div class="card-body">
										  <a href="{{ route('wards.under-zone', ['zid'=>$zone_v->id]) }}" class="btn btn-outline-primary btn-block btn-lg"> <i> Zone <b>{{ $zone_v->name }}</b> </i></a>
									 </div>
								</div>
								<br>
						  </div>
						@endforeach
				</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

<br><br>

@endsection
