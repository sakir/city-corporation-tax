@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-step-backward" aria-hidden="true"></i> Back</a> &nbsp; 
						<small>SuperAdmin</small> |  ট্যেক্স সংগ্রহ
					</h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
				
                <div class="card">
                    <div class="card-block">
								<form action="{{ route('reports.tax-collection-post') }}" method="post">
									@csrf
									<div class="container-fluid">
									  <div class="row">
										 <div class="col-2">
											<label class="" for="zone_id">জোন</label>
											<select class="form-control form-control-lg text-center" id="zone_id">
												<option selected> -- </option>
												<option value="1">০১</option>
												<option value="2">০২</option>
												<option value="3">০৩</option>
												<option value="4">০৪</option>
												<option value="5">০৫</option>
												<option value="6">০৬</option>
												<option value="7">০৭</option>
												<option value="8">০৮</option>
											</select>
											<script>document.getElementById('zone_id').value = '{{ Auth::user()->permission_id }}';</script>
										 </div>
										 <div class="col-2">
											<label class="" for="ward_id">ওয়ার্ড নির্বাচন করুন</label>
											<select class="form-control form-control-lg text-center" id="ward_id">
												<option value=""> -- </option>
												@foreach( $wards as $w_k => $w_v )
													<option value="{{ $w_v->ward_id }}"> W: {{ $w_v->ward_number }} </option>
												@endforeach
											</select>
										 </div>
										 <div class="col">
											<label class="" for="wards_area_id">মহল্লা নির্বাচন করুন</label>
											<select class="form-control form-control-lg" id="wards_area_id">
												<option selected> -- </option>
												<option> ওয়ার্ড সিলেক্ট করতে হবে আগে </option>
											</select>
										 </div>
										 <div class="col-3">
											<label class="" for="holding_id">হোল্ডিং নং </label>
											<select class="form-control form-control-lg border border-dark text-center" id="holding_id" name="holding_id">
												<option selected> -- </option>
												<option> মহল্লা সিলেক্ট করতে হবে আগে </option>
											</select>
											<!-- 
											<input type="text" class="form-control form-control-lg text-center border border-dark " id="holding_no">
											-->
										 </div>
									  </div>
									  <br>
									  <div class="form-row">
										 <div class="col-3">
											<label class="" for="fiscal_year" required > অর্থ বছর </label>
											<select class="form-control form-control-lg text-center" id="fiscal_year" name="fiscal_year">
												<option value="" selected> -- </option>
												@foreach( $fiscal_years as $fy )
													<option value="{{ $fy }}">{{ $fy }}</option>
												@endforeach
											</select>
											<!-- <script>document.getElementById('zone_id').value = '{{ Auth::user()->permission_id }}';</script> -->
										 </div>
										 <div class="col-3">
											<label class="" for="receipt_amount"> টাকা প্রদান </label>
											<input type="number" class="form-control form-control-lg text-center" id="receipt_amount" name="receipt_amount" min="0">
										 </div>
										 <div class="col-3">
											<label class="" for="">  </label>
											<button type="submit" class="btn btn-primary btn-lg btn-block"> জমা গ্রহন </button>
										 </div>
										 <div class="col-2">
											
											<a href="#" class="text-center btn-lg btn-block" id=""> বিস্তারিত </a>
										 </div>
										</div>
									
										<div class="" id="holdings_detail_show"></div>
									</div>
								</form>
								<hr>
                    </div>
                </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
   <script>

         function holdings_load_by_ward_areas(wards_area_id){
				
				 $.ajax({
                  method: 'POST',
                  type: 'POST',
                  url:'{{ route("ajax.holdings-load-by-ward-areas") }}',
                  dataType: 'JSON',
                  data:{
                      _token: $('meta[name="csrf-token"]').attr('content'),
                      wards_area_id:wards_area_id
                  },
                  success: function(res){
							
                    //  console.log(res);
						
                      if( res.status  ){
									var holdingOptions = '<option value=""> - এবার হোল্ডিং নির্বাচন করুন - </option>';
									$.each(res.data, function (key, value){
										var object = value;
										holdingOptions += '<option value=' + value.id + '>' + value.holding_no + '</option>';
									//	$.each(object, function(k,v) {
										//	console.log(value); //this will print names of programming languages to the console.
									//	}
									});
									$('#holding_id option').remove();
									$('#holding_id').append(holdingOptions);
									
                      }else{
									/*
									thisEle.find('.per_sql_feet').val(0);
									thisEle.find('.asset_amount').val(0);
									thisEle.find('.asset_gtotal_amt').val(0);
									*/
                      }
							
                  //    $('#per_sql_feet').val(res.per_square_feet_rate);
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      console.log('jqXHR',jqXHR);
                      console.log('textStatus',textStatus);
                      console.log('errorThrown',errorThrown);
                  }
              });
			}
			 
         function wards_area_load(ward_id){
				
            $.ajax({
					method: 'POST',
					type: 'POST',
					url:'{{ route("ajax.words-area") }}',
					dataType: 'JSON',
					data:{
						 _token: $('meta[name="csrf-token"]').attr('content'),
						 ward_id:ward_id
					},
					success: function(res){
						
						// console.log(res);
					
						 if( res.status  ){
								var wardAreaOptions = '<option value=""> -- এবার মহল্লা নির্বাচন করুন -- </option>';
								$.each(res.data, function (key, value){
									var object = value;
									wardAreaOptions += '<option value=' + value.area_id + '>' + value.area_name + '</option>';
								//	$.each(object, function(k,v) {
									//	console.log(value); //this will print names of programming languages to the console.
								//	}
								});
								$('#wards_area_id option').remove();
								$('#wards_area_id').append(wardAreaOptions);
							 
						 }else{
								/*
								thisEle.find('.per_sql_feet').val(0);
								thisEle.find('.asset_amount').val(0);
								thisEle.find('.asset_gtotal_amt').val(0);
								*/
						 }
						
					//    $('#per_sql_feet').val(res.per_square_feet_rate);
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log('jqXHR',jqXHR);
						console.log('textStatus',textStatus);
						console.log('errorThrown',errorThrown);
					}
            });
         }
				
				

         function holdings_tax_history_load(holdingAndFyear){
				
            $.ajax({
					method: 'POST',
					type: 'POST',
					url:'{{ route("reports.holdings_total_due") }}',
					dataType: 'JSON',
					data:{
						 _token: 		$('meta[name="csrf-token"]').attr('content'),
						 holding_id:	holdingAndFyear.holding_id,
						 fyear:			holdingAndFyear.fyear
					},
					success: function(res){
						
						// console.log(res);
						
						 if( res.status  ){
							 console.log('if: res.status: ', res);
							 if( res.status == 'success' ){
								  var paymentHistoryHTML_cont = '';
								  var paymentHistoryHTML = '<br><table class="table table-bordered table-striped">' +
																		'<thead>'+
																			'<tr class="text-center thead-light">'+
																				'<th>অর্থ বছর</th>' +
																				'<th>মোট ধার্যকৃত ট্যেক্স</th>' +
																				'<th>মোট পরিশোধিত টাকা</th>' +
																				'<th>মোট অপরিশোধিত টাকা</th>' +
																			'</tr>'+
																		'</thead>' +
																		'<tbody>';
								  $.each(res.fyear_asset_records, function(fyearAssetK, fyearAssetV) {
										
										paymentHistoryHTML = paymentHistoryHTML + 
																				'<tr>' +
																					'<td class="h6">'+ fyearAssetV.fyear +'</td>' +
																					'<td class="h6 text-primary text-right">' + fyearAssetV.fyear_total_tax_amount +'</td>' +
																					'<td class="h6 text-success text-right">' + fyearAssetV.fyear_tax_total_payment +'</td>' +
																					'<td class="h6 text-danger text-right">' + (parseInt(fyearAssetV.fyear_total_tax_amount) - parseInt(fyearAssetV.fyear_tax_total_payment)) +'</td>' +
																				'</tr>' ;
									//	paymentHistoryHTML_cont += paymentHistoryHTML;
								  });
								  $('#holdings_detail_show').html(paymentHistoryHTML + '</tbody></table>');
							 }
						 }else{
							console.log('else: res.status : ', res);
								/*
								thisEle.find('.per_sql_feet').val(0);
								thisEle.find('.asset_amount').val(0);
								thisEle.find('.asset_gtotal_amt').val(0);
								*/
						 }
						
					//    $('#per_sql_feet').val(res.per_square_feet_rate);
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log('jqXHR',jqXHR);
						console.log('textStatus',textStatus);
						console.log('errorThrown',errorThrown);
					}
            });
         }
				
				
				
			$( document ).ready(function() {
					
				$('#ward_id').on('change', function(){
				//	 console.log('$(this).val()',$(this).val());
					 wards_area_load($(this).val());
				});
					
				$('#wards_area_id').on('change', function(){
					// console.log('$(this).val()',$(this).val());
					 holdings_load_by_ward_areas($(this).val());
				});
				
				$('#holding_id, #fiscal_year').on('change', function(){
					 console.log('Holding ID: ',$('#holding_id').val());
					 console.log('অর্থ বছর: ',$('#fiscal_year').val());
					 var holdingAndFyear = {
						 holding_id: 	$('#holding_id').val(),
						 fyear:			$('#fiscal_year').val()
					 }
					 holdings_tax_history_load(holdingAndFyear);
				});
				
			});
			 
   </script>

    <script type="text/javascript">
    </script>
@endsection
