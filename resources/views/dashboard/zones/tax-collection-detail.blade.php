@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-step-backward" aria-hidden="true"></i> Back</a> &nbsp; 
						<small>SuperAdmin</small> |  ট্যেক্স সংগ্রহ
					</h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

                <div class="card">
                    <div class="card-block">
					  
								<div class="container-fluid">
									<div class="row">
									 <div class="col-2">
										<p class="h2" for="zone_id"><small>জোন </small> <span class="badge badge-primary">{{ BngConv::en2bn($holding->zone) }}</span></p>
										
									 </div>
									 <div class="col-2">
										<p class="h2" for="zone_id"><small>ওয়ার্ড</small>  <span class="badge badge-primary">{{ BngConv::en2bn($holding->ward_number) }}</span></p>
									 </div>
									 <div class="col">
										<p class="h2" for="zone_id"><small>মহল্লা </small> <span class="badge badge-primary">{{ $holding->area_name }}</span></p>
									 </div>
									 <div class="col-3">
										<p class="h2" for="zone_id"><small>হোল্ডিং </small> <span class="badge badge-primary">{{ BngConv::en2bn($holding->holding_no) }}</span></p>
									 </div>
									</div>

									<!-- ===== Alert Message showing ====== -->
									@foreach (['danger', 'warning', 'success', 'info'] as $key)
										 @if(Session::has($key))

											  <div class="toast {{ $key }}" >
													<div class="toast-header">
														 <div class="lbl_color rounded mr-2"></div>
														 <strong class="mr-auto">{{ $key }}</strong>

														 <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
															  <span aria-hidden="true"> &times; </span>
														 </button>
													</div>
													<div class="toast-body">
														 {!! Session::get($key) !!}
													</div>
											  </div>
										 @endif
									@endforeach

									<br>
									<table class="table table-striped ">
									@foreach( $fyear_asset_records as $fyear_records_k => $fyear_records_v )
										
											<tr>
												<td>
													<p class="h3 border border-dark text-center p-2">{{ $fyear_records_v['fyear'] }}</p>
													
													<p class="h5"> মোট ধার্যকৃত ট্যেক্সঃ <span class="badge badge-primary">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount']) }}</span> টাকা</p>
													<p class="h6"> মোট পরিশোধিত <span class="badge badge-success">{{ BngConv::en2bn($fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা</p>
													<p class="h6"> মোট অপরিশোধিত <span class="badge badge-danger">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount'] - $fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা</p>
													<div class="progress  bg-danger">
													  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{ $fyear_records_v['payment_percentage'] }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</td>
												<td>
													@foreach( $fyear_records_v['asset_records'] as $asset_k => $asset_v )
													
														<div class="card border mb-2">
														  <div class="card-body">
																<p class="h6">
																	<span class="badge badge-secondary">{{ $asset_v->asset_category_title }}</span> | 
																	<span class="badge badge-secondary">{{ $asset_v->asset_cat_class_title }}</span> | 
																	<span class="badge badge-secondary">{{ $asset_v->asset_cat_using_title }}</span>
																</p>
																<p class="h6"> 
																	স্কয়ার ফিট <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->square_feet) }}</span> | 
																	প্রতি স্কয়ার ফিট মুল্যমান :  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->squarefeet_rate) }} টাকা </span> | 
																	ফ্লোর/ফ্ল্যাট সংখ্যা:  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->floor_quantity) }} টি </span> 
																</p>
																<p class="h6"> 
																	সর্বমোট সম্পত্তি টাকা পরিমানঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_asset_amount) }} টাকা </span> | 
																	সম্পত্তির ওপর কর ধার্যকৃত শতাংশঃ  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->percentage_rate) }} % </span>
																</p>
																<p class="h5">এই সম্পত্তির ধার্যকৃত ট্যেক্সঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_percentage_rate_payable) }}</span> টাকা</p>
															
														  </div>
														</div>
													@endforeach
												</td>
												<td>
														<div class="card border mb-2 pt-2 text-center">
																<span class="h5">ট্যেক্স পরিশোধঃ  </span>
														</div>
													@foreach( $fyear_records_v['fyear_tax_payment_records'] as $paid_k => $paid_v )
														<div class="card border mb-2">
														  <div class="card-body">
																<h5>টাকাঃ {{ BngConv::en2bn(round($paid_v->payment_amount*1, 2)) }}</h5>
																<p><em>{{ $paid_v->payment_datetime }} </em></p>
																<span class="badge badge-secondary">{{ $paid_v->payment_by }} </span>
														  </div>
														</div>
													@endforeach
													
												</td>
											</tr>
										
										{{-- print_r($fyear_records_v) --}}
										
									@endforeach
									</table>
									<div class="" id="holdings_detail_show"></div>
									
								</div>
									
								<hr>
                    </div>
                </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
   <script>

			 
   </script>

    <script type="text/javascript">
    </script>
@endsection
