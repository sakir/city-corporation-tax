@extends('layouts-dashboard.general')

@section('content')


   <!-- ============================================================== -->
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid">
       <!-- ============================================================== -->
       <!-- Bread crumb and right sidebar toggle -->
       <!-- ============================================================== -->
       <div class="row page-titles">
           <div class="col-md-6 col-8 align-self-center">
               <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | Dashboard Home</h3>
           </div>
           <div class="col-md-6 col-4 align-self-center">
               <a href="" class="btn pull-right hidden-sm-down btn-success"> --- </a>
           </div>
       </div>
       <!-- ============================================================== -->
       <!-- End Bread crumb and right sidebar toggle -->
       <!-- ============================================================== -->
       <!-- ============================================================== -->
       <!-- Start Page Content -->
       <!-- ============================================================== -->
       <div class="row">
           <div class="col-12">
               <div class="card">
                   <div class="card-block text-center">
                       
                    <img src="http://gazipur-citycorpo.lara/dashboard/images/favicon.png">
                    <img src="http://gazipur-citycorpo.lara/dashboard/images/gazipur-city-zones.jpg" width="400px">
                    <h3>গাজীপুর সিটি কর্পোরেশন </h3>
                    <h3>পূর্ণাঙ্গ  <b>কর ব্যাবস্থাপনা</b> সফটওয়্যার</h3>
                   </div>
               </div>
           </div>
       </div>
       <!-- ============================================================== -->
       <!-- End PAge Content -->
       <!-- ============================================================== -->
   </div>
   <!-- ============================================================== -->
   <!-- End Container fluid  -->
   <!-- ============================================================== -->

   
@endsection
