@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-step-backward" aria-hidden="true"></i> Back</a> &nbsp; 
						<small>SuperAdmin</small> |  ---
					</h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
				
                <div class="card">
                    <div class="card-block">
								<table class="table">
									<tr>
										<td>
											<h3>অর্থ বছর: {{ $fyear }}</h3>
											<hr>
											<h3 class="text-primary"><small>মোট পাওনা</small>: <b>{{ round($tax_percent_amt_total, 2) }}</b> টাকা</h3>
											<h3 class="text-success"><small>মোট প্রাপ্তি</small>: <b>{{ round($total_payments_amount, 2) }}</b> টাকা</h3>
											<h3 class="text-danger"><small>মোট বকেয়া</small>: <b>{{ round($tax_percent_amt_total - $total_payments_amount, 2) }}</b> টাকা</h3>
										</td>
										<td>
											<div id="piechart_3d" style="width: 400px; height: 250px;"></div>
										</td>
									</tr>
								</table>
								<hr>
                        <table id="tbl_payments" data-select="" class="table table-striped table-hover col_4_right col_5_right col_1ast_right">
                            <thead>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>ওয়ার্ড</th>
                                <th>হোল্ডিং</th>
                                <th>পেমেন্ট টাকা</th>
                                <th>পেমেন্ট সময়</th>
                                <th>পেমেন্ট মাধ্যম</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>ওয়ার্ড</th>
                                <th>হোল্ডিং</th>
                                <th>পেমেন্ট টাকা</th>
                                <th>পেমেন্ট সময়</th>
                                <th>পেমেন্ট মাধ্যম</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
   <script>
		
			// alert(url + "settings/dt-ajax-all-wards);
			function load_payments(tbl_ID) {
				
            var data = [
                {"data": "sl"},
                {"data": "ward_no"},
                {"data": "holding_no"},
                {"data": "payment_amount"},
                {"data": "payment_datetime"},
                {"data": "payment_by"}
            ];
				
            var jsonDataURL = "{{ route('reports.dt_ajax_fiscal_year_payment_list',['fyear'=>$fyear]) }}";
				
            $.getJSON(jsonDataURL, function (result) {
					
                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    "columns": data
                });
            });
				
            var rowIndex = 0;
			}
			
			load_payments('#tbl_payments');

   </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {	
		
        var data = google.visualization.arrayToDataTable([
			//	['Task', 'Total balance percentage view'],
				['Task', 'Hours per Day'],
          ['মোট প্রাপ্তি',     "{{ round($total_payments_amount, 2) }}" * 1],
          ['মোট বকেয়া',      "{{ round($tax_percent_amt_total - $total_payments_amount, 2) }}" * 1],
        ]);
			
        var options = {
          is3D: true,
			 chartArea:{left:0,top:20,width:'100%',height:'100%'},
          slices: {
            0: { color: '#55ce63' },
            1: { color: '#f62d51' }
          },
			 pieHole: 0.4,
        };
			
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
@endsection

