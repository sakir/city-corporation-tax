@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | Employee <small> (  Add New Employee )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/zones') }}" class="btn pull-right hidden-sm-down btn-success"> Add New Employee </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- ===== Alert Message showing ====== -->
        @foreach (['danger', 'warning', 'success', 'info'] as $key)
            @if(Session::has($key))

                <div class="toast {{ $key }}" >
                    <div class="toast-header">
                        <div class="lbl_color rounded mr-2"></div>
                        <strong class="mr-auto">{{ $key }}</strong>

                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true"> &times; </span>
                        </button>
                    </div>
                    <div class="toast-body">
                        {!! Session::get($key) !!}
                    </div>
                </div>
            @endif
        @endforeach


        <form action="{{ route('settings.employee.add.post') }}" method="post" enctype="multipart/form-data" >
            <div class="card container-fluid">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row">
                <div class="col-6">

                        <div class="card-header">
                            <h4 class="card-title">Employee primary information :</h4>
                        </div>
                        <br>
                        @csrf
                        <div class="form-group">
                            <label for="employee_name">Employee Name</label>
                            <input type="text" class="form-control form-control-lg" id="employee_name" name="employee_name"
                                   placeholder="Employee Name">
                        </div>
                        <div class="form-group">
                            <label for="employee_email">Email</label>
                            <input type="text" class="form-control" id="employee_email" name="employee_email"
                                   placeholder="employee@email.address">
                        </div>
                        <br>


                        <div class="card-header">

                            <h4 class="card-title">Employee basic information :</h4>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="emp_phone">Phone numbers:</label>
                            <input type="text" class="form-control" id="emp_phone" name="emp_phone" placeholder="Phone Numbers">
                        </div>
                        <div class="form-group">
                            <label for="nid">National ID:</label>
                            <input type="text" class="form-control" id="nid" name="nid" placeholder="employee@email.address">
                        </div>
                        <div class="form-group">
                            <label for="nid">Current Address:</label>
                            <textarea class="form-control" id="nid" name="nid" placeholder="Address"></textarea>

                                <button class="btn btn-secondary btn-sm float-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Set on Google Map <i class="fa fa-map-marker m-r-10" aria-hidden="true"></i>
                                </button>
                                <div class="clearfix"></div>
                            <div class="collapse border" id="collapseExample">
                                <div class="card card-body">

                                    <div class="form-row">
                                        <div class="col-5">
                                            <label for="latitude">Latitude</label>
                                            <input type="text" id="latitude" name="latitude"
                                                   class="form-control form-control-sm"
                                                   placeholder="#Latitude" readonly>
                                        </div>
                                        <div class="col-5">
                                            <label for="longitude">Longitude</label>
                                            <input type="text" id="longitude" name="longitude"
                                                   class="form-control form-control-sm"
                                                   placeholder="#Longitude" readonly>
                                        </div>
                                        <div class="col-2">
                                            <label for="zoom">Zoom</label>
                                            <input type="text" id="zoom" name="zoom" class="form-control form-control-sm"
                                                   placeholder="#Zoom"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div id="map_canvas" style="width: 100%; height: 400px;"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="facebood">Facebook ID link:</label>
                            <input type="text" class="form-control" id="facebood" name="facebood" placeholder="Facebook ID link">
                        </div>
                        <div class="form-group">
                            <label for="skype">Skype:</label>
                            <input type="text" class="form-control" id="skype" name="skype" placeholder="skype">
                        </div>
                        <div class="form-group">
                            <label for="whatsapp">whatsapp:</label>
                            <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="whatsapp">
                        </div>
                        <div class="form-group">
                            <label for="imo">imo:</label>
                            <input type="text" class="form-control" id="imo" name="imo" placeholder="imo">
                        </div>
                        <hr>

                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">

                            <div class="material-switch pull-right">
                                <strong>Is active or inactive</strong> &nbsp;
                                <input id="emp_status" name="emp_status" type="checkbox" value="1"/>
                                <label for="emp_status" class="label-primary"></label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <p>Profile Picture:</p>
                            <input type="file" name="profile_pic" class="form-control">
                        </div>
                        <div class="col-8">
                            <div class="" style="width: 200px;height: 200px;border: 2px dashed #01ABED;">
                                <img src="{{ url('/') }}/images/dummy-pp-200.jpg" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="more_detail">More Details ( or Permanent Address):</label>
                        <textarea class="form-control" id="more_detail" name="more_detail" rows="5" placeholder="More details"></textarea>
                    </div>
                    <hr>
                    <br>
                    <div class="form-group">
                        <label for="zone_name">Select the Working Zone</label>
                        <select class="form-control" name="zone_id">
                            <option value=""> - Select working zone - </option>
                            @foreach ($zones as $zone)
                                <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="emp_phone">Joining Date:</label>
                        <input type="text" class="form-control" id="emp_phone" name="emp_phone" placeholder="Joining Date">
                    </div>
                    <div class="form-group">
                        <label for="emp_phone">Designation:</label>
                        <input type="text" class="form-control" id="emp_phone" name="emp_phone" placeholder="Designation">
                    </div>
                    <div class="form-group">
                        <label>Make a password</label>
                        <div class="input-group" id="show_hide_password">
                            <input class="form-control" type="password">
                            <div class="input-group-append">
                                <a href="" class="input-group-text" style="padding-right: 20px; padding-left: 20px;"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <br>
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Save All</button>

                </div>
            </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>



        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password input').attr("type") == "text"){
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass( "fa-eye-slash" );
                $('#show_hide_password i').removeClass( "fa-eye" );
            }else if($('#show_hide_password input').attr("type") == "password"){
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass( "fa-eye-slash" );
                $('#show_hide_password i').addClass( "fa-eye" );
            }
        });

        // this is the id of the form
        $("#idForm").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    alert(data); // show response from the php script.
                }
            });


        });

        var geocoder;
        var map;
        var marker;
        var defZoom = 14;
        var defLat = 23.91631269240717;
        var defLon = 90.39773035134272;

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
                icon: {
                    url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                    anchor: new google.maps.Point(63, 63)
                },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
                    icon: {
                        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                        anchor: new google.maps.Point(63, 63)
                    },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
