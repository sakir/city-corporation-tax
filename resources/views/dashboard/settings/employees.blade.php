@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | ZoneAdmin <small> (  Add New ZoneAdmin )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/zones') }}" class="btn pull-right hidden-sm-down btn-success"> Add New ZoneAdmin </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->


        <div class="row">
            <div class="col-7">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Admin list :</h4>

                        <!-- ===== Alert Message showing ====== -->
                        @if(Session::has('success'))
                            <p class="alert alert-{{ 'success' }}">{!! Session::get('success') !!}</p>
                        @endif

                        <table id="employees_table" data-select="" class="table table-striped table-hover col_1ast_right">
                            <thead>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Admin Name</th>
                                <th>Admin Role</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Admin Name</th>
                                <th>Admin Role</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

            </div>
            <div class="col-5">
                <div class="card">
                    <div class="card-header">

                        <h4 class="card-title">Add New ZoneAdmin :</h4>
                    </div>
                    <div class="card-body">

                        <!-- ===== Alert Message showing ====== -->
                        @foreach (['danger', 'warning', 'success', 'info'] as $key)
                            @if(Session::has($key))

                                <div class="toast {{ $key }}" >
                                    <div class="toast-header">
                                        <div class="lbl_color rounded mr-2"></div>
                                        <strong class="mr-auto">{{ $key }}</strong>

                                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                            <span aria-hidden="true"> &times; </span>
                                        </button>
                                    </div>
                                    <div class="toast-body">
                                        {!! Session::get($key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach


                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('settings.employee.add.post') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="employee_name">ZoneAdmin Name</label>
                                <input type="text" class="form-control" id="employee_name" name="name"
                                       placeholder="ZoneAdmin Name">
                            </div>
                            <div class="form-group">
                                <label for="employee_email">ZoneAdmin Email</label>
                                <input type="text" class="form-control border border-dark" id="employee_email" name="email"
                                       placeholder="ZoneAdmin Email">
                            </div>
									 <div class="form-row">
										<div class="col">
											 <div class="form-group">
												  <label for="password">Password</label>
												  <input type="text" class="form-control border border-dark" id="password" name="password"
															placeholder="Password">
											 </div>
										</div>
										<div class="col">
											 <div class="form-group">
												  <label for="conf_password">Confirm Password</label>
												  <input type="text" class="form-control border border-dark" id="conf_password" name="conf_password"
															placeholder="Confirm Password">
											 </div>
										</div>
									 </div>
                            <div class="form-group">
                                <label for="zone_id">Zone</label>
										  <select name="zone_id" class="form-control form-control-lg" id="">
											<option value=""> -- Select the Zone -- </option>
											  @foreach( $zones as $zone_k => $zone_v )
												<option value="{{ $zone_v->id }}"> Zone # {{ $zone_v->id }} </option>
											  @endforeach
										  </select>
                            </div>
                            
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-zones);
        function load_all_zones(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "name"},
                {"data": "usertype"},
                {"data": "zone"},
                {"data": "action"}
            ];

            var jsonDataURL = base_url + "settings/dt-ajax-all-employees/";

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_employee_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this Employee informations !",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_zones('#employees_table');

    </script>

@endsection
