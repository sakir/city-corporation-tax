@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | রোড <small> (  নতুন যুক্ত করুন  )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ route('settings.roads') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">সকল রোডের তালিকা :</h4>

                        <!-- ===== Alert Message showing ====== -->
                        @if(Session::has('success'))
                            <p class="alert alert-{{ 'success' }}">{!! Session::get('success') !!}</p>
                        @endif

                        <table id="roads_table" data-select="" class="table table-striped table-hover col_3_center col_4_center col_1ast_right">
                            <thead>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Road Name</th>
                                <th>ward</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Road Name</th>
                                <th>ward</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">

                        <h4 class="card-title">নতুন রোড যুক্ত করুন :</h4>
                    </div>
                    <div class="card-body">

                        <!-- ===== Alert Message showing ====== -->
                        @foreach (['danger', 'warning', 'success', 'info'] as $key)
                            @if(Session::has($key))

                                <div class="toast {{ $key }}" >
                                    <div class="toast-header">
                                        <div class="lbl_color rounded mr-2"></div>
                                        <strong class="mr-auto">{{ $key }}</strong>

                                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                            <span aria-hidden="true"> &times; </span>
                                        </button>
                                    </div>
                                    <div class="toast-body">
                                        {!! Session::get($key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach


                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('settings.roads.post') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="road_name">রোড নাম</label>
                                <input type="text" class="form-control form-control-lg" id="road_name" name="road_name"
                                       placeholder="রোড নাম">
                            </div>
                            <div class="form-row">
										<div class="col">
											<label for=""> &nbsp; </label>
											<h1><span class="badge badge-primary">জোন <b>{{  Auth::user()->permission_id }}</b></span></h1>
										</div>
										<div class="col">
                                <label for="ward_id">Select Under Word</label>
											<select name="ward_id" id="ward_id" class="form-control form-control-lg">
												 <option value="0"> ওয়ার্ড সিলেক্ট  করুন</option>
												@foreach( $wards as $ward_k => $ward_v )
												 <option value="{{ $ward_v->ward_id }}"> ওয়ার্ড: {{ $ward_v->ward_number }} </option>
												@endforeach
											</select>
											<script>document.getElementById('').value = '';</script>
										</div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-wards);
        function load_all_wards(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "road_name"},
                {"data": "ward"},
                {"data": "zone"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = base_url + "settings/dt-ajax-all-roads/" + selID

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_ward_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this ward!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_wards('#roads_table');

        var geocoder;
        var map;
        var marker;
        var defZoom = 14;
        var defLat = 23.91631269240717;
        var defLon = 90.39773035134272;

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
                icon: {
                    url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                    anchor: new google.maps.Point(63, 63)
                },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
                    icon: {
                        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                        anchor: new google.maps.Point(63, 63)
                    },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
