@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | Holding  <small> (  ট্যেক্স প্রদান )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/holding-add') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <?php
        /*
        echo '<pre>';
        print_r($wards);
        echo '</pre>';
        */
        ?>

        <div class="card" >
            <div class="card-block">
                <form action="{{ route('settings.holdings.addnew') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-8" >

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <label for="zone_id">সিলেক্টেড জোন: </label>
                                        <select name="zone_id" id="zone_id" class="form-control form-control-lg">
                                            <option value="{{ $holding->zone_id }}"> Zone: {{ $holding->zone_name }} </option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="ward_id">সিলেক্টেড ওয়ার্ড: </label>
                                        <select name="ward_id" id="ward_id" class="form-control form-control-lg">
                                            <option value="{{ $holding->ward_id }}"> Ward: {{ $holding->ward_name }} </option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="holding_no">হোল্ডিং নং</label>
                                            <input type="text" class="form-control form-control-lg text-center font-weight-bold border border-dark" name="holding_no" id="holding_no" value="{{ $holding->holding_no }}">
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <!--
                                <button class="btn btn-block btn-lg btn-primary"> Submit to Save </button>
                                -->

                                <?php
                                /*
                                echo '<pre>';
                                    print_r($holding);
                                echo '</pre>';
                                */
                                ?>
                            </div>


                            <?php
                            $grand_due = 0;
                            foreach( $payment_history as $ph_y_k => $ph_y_v ){

                                $have_to_pay = (($holding->yearly_amount * 1) * ($holding->yearly_percentage * 1)) / 100;
                                $total_payed_amt = 0;
                                foreach( $ph_y_v as $ph_k => $ph_v ){
                                    $total_payed_amt += $ph_v->payment_amount;
                                }
                                $grand_due += $have_to_pay - $total_payed_amt;
                            }
                            ?>
                            <table id="" class="table text-white bg-secondary table-striped col_1st_left col_2_right col_1ast_right">
                                <thead>
                                <tr>
                                    <th>Total Due:</th>
                                    <th><b><?=$grand_due?></b> Tk.</th>
                                </tr>
                                </thead>
                            </table>
                            <?php
                            $grand_due = 0;
                            foreach( $payment_history as $ph_y_k => $ph_y_v ){ ?>

                            <table id="payment_history_table" class="table table-striped table-bordered table-sm col_2_right col_1ast_right mb-0">
                                <thead class="text-white bg-secondary">
                                <tr class="text-white bg-secondary">
                                    <th class="pb-0" colspan="2"><h4 class="font-italic text-center text-white mb-0"><?=$ph_y_k?></h4></th>
                                </tr>
                                <tr>
                                    <th><small>Date</small></th>
                                    <th><small>Amount</small></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $have_to_pay = (($holding->yearly_amount * 1) * ($holding->yearly_percentage * 1)) / 100;
                                $total_payed_amt = 0;
                                foreach( $ph_y_v as $ph_k => $ph_v ){
                                $total_payed_amt += $ph_v->payment_amount;
                                ?>
                                <tr>
                                    <td><small><?= date( 'd-M-Y', strtotime( $ph_v->payment_datetime ) ) ?></small></td>
                                    <td><?= $ph_v->payment_amount ?> Tk.</td>
                                </tr>
                                <?php	} ?>
                                </tbody>
                            </table>

                            <table id="" class="table bg-secondary text-white table-striped table-bordered table-sm col_1st_right col_2_right col_1ast_right">
                                <thead>
                                <tr>
                                    <th><small>Have to pay</small></th>
                                    <th><small>Payed Amount</small></th>
                                    <th><small>Due</small></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="text-white bg-secondary">
                                    <td><?= $have_to_pay ?> Tk.</td>
                                    <td><?= $total_payed_amt ?> Tk.</td>
                                    <td><?= $have_to_pay - $total_payed_amt ?> Tk.</td>
                                </tr>
                                </tbody>
                            </table>

                            <?php
                            $grand_due += $have_to_pay - $total_payed_amt;
                            } ?>
                        </div>

                        <div class="col-4" >

                            <h3>ম্যাপ লোকেশন: </h3>
                            <hr>
                            <div class="form-row">
                                <div class="col-5">
                                    <label for="latitude">Latitude</label>
                                    <input type="text" id="latitude" name="latitude"
                                           class="form-control form-control-sm code_text"
                                           placeholder="#Latitude" value="" readonly>
                                </div>
                                <div class="col-5">
                                    <label for="longitude">Longitude</label>
                                    <input type="text" id="longitude" name="longitude"
                                           class="form-control form-control-sm code_text"
                                           placeholder="#Longitude" readonly>
                                </div>
                                <div class="col-2">
                                    <label for="zoom">Zoom</label>
                                    <input type="text" id="zoom" name="zoom" class="form-control form-control-sm code_text"
                                           placeholder="#Zoom"
                                           readonly>
                                </div>
                            </div>
                            <div id="map_canvas" style="width: 100%; height: 400px;"></div>
                            <br>
                            <label for="description">আরো বিস্তারিত/বিবরনঃ  </label>
                            <textarea name="description" id="description" class="form-control" rows="6">{{ $holding->description }}</textarea>
                            <hr>
                            <button class="btn btn-lg btn-primary btn-block" name=""> <i class="fa fa-floppy-o" ></i> &nbsp; Update / পরিবর্তন করুন  </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        function asset_sqrft_and_ret_amt_calculate(sqrFeet, assetCatClass){
            var totalAmt = (sqrFeet * 1) * (assetCatClass * 1);
            $('#asset_amount').val(totalAmt);
        }
        function gandTotalAmt(totalAmt, assetUnitQty){
            var grandTotalAmt = (totalAmt * 1) * (assetUnitQty * 1);
            $('#asset_gtotal_amt').val(grandTotalAmt);
        }

        function percentageWithGandTotalAmt(gTotalAmt, percentage){
            var percentageAmt = (gTotalAmt * percentage) / 100;
            $('#tax_gtotal_amt').val(percentageAmt);
        }


        $('#per_sql_feet').val( $('#asset_cat_class').find(':selected').data('squarefeet_rate') );
        $('.inp_percentage').val( $('#asset_category').find(':selected').data('percentage') );
        asset_sqrft_and_ret_amt_calculate($('#sqr_feet').val(), $('#per_sql_feet').val());
        gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );
        percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());


        $( '#asset_cat_class' ).on('change', function(){
            $('#per_sql_feet').val( $(this).find(':selected').data('squarefeet_rate') );
            asset_sqrft_and_ret_amt_calculate($('#sqr_feet').val(), $('#per_sql_feet').val());
            gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );

            // === For grand percentage amount
            percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
        });

        $( '#sqr_feet' ).on('change,blur, keyup', function(){
            asset_sqrft_and_ret_amt_calculate($('#sqr_feet').val(), $('#per_sql_feet').val());
            gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );

            // === For grand percentage amount
            percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
        });

        $( '#asset_qty' ).on('change,blur, keyup, mouseup', function(){
            gandTotalAmt($('#asset_amount').val(),$('#asset_qty').val() );

            // === For grand percentage amount
            percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
        });

        $( '#asset_category' ).on('change', function(){
            //	console.log( $(this).find(':selected').data('percentage') );
            $('.inp_percentage').val( $(this).find(':selected').data('percentage') );

            // === For grand percentage amount
            percentageWithGandTotalAmt($('.inp_percentage').val(),$('#asset_gtotal_amt').val());
        });

        /* ================= Google Map ================= */

        var geocoder;
        var map;
        var marker;
        var defZoom = 14;
        var defLat = 23.9413210;
        var defLon = 90.3844680;

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
                //    icon: {
                //        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                //        anchor: new google.maps.Point(63, 63)
                //    },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
                    //     icon: {
                    //         url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                    //         anchor: new google.maps.Point(63, 63)
                    //     },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize"></script>
@endsection
