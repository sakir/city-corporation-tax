@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | ZoneAdmin  <small> ( Edit )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/zones') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->


        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-block">
							  <div class="card-body">
								  
									<!-- ===== Alert Message showing ====== -->
									@foreach (['danger', 'warning', 'success', 'info'] as $key)
										 @if(Session::has($key))

											  <div class="toast {{ $key }}" >
													<div class="toast-header">
														 <div class="lbl_color rounded mr-2"></div>
														 <strong class="mr-auto">{{ $key }}</strong>

														 <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
															  <span aria-hidden="true"> &times; </span>
														 </button>
													</div>
													<div class="toast-body">
														 {!! Session::get($key) !!}
													</div>
											  </div>
										 @endif
									@endforeach


									@if ($errors->any())
										 <div class="alert alert-danger">
											  <ul>
													@foreach ($errors->all() as $error)
														 <li>{{ $error }}</li>
													@endforeach
											  </ul>
										 </div>
									@endif
							  
									<form action="{{ route('settings.user.edit.post', ['id'=>$zoneadmin->id]) }}" method="post">
										 @csrf
										 <div class="form-group">
											  <label for="employee_name">ZoneAdmin Name</label>
											  <input type="text" class="form-control" id="employee_name" name="name"
														placeholder="ZoneAdmin Name" value="{{ $zoneadmin->name }}">
										 </div>
										 <div class="form-group">
											  <label for="employee_email">ZoneAdmin Email</label>
											  <input type="text" class="form-control border border-dark" id="employee_email" name="email"
														placeholder="ZoneAdmin Email" value="{{ $zoneadmin->email }}">
										 </div>
										 <div class="form-row">
											<div class="col">
												 <div class="form-group">
													  <label for="password">Setup New Password</label>
													  <input type="text" class="form-control border border-dark" id="password" name="password"
																placeholder="Password" >
												 </div>
											</div>
											<div class="col">
												 <div class="form-group">
													  <label for="conf_password">Confirm Password</label>
													  <input type="text" class="form-control border border-dark" id="conf_password" name="conf_password"
																placeholder="Confirm Password" >
												 </div>
											</div>
										 </div>
										 <div class="form-group">
											  <label for="zone_id">Zone</label>
											  <select name="zone_id" class="form-control form-control-lg" id="zone_id">
												<option value=""> -- Select the Zone -- </option>
												  @foreach( $zones as $zone_k => $zone_v )
													<option value="{{ $zone_v->id }}"> Zone # {{ $zone_v->id }} </option>
												  @endforeach
											  </select>
											  <script>document.getElementById('zone_id').value = '{{ $zoneadmin->permission_id }}';</script>
										 </div>
										 
										 <button type="submit" class="btn btn-lg btn-primary btn-block">Update</button>
									</form>

							  </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
	 
    </script>
@endsection
