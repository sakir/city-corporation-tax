@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-6 col-8 align-self-center">
				 <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | ট্যাক্স প্রস্তুতিকরন রেকর্ডস </h3>
			</div>
			<div class="col-md-6 col-4 align-self-center">
				 <a href="{{ url('settings/wards') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
			</div>
		</div>

		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->

		<div class="row">
			<div class="container-fluid">
				 <div class="card">
					  <div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th>অর্থ বছর</th>
									<th>মোট পাওনা ট্যাক্স</th>
									<th></th>
								</tr>
							</thead>
							@foreach( $fyear_tax_gener_records as $yrec_k => $yrec_v )
							<tr>
								<td><h3><a href="{{ route('reports.fiscal-year', ['fyear'=>$yrec_v->fiscal_year]) }}">{{ $yrec_v->fiscal_year }}</a></h3></td>
								<td><h3>{{ number_format($yrec_v->tax_percent_amt_total, 2, '.', ',') }} টাকা </h3></td>
								<td>
									<a href="{{ route('reports.fiscal-year', ['fyear'=>$yrec_v->fiscal_year]) }}" class="btn btn-outline-primary">বিস্তারিত</a>
								</td>
							</tr>
							@endforeach
						</table>
					  </div>
				 </div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
    </script>
@endsection
