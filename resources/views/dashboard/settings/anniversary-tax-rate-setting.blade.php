@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | পঞ্চবার্ষিকী ট্যাক্স রেট সেটিংস </h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/wards') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
			
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
			
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
								@if($annivers_asset_cat_class_rate)
									<b>Edit</b><a href="{{ url('settings/anniversary-tax-rate') }}" class="btn btn-sm btn-danger float-right">X</a>
									<form action="{{ route('settings.anniversary-tax-rate-edit.cat-class-rate.update', ['id'=> $annivers_asset_cat_class_rate->id]) }}" method="post">
									<table class="table table-dark  col_3_right col_1ast_right">
									 
									  <tbody>
											 <tr class="">
												<td>{{ $annivers_asset_cat_class_rate->ar_title }}<br>
													{{ date('Y', strtotime($annivers_asset_cat_class_rate->ar_from)) }} - {{ date('Y', strtotime($annivers_asset_cat_class_rate->ar_to)) }}
												</td>
												<td><b>{{ $annivers_asset_cat_class_rate->asset_cat_class_title }}</b></td>
												<td>
													<div class="input-group">
													  <input type="text" class="form-control text-right" name="squarefeet_rate" placeholder="%" value="{{ $annivers_asset_cat_class_rate->squarefeet_rate }}" size="1">
													  <div class="input-group-append">
														 <span class="input-group-text" id="basic-addon2">Tk.</span>
													  </div>
													</div>
												</td>
												<td>
													<button href="{{ url('settings/anniversary-tax-rate/asset-cat-class-rate-update') }}" class="btn btn-primary btn-sm">Save</button>
												</td>
											 </tr>
									  </tbody>
									</table>
									</form>
								@endif
								<table class="table table-striped col_3_right col_1ast_right">
								  <thead>
										 <tr>
											<td>বার্ষিকী</td>
											<td>সম্পত্তির ধরন</td>
											<td>প্রতি স্কয়ার ফিট টাকা</td>
											<td></td>
										 </tr>
								  <thead>
								  <tbody>
										@foreach( $anniversary_asset_cat_class_rate as $rate_k => $rate_v )
											 <tr>
												<td>{{ $rate_v->ar_title }}<br>
													{{ date('Y', strtotime($rate_v->ar_from)) }} - {{ date('Y', strtotime($rate_v->ar_to)) }}
												</td>
												<td>{{ $rate_v->asset_cat_class_title }}</td>
												<td>{{ $rate_v->squarefeet_rate }} <small>টাকা</small></td>
												<td>
												@if( $rate_k < 4 )
													<a href="{{ url('settings/anniversary-tax-rate/asset-cat-class-rate/' . $rate_v->cat_class_rate_id) }}" class="btn btn-outline-primary btn-sm">Edit</a>
												@endif
												</td>
											 </tr>
										@endforeach
								  </tbody>
								</table>
                        <form action="{{ route('settings.wards.post') }}" method="post">
                            @csrf
									 
									<label for="inputEmail4">পঞ্চবার্ষিকী সিলেক্ট: </label>
									<select name="" id="anniversary_range" class="form-control form-control-lg">
										<option> -- পঞ্চবার্ষিকী সিলেক্ট -- </option>
										@foreach( $anniversary_range as $ar_k => $ar_v )
												<option value="{{ $ar_v->ar_id }}"> {{ $ar_v->ar_title . ' . ' . date('Y', strtotime($ar_v->ar_from)) . '-' . date('Y', strtotime($ar_v->ar_to)) }} </option>
										@endforeach
									</select>
									<script>/* document.getElementById('anniversary_range').value = ''; */</script>
									
									<br>
									<br>
									<div class="form-row">
										<div class="col">
											<label for="inputEmail4">Asset Category: </label>
											<select name="" id="anniversary_range" class="form-control form-control-lg">
												<option> -- Select Asset Category -- </option>
												@foreach( $asset_category as $ac_k => $ac_v )
														<option value="{{ $ac_v->asset_category_id }}"> {{ $ac_v->asset_category_title }} </option>
												@endforeach
											</select>
											<script>/* document.getElementById('anniversary_range').value = ''; */</script>
										</div>
										<div class="col">
											<label for="inputEmail4">Percentage: </label>
											<div class="input-group">
											  <input type="text" class="form-control form-control-lg text-right" placeholder="%" >
											  <div class="input-group-append">
												 <span class="input-group-text" id="basic-addon2"> <i class="fa fa-percent"></i> </span>
											  </div>
											</div>
										</div>
									</div>
                            <br>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
								@if($annivers_asset_cat_percentage)
									<?php // echo '<pre>';print_r($annivers_asset_cat_percentage);echo '</pre>'; ?>
									<b>Edit</b><a href="{{ url('settings/anniversary-tax-rate') }}" class="btn btn-sm btn-danger float-right">X</a>
									<form action="{{ route('settings.anniversary-tax-rate-edit.category-percentage.update', ['id'=> $annivers_asset_cat_percentage->id]) }}" method="post">
									<table class="table table-dark  col_3_right col_1ast_right">
									  <tbody>
											 <tr class="">
												<td>{{ $annivers_asset_cat_percentage->ar_title }}<br>
													{{ date('Y', strtotime($annivers_asset_cat_percentage->ar_from)) }} - {{ date('Y', strtotime($annivers_asset_cat_percentage->ar_to)) }}
												</td>
												<td><b>{{ $annivers_asset_cat_percentage->asset_category_title }}</b></td>
												<td>
													<div class="input-group mb-3">
													  <input type="text" class="form-control text-right" name="percentage" placeholder="%" value="{{ $annivers_asset_cat_percentage->percentage }}" size="1">
													  <div class="input-group-append">
														 <span class="input-group-text" id="basic-addon2">%</span>
													  </div>
													</div>
												</td>
												<td>
													<button href="{{ url('settings/anniversary-tax-rate/asset-cat-percentage-update') }}" class="btn btn-primary btn-sm">Save</button>
												</td>
											 </tr>
									  </tbody>
									</table>
									</form>
								@endif
								<table class="table table-striped col_3_right col_1ast_right">
								  <thead>
										 <tr>
											<td>বার্ষিকী</td>
											<td>সম্পত্তির উদ্দেশ্য</td>
											<td>শতাংশ</td>
											<td></td>
										 </tr>
								  <thead>
								  <tbody>
										@foreach( $anniversary_asset_cat_percentage as $rate_k => $rate_v )
											 <tr>
												<td>{{ $rate_v->ar_title }}<br>
													{{ date('Y', strtotime($rate_v->ar_from)) }} - {{ date('Y', strtotime($rate_v->ar_to)) }}
												</td>
												<td>{{ $rate_v->asset_category_title }}</td>
												<td>{{ $rate_v->percentage }}<small>%</small></td>
												<td>
												@if( $rate_k < 3 )
													<a href="{{ url('settings/anniversary-tax-rate/asset-cat-percentage/' . $rate_v->cat_percentage_id) }}" class="btn btn-outline-primary btn-sm">Edit</a>
												@endif
												</td>
											 </tr>
										@endforeach
								  </tbody>
								</table>
                        <!-- ===== Alert Message showing ====== -->
                        @foreach (['danger', 'warning', 'success', 'info'] as $key)
                            @if(Session::has($key))

                                <div class="toast {{ $key }}" >
                                    <div class="toast-header">
                                        <div class="lbl_color rounded mr-2"></div>
                                        <strong class="mr-auto">{{ $key }}</strong>

                                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                            <span aria-hidden="true"> &times; </span>
                                        </button>
                                    </div>
                                    <div class="toast-body">
                                        {!! Session::get($key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        @if ($errors->any())
                           <div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
                           </div>
                        @endif

                        <form action="{{ route('settings.wards.post') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="ward_name">ward Name</label>
                                <input type="text" class="form-control form-control-lg" id="ward_name" name="ward_name"
                                       placeholder="ward Name">
                            </div>
                            <br>
                            <br>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-wards);
        function load_all_wards(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "ward_name"},
                {"data": "zone"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = base_url + "settings/dt-ajax-all-wards/" + selID

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_ward_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this ward!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_wards('#wards_table');

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
