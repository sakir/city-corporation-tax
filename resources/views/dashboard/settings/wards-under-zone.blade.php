@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
               <h3 class="text-themecolor m-b-0 m-t-0">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-step-backward" aria-hidden="true"></i> Back</a> &nbsp;
						<small>SuperAdmin</small> | ওয়ার্ড সমূহ <small></small>
					</h3>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->


			<div class="card">
				 <div class="card-body ">
					<table class="container-fluid">
						<tr>
							<td>
								<img src="{{ url('/') }}/dashboard/images/favicon.png" class="img-fluid">
							</td>
							<td>
								<h1 class="">গাজীপুর সিটি কর্পোরেশন</h1>
								<hr>
								<h2 class="">জোনঃ <b>{{ $zone->name }} </b> এর ওয়ার্ড সমূহ</h2>
                                <hr>
                                <a href="#" class="btn btn-outline-info "> <i class="fa fa-wrench"></i> &nbsp; Tax Rate Settings</a>
							</td>
						</tr>
					</table>
				 </div>
			</div>

			<div class="row justify-content-md-center">
				@foreach($wards as $ward_k => $ward_v)
				  <div class="col-lg-3 col-sm-6">
						<?php
						/*
							echo '<pre>';
							print_r($ward_v);
							echo '</pre>';
						*/
						?>
						<div class="card">
							 <div class="card-body">
								  <a href="{{ route('settings.holdings.under-zone-ward', ['zid'=>$ward_v->zone_id,'wid'=>$ward_v->ward_id] ) }}" class="btn btn-outline-primary btn-block btn-lg"> <i> Ward <b>{{ $ward_v->ward_number }}</b> </i></a>
							 </div>
						</div>
				  </div>
				@endforeach
			</div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-zones);
        function load_all_zones(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "name"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = base_url + "settings/dt-ajax-all-zones/" + selID

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_zone_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this Zone!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_zones('#zones_table');

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
