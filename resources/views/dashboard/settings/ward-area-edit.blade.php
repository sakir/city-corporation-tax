@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | মহল্লা  <small> (  মহল্লা সংশোধন )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ route('settings.areas') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-7">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">সকল মহল্লার তালিকা :</h4>

                        <table id="ward_areas_table" data-select="" class="table table-striped table-hover col_1ast_right">
                            <thead>
										 <tr class="bg-info text-white">
											  <th>SL</th>
											  <th>Area Name</th>
											  <th>Ward</th>
											  <th>Zone</th>
											  <th>Action</th>
										 </tr>
                            </thead>
                            <tbody></tbody>
										 <tr class="table-warning">
											  <th> * </th>
											  <th><b>{{ $the_area->area_name }}</b> </th>
											  <th>{{ $the_area->ward_number }} </th>
											  <th>{{ $the_area->name }} </th>
											  <th class="text-right">
													<a href="{{ route('settings.ward.edit', ['id' => $the_area->area_id]) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
											  </th>
										 </tr>
                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Area Name</th>
                                <th>Ward</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-5">
                <div class="card" style="background-color: #ffeeba;">
                    <div class="card-header" style="background-color: #ffeeba;">

                        <h4 class="card-title">মহল্লা সংশোধন :</h4>
                    </div>
                    <div class="card-body">
							
                        <!-- ===== Alert Message showing ====== -->
                        @foreach (['danger', 'warning', 'success', 'info'] as $key)
                            @if(Session::has($key))

                                <div class="toast {{ $key }}" >
                                    <div class="toast-header">
                                        <div class="lbl_color rounded mr-2"></div>
                                        <strong class="mr-auto">{{ $key }}</strong>

                                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                            <span aria-hidden="true"> &times; </span>
                                        </button>
                                    </div>
                                    <div class="toast-body">
                                        {!! Session::get($key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
								
                        <form action="{{ route('settings.area.update', ['id'=> $the_area->area_id]) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="ward_name">মহল্লা নাম</label>
                                <input type="text" class="form-control form-control-lg" id="area_name" name="area_name"
                                       placeholder="মহল্লা নাম" value="{{ $the_area->area_name }}">
                            </div>
                            <div class="form-row">
										<div class="col">
											<label for="ward_name"> &nbsp; </label>
											<h1><span class="badge badge-primary">জোন <b>{{  Auth::user()->permission_id }}</b></span></h1>
										</div>
										<div class="col">
                                <label for="ward_name">Select Under Word</label>
											<select name="ward_id" id="ward_id" class="form-control form-control-lg">
												 <option value="0"> ওয়ার্ড সিলেক্ট  করুন</option>
												@foreach( $wards as $ward_k => $ward_v )
												 <option value="{{ $ward_v->ward_id }}"> ওয়ার্ড: {{ $ward_v->ward_number }} </option>
												@endforeach
											</select>
											<script>document.getElementById('ward_id').value = '{{ $the_area->ward_id }}';</script>
										</div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-wards);
        function load_all_areas(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "area_name"},
                {"data": "ward"},
                {"data": "zone"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = base_url + "settings/dt-ajax-all-areas/" + selID

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_ward_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this ward!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_areas('#ward_areas_table');

    </script>

@endsection
