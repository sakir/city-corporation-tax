@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | বাৎসরিক ট্যেক্স প্রস্তুতিকরন </h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/wards') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> --- :</h4>
								<hr>
								<form action="{{ route('settings.fyear-holding-property-save-and-tax-gen-ready-req') }}" method="post">
									@csrf
									<div class="row">
										<div class="col-md-2">
											<div class="form-group text-center">
											  <label for="zone_id"> </label>
											  <h2>জোন : {{ Auth::user()->permission_id }}</h2>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="fyear">যে অর্থ বছরের জন্য </label>
												<select name="fyear" id="fyear" class="form-control form-control-lg">
													<option value=""> -- সিলেক্ট অর্থ বছর -- </option>
													<option value="2003-2004"> 2003-2004 </option>
													<option value="2004-2005"> 2004-2005 </option>
													<option value="2005-2006"> 2005-2006 </option>
													<option value="2006-2007"> 2006-2007 </option>
													<option value="2007-2008"> 2007-2008 </option>
													<option value="2008-2009"> 2008-2009 </option>
													<option value="2009-2010"> 2009-2010 </option>
													<option value="2010-2011"> 2010-2011 </option>
													<option value="2011-2012"> 2011-2012 </option>
													<option value="2012-2013"> 2012-2013 </option>
													<option value="2013-2014"> 2013-2014 </option>
													<option value="2014-2015"> 2014-2015 </option>
													<option value="2015-2016"> 2015-2016 </option>
													<option value="2016-2017"> 2016-2017 </option>
													<option value="2017-2018"> 2017-2018 </option>
													<option value="2018-2019"> 2018-2019 </option>
													<option value="2019-2020"> 2019-2020 </option>
													<option value="2020-2021"> 2020-2021 </option>
													<option value="2021-2022"> 2021-2022 </option>
												</select>
												<script>document.getElementById('fyear').value = '{{ $fyear ?? '' }}';</script>
											</div>
										</div>
										<div class="col-md-3">
											  <label for=""> &nbsp; </label>
											  <button type="submit" class="btn btn-outline-primary btn-lg btn-block" name=""> <i class="fa fa-cogs m-r-10" aria-hidden="true"></i> Ready to Generate </button>
										</div>
									</div>
								</form>
								<hr>
								<div class="row justify-content-md-center">
									<div class="col-8">
										@if(isset($selected_holding_data))
											<h2 class="text-center">মোট <b>{{ $selected_holding_data['selected_zone_count'] }}</b> টি হোল্ডিং সনাক্ত করা হয়েছে  এই({{ $fyear }}) অর্থবছরের ট্যেক্স বিল প্রস্তুত করার জন্য।</h2>
											<h4 class="text-center">( {{ $selected_holding_data['prev_record_info'] }} )</h4>
											<div class="row justify-content-md-center">
												<div class="col-6">
													<form action="{{ route('settings.fyear-holding-property-save-and-tax-gen-ready-confirm') }}" method="POST" id="tax_gen_ready_confirm">
														@csrf
														<button name="tax_gen_confirm" id="tax_gen_confirm" type="submit" value="{{ $holding_tax_bill_generated_record->id }}" class="btn btn-primary btn-block btn-lg"> YES CONFIRM  </button>
														<input type="hidden" value="{{ json_encode($selected_holding_data['detected_holdings']) }}" id="total_holdings_jason">
														<input type="hidden" value="0" id="targeted_holding_index">
														<input type="hidden" value="{{ $selected_holding_data['selected_zone_count'] }}" id="selected_holding_count">
														<input type="hidden" value="0" id="done_percentage" class="form-control form-control-lg">
													</form>
													
												</div>
											</div>
											<br><br>
											<h5 id="timeToEnd"></h5>
											<div class="progress border border-success" style="height: 50px;">
											  <div class="progress-bar progress-bar-striped bg-success percentage_print font-weight-bold" role="progressbar" style="width: 0%;font-size: 20px;" aria-valuemin="0" aria-valuemax="100">0%</div>
											</div>
											<br><br>
										@endif
									</div>
								</div>
                    </div>
                </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
		
@endsection

@section('js_script')
    <script>
	 
	 
		$(document).ready(function () {

		@if(isset($selected_holding_data))
			function ajax_loop_tax_gen(){
				
				$.ajax({
					url: '{{ route("settings.fyear-holding-property-save-and-tax-gen-ready-confirm") }}',
					method: 'POST',
					type: 'POST',
					startTime: performance.now(),
					data: {
								'_token':$("input[name=_token]").val(),
								'tax_gen_confirm': $('#tax_gen_confirm').val(),
							//	'total_holdings_jason': $('#total_holdings_jason').val(),
								'targeted_holding_index': $('#targeted_holding_index').val(),
							//	'selected_holding_count': $('#selected_holding_count').val()
							},
					success: function(ret) {
							ret = JSON.parse(ret);
						//	console.log(ret);
							if( ret.status == 'success' ){
							//	console.log('after success: ',ret);
								if( ret.done_percentage <= 100 ){
									$('#done_percentage').val(ret.done_percentage);
									$('.percentage_print').text(parseInt(ret.done_percentage) + '%').width(ret.done_percentage + '%');
									$('#targeted_holding_index').val(ret.done_holding_index*1 + 1);
									
									var incompleteItems = ('{{ $selected_holding_data["selected_zone_count"] }}'*1) - (ret.done_holding_index*1 + 1);
									console.log('incompleteItems: ',incompleteItems);
								  //Calculate the difference in milliseconds.
								  var time = performance.now() - this.startTime;
						 
								  //Convert milliseconds to seconds.
								  var seconds = time / 1000;
						 
								  //Round to 3 decimal places.
								  seconds = seconds.toFixed(3);
									
									var totalSecondsToEnd = incompleteItems * seconds;
									if( totalSecondsToEnd > 60 ){
										var totalMinutes = totalSecondsToEnd / 60;
										if( totalMinutes > 1 ){
											var timeToEnd = totalMinutes.toFixed(0) + ' minutes left.';
										}else{
											var timeToEnd = totalMinutes.toFixed(0) + ' minute left.';
										}
									}else{
										if( totalSecondsToEnd > 1 ){
											var timeToEnd = totalSecondsToEnd.toFixed(0) + ' seconds left.';
										}else{
											var timeToEnd = totalSecondsToEnd.toFixed(0) + ' second left.';
										}
									}
									$('#timeToEnd').text(timeToEnd);
									if( ret.next_holding_id == 'end' ){
										$('#tax_gen_confirm').html(
											'YES DONE'
										).prop('disabled', true);
										return false;
									}
									
									ajax_loop_tax_gen();
								}
							}else{
								$('#done_percentage').val(ret.done_percentage + ' Error showing !!');
							}
						//	$("body").html(text+"<h1>done!</h1>")
										
					}
				});
				
			}
			
			$('#tax_gen_ready_confirm').on('submit', function(e){
				e.preventDefault();
				$('#tax_gen_confirm').html(
					'<span class="spinner-border spinner-border" role="status" aria-hidden="true"></span> &nbsp; ' +
					'YES CONFIRM'
				).prop('disabled', true);
				ajax_loop_tax_gen();
			});

		@endif
		});
    </script>
@endsection
