@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-step-backward" aria-hidden="true"></i> Back</a> &nbsp; 
						<small>SuperAdmin</small> | হোল্ডিং সমূহ 
					</h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/wards') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
				
                <div class="card">
                    <div class="card-block">
                        <h2 class="card-title text-center"> জোনঃ <b>{{ $selected_zone_no }}</b> &nbsp; এর &nbsp;  ওয়ার্ডঃ  <b>{{ $selected_ward_no }}</b></h2>

                        <!-- ===== Alert Message showing ====== -->
                        @if(Session::has('success'))
                            <p class="alert alert-{{ 'success' }}">{!! Session::get('success') !!}</p>
                        @endif

                        <table id="holdings_table" data-select="" class="table table-striped table-hover col_4_right col_5_right col_6_right col_1ast_right">
                            <thead>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>হোল্ডিং</th>
                                <th>মালিকের নাম</th>
                                <th>ওয়ার্ড-জোন</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>হোল্ডিং</th>
                                <th>মালিকের নাম</th>
                                <th>ওয়ার্ড-জোন</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection


@php $dt_ajax_action = URL('/') . '/settings/dt-ajax-all-holdings/';  @endphp
@if( $selected_zone_no != '' && $selected_ward_no == '' )
   @php $dt_ajax_action = URL('/') . '/settings/dt-ajax-all-holdings/' . $selected_zone_no;  @endphp
@elseif( $selected_zone_no != '' && $selected_ward_no != '')
	@php $dt_ajax_action = URL('/') . '/settings/dt-ajax-all-holdings/' . $selected_zone_no . '/' . $selected_ward_no;  @endphp
@endif

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-wards);
        function load_all_wards(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "holding_no"},
                {"data": "owner_name"},
                {"data": "ward_zone"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = '{{ $dt_ajax_action ?? '' }}';

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_ward_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this ward!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_wards('#holdings_table');

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
