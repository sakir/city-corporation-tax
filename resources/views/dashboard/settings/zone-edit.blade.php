@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | Zones  <small> (  Zine edit )</small></h3>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <a href="{{ url('settings/zones') }}" class="btn pull-right hidden-sm-down btn-success"> Add New </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->


        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Zone list :</h4>

                        <table id="zones_table" data-select=""
                               class="table table-striped table-hover col_1ast_right">
                            <thead>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Zone Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tr class="table-warning">
                                <th> * </th>
                                <th>{{ $the_zone->name }} </th>
                                <th class="text-right">
                                    <a href="{{ route('settings.zone.edit', ['id' => $the_zone->id]) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </th>
                            </tr>

                            <tfoot>
                            <tr class="bg-info text-white">
                                <th>SL</th>
                                <th>Zone Name</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

            </div>
            <div class="col-6">
                <div class="card" style="background-color: #ffeeba;">
                    <div class="card-header" style="background-color: #ffeeba;">

                        <h4 class="card-title">Edit Zone :</h4>
                    </div>
                    <div class="card-body">

                        <!-- ===== Alert Message showing ====== -->
                        @foreach (['danger', 'warning', 'success', 'info'] as $key)
                            @if(Session::has($key))

                                <div class="toast {{ $key }}" >
                                    <div class="toast-header">
                                        <div class="lbl_color rounded mr-2"></div>
                                        <strong class="mr-auto">{{ $key }}</strong>

                                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                            <span aria-hidden="true"> &times; </span>
                                        </button>
                                    </div>
                                    <div class="toast-body">
                                        {!! Session::get($key) !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('settings.zone.update', ['id'=>$the_zone->id]) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="zone_name">Zone Name</label>
                                <input type="text" class="form-control form-control-lg" id="zone_name" name="zone_name"
                                       placeholder="Zone Name" value="{{ $the_zone->name }}">
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col-5">
                                    <label for="latitude">Latitude</label>
                                    <input type="text" id="latitude" name="latitude"
                                           class="form-control form-control-sm"
                                           placeholder="#Latitude" readonly value="{{ $lat }}">
                                </div>
                                <div class="col-5">
                                    <label for="longitude">Longitude</label>
                                    <input type="text" id="longitude" name="longitude"
                                           class="form-control form-control-sm"
                                           placeholder="#Longitude" readonly value="{{ $lon }}">
                                </div>
                                <div class="col-2">
                                    <label for="zoom">Zoom</label>
                                    <input type="text" id="zoom" name="zoom" class="form-control form-control-sm"
                                           placeholder="#Zoom"
                                           readonly value="{{ $zoom }}">
                                </div>
                            </div>
                            <div id="map_canvas" style="width: 100%; height: 400px;"></div>
                            <br>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Save</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-zones);
        function load_all_zones(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "name"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = "{{ route('settings.zones.dt-ajax-zone-select',['id' => $the_zone->id]) }}";

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                console.log('selectedRow', result.select);

                var tblRowLength = $("select[name='zones_table_length']").val();

                if (selectedRow < tblRowLength - 1) {
                    selectedRow = (selectedRow - 3) * 0;
                }

                console.log('selectedRow__', result.select);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    "columns": data,
                    "preDrawCallback": function (settings) {

                    },
                    "rowCallback": function (row, data) {

                    },
                    "initComplete": function (settings, json) {

                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_zones('#zones_table');


        var geocoder;
        var map;
        var marker;
        var defZoom = {{ $zoom }};
        var defLat = {{ $lat }};
        var defLon = {{ $lon }};

        $('#zoom').val(defZoom);
        $('#latitude').val(defLat);
        $('#longitude').val(defLon);

        function handleEvent(event) {
            console.log(map.getZoom());
            document.getElementById('latitude').value = event.latLng.lat();
            document.getElementById('longitude').value = event.latLng.lng();
        }

        function initialize() {
            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(defLat, defLon),
                    zoom: defZoom,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // =============== Get zoom data
            google.maps.event.addListener(map, 'zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });

            // =============== Set default marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(defLat, defLon),
                draggable: true,
                icon: {
                    url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                    anchor: new google.maps.Point(63, 63)
                },
                map: map
            });


            // =============== Marker on Drag
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            //    console.log(map.getZoom());

            // =============== Marker setup onclick
            google.maps.event.addListener(map, "click", function (e) {

                latLng = e.latLng;
                $('#latitude').val(e.latLng.lat());
                $('#longitude').val(e.latLng.lng());

                // if marker exists and has a .setMap method, hide it
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    radius: 100,
                    draggable: true,
                    icon: {
                        url: base_url + 'dashboard/images/Ripple-1.6s-126px.gif',
                        anchor: new google.maps.Point(63, 63)
                    },
                    map: map
                });
                map.panTo(latLng);

                marker.addListener('drag', handleEvent);
                marker.addListener('dragend', handleEvent);
            });


        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
