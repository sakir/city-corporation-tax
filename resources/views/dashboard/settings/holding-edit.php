<?php $this->load->view('header'); ?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row page-titles">
      <div class="col-md-6 col-8 align-self-center">
         <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | Holding Edit</h3>
      </div>
      <div class="col-md-6 col-4 align-self-center">
         <a href="<?= base_url('superadmin/holding') ?>" class="btn pull-right hidden-sm-down btn-success"> Add new Holding </a>
      </div>
   </div>
   <!-- ============================================================== -->
   <!-- End Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
   <?php
//	pre($holding);
   $zone_name = '';
   $selected_ID = '';
   if (isset($wards)) {
		
   }
   ?>
   <div class="row">
		<div class="col-7" >
			<div class="card" >
				<div class="card-block">
		
					<div class="container-fluid">
						<div class="row">
							<div class="col">
								<label for="inputEmail4">Select the zone: </label>
								<select name="" class="form-control form-control-lg">
									<option> -- Select the Zone -- </option>
									<?php foreach ($zones as $pc_k => $pc_v) { ?>
										
										<option value="" <?php if( $holding->zone_id == $pc_v->zone_id ){ echo 'selected'; } ?>> <?php echo $pc_v->zone_name; ?> </option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col">
								<label for="inputEmail4">Select the Ward: </label>
								<select name="" class="form-control form-control-lg">
									<option> -- Select the Ward -- </option>
									<?php foreach ($wards as $pc_k => $pc_v) { ?>
										
										<option value="" <?php if( $holding->ward_id == $pc_v->ward_id ){ echo 'selected'; } ?>> <?php echo $pc_v->ward_name; ?> </option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<hr>
						
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<label for="holding_no">Holding No.</label>
									<input type="text" class="form-control form-control-lg" name="holding_no" id="holding_no" placeholder="..." value="<?= $holding->holding_no ?>">
								</div>
							</div>
						</div>
				
						<div class="form-row">
							<div class="col">
								<label for="owner_name">Owner Name: </label>
								  <input type="text" class="form-control" name="owner_name" id="owner_name" value="<?= $holding->owner_name ?>">
							</div>
							<div class="col">
								<label for="owner_fathers_name">Owner father's name: </label>
								  <input type="text" class="form-control" name="owner_fathers_name" id="owner_fathers_name" value="<?= $holding->owner_fathers_name ?>">
							</div>
						</div>
						<br>
						<div class="form-row">
							<div class="col">
								<label for="phone">Phone: </label>
								  <input type="text" class="form-control" name="phone" id="phone" value="<?= $holding->phone ?>">
							</div>
							<div class="col">
								<label for="email">Email: </label>
								  <input type="text" class="form-control" name="email" id="email" value="<?= $holding->email ?>">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-5">
								<label for="yearly_amount">Yearly Amount: </label>
								<div class="input-group mb-3">
								  <input type="number" class="form-control form-control-lg text-right" name="yearly_amount" id="yearly_amount" value="<?= $holding->yearly_amount ?>">
								  <div class="input-group-append">
									 <span class="input-group-text"> <b>Tk.</b> </span>
								  </div>
								</div>
								<label for="yearly_percentage">Yearly Percentage: </label>
								<div class="input-group mb-3">
								  <input type="text" class="form-control form-control-lg text-right" name="yearly_percentage" id="yearly_percentage" value="<?= $holding->yearly_percentage ?>">
									<div class="input-group-append">
									 <span class="input-group-text"> &nbsp; <i class="fa fa-percent" aria-hidden="true"></i> &nbsp; </span>
									</div>
								</div>
							</div>
							<div class="col">
								<label for="measurement_of_home">Description (Measurement of Home) : </label>
								<textarea name="" id="" class="form-control" rows="6"><?= $holding->measurement_of_home ?></textarea>
							</div>
						</div>
						<hr>
						<!--
						<button class="btn btn-block btn-lg btn-primary"> Submit to Save </button>
						-->
					</div>
								
				</div>
			</div>
      </div>
		
		<div class="col-5" >
			<div class="card" >
				<div class="card-block">
					<h3>Payment History</h3>
					<?php // pre($payment_history); ?>
					
					<?php 
						$grand_due = 0;
						foreach( $payment_history as $ph_y_k => $ph_y_v ){ 
						
							$have_to_pay = (($holding->yearly_amount * 1) * ($holding->yearly_percentage * 1)) / 100;
							$total_payed_amt = 0;
							foreach( $ph_y_v as $ph_k => $ph_v ){ 
								$total_payed_amt += $ph_v->payment_amount;
							}
							$grand_due += $have_to_pay - $total_payed_amt;
						}
					 ?>
						<table id="" class="table text-white bg-secondary table-striped col_1st_left col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th>Total Due:</th>
										 <th><b><?=$grand_due?></b> Tk.</th>
									</tr>
							  </thead>
						</table>
					<?php 
						$grand_due = 0;
						foreach( $payment_history as $ph_y_k => $ph_y_v ){ ?>		
						
						<table id="payment_history_table" class="table table-striped table-bordered table-sm col_2_right col_1ast_right mb-0">	
							  <thead class="text-white bg-secondary">							
									<tr class="text-white bg-secondary">
										 <th class="pb-0" colspan="2"><h4 class="font-italic text-center text-white mb-0"><?=$ph_y_k?></h4></th>
									</tr>
									<tr>
										 <th><small>Date</small></th>
										 <th><small>Amount</small></th>
									</tr>
							  </thead>
							  <tbody>
								<?php 
									$have_to_pay = (($holding->yearly_amount * 1) * ($holding->yearly_percentage * 1)) / 100;
									$total_payed_amt = 0;
									foreach( $ph_y_v as $ph_k => $ph_v ){ 
										$total_payed_amt += $ph_v->payment_amount;
									?>									
									<tr>
										 <td><small><?= date( 'd-M-Y', strtotime( $ph_v->payment_datetime ) ) ?></small></td>
										 <td><?= $ph_v->payment_amount ?> Tk.</td>
									</tr>
								<?php	} ?>
							  </tbody>
						 </table>
						
						<table id="" class="table bg-secondary text-white table-striped table-bordered table-sm col_1st_right col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th><small>Have to pay</small></th>
										 <th><small>Payed Amount</small></th>
										 <th><small>Due</small></th>
									</tr>
							  </thead>
							  <tbody>								
									<tr class="text-white bg-secondary">
										 <td><?= $have_to_pay ?> Tk.</td>
										 <td><?= $total_payed_amt ?> Tk.</td>
										 <td><?= $have_to_pay - $total_payed_amt ?> Tk.</td>
									</tr>
							  </tbody>
						 </table>
						
					<?php	
						$grand_due += $have_to_pay - $total_payed_amt;
						} ?>
						
						<table id="" class="table text-white bg-secondary table-striped col_1st_left col_2_right col_1ast_right">	
							  <thead>	
									<tr>
										 <th>Total Due:</th>
										 <th><b><?=$grand_due?></b> Tk.</th>
									</tr>
							  </thead>
						</table>
						<a href="<?= base_url('/') ?>superadmin/holding_notice_to_pay/edit/<?=$holding->id?>" class="btn btn-block btn-lg btn-outline-primary"> <i class="fa fa-print" aria-hidden="true"></i> &nbsp; Notefication Print out</a>
						<button class="btn btn-block btn-lg btn-outline-primary"> <i class="fa fa-mobile" aria-hidden="true"></i> <i class="fa fa-bell" aria-hidden="true"></i> &nbsp; Send notification SMS</button>
				</div>
			</div>
		</div>
	</div>
   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<?php $this->load->view('footer'); ?>
<script>
	$(document).ready( function () {
	//	 $('#payment_history_table').DataTable();
	} );
</script>