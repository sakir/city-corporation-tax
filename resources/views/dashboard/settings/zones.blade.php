@extends('layouts-dashboard.general')

@section('content')

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0"><small>SuperAdmin</small> | All Zones <small></small></h3>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

		
			<div class="card">
				 <div class="card-body ">
					<table class="container-fluid">
						<tr>
							<td>
								<img src="{{ url('/') }}/dashboard/images/favicon.png" class="img-fluid">
							</td>
							<td>
								<h1 class="">গাজীপুর সিটি কর্পোরেশন</h1>
								<h3 class="">জোন সমূহ</h3>
								</td>
						</tr>
					</table>
				 </div>
			</div>
				  
        <div class="row">
			  <div class="col">
					<div class="card">
						 <div class="card-body ">
							<img src="{{ url('/') }}/dashboard/images/gazipur-city-zones.jpg" class="img-fluid">
						 </div>
					</div>
			  </div>
			  <div class="col">
				  <div class="row">
						@foreach($zones as $zone_k => $zone_v)
						  <div class="col-lg-6 col-sm-6">
								<div class="card">
									 <div class="card-body">
										  <a href="{{ route('settings.wards.under-zone', ['zid'=>$zone_v->id]) }}" class="btn btn-outline-primary btn-block btn-lg"> <i> Zone <b>{{ $zone_v->name }}</b> </i></a>
									 </div>
								</div>
						  </div>
						@endforeach
				  </div>
			  </div>
		  </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('js_script')
    <script>
        //    alert(url + "settings/dt-ajax-all-zones);
        function load_all_zones(tbl_ID) {

            var data = [
                {"data": "sl"},
                {"data": "name"},
                {"data": "action"}
            ];

            var selID = $(tbl_ID).data('select');
            var jsonDataURL = base_url + "settings/dt-ajax-all-zones/" + selID

            $.getJSON(jsonDataURL, function (result) {

                var selectedRow = result.select - 1;

                if (selectedRow < 9) {
                    selectedRow = (selectedRow - 3) * 0;
                }
                //	console.log(selectedRow);

                $(tbl_ID).DataTable({
                    'ajax': jsonDataURL,
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': true,
                    'displayStart': selectedRow,
                    "columns": data,
                    "initComplete": function (settings, json) {
                        $('.selected').closest('tr').addClass('table-warning');

                        $('.form_zone_remove').on('click', function (e) {

                            e.preventDefault();
                            var thisForm = $(this).parents('form');

                            Swal.fire({
                                title: 'Are you sure?',
                                text: "Once deleted, you will not be able to recover this Zone!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#F62D51',
                                cancelButtonColor: '#7460EE',
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if (result.value) {
                                    thisForm.submit();
                                }
                            });

                        });
                    }
                });
            });

            var rowIndex = 0;
        }

        load_all_zones('#zones_table');

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmnI7-QHCE5Kw-tq8FmfKaKKDGRZTemOE&callback=initialize">
    </script>
@endsection
