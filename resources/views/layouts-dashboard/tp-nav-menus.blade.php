
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
   <!-- Sidebar scroll-->
   <div class="scroll-sidebar">
		
      <!-- Sidebar navigation-->
      <nav class="sidebar-nav">
         <ul id="sidebarnav" class="">

            <li class="">
               <a href="#" class="waves-effect"><i class="fa fa-tachometer m-r-10" aria-hidden="true"></i>Dashboard Home</a>
            </li>
				@if( Auth::user()->usertype == 'superadmin' )
             <li class="">
                 <a href="{{ route('settings.zones') }}" class="waves-effect"><i class="fa fa-map-marker m-r-10" aria-hidden="true"></i>জোন সেটিংস</a>
             </li>
             <li class="">
                 <a href="{{ route('settings.wards') }}" class="waves-effect"><i class="fa fa-map-marker m-r-10" aria-hidden="true"></i>ওয়ার্ড সেটিংস</a>
             </li>
				@endif
             <li class="">
                 <a href="{{ route('settings.areas') }}" class="waves-effect"><i class="fa fa-map-marker m-r-10" aria-hidden="true"></i>মহল্লা সেটিংস</a>
             </li>
             <li class="">
                 <a href="{{ route('settings.roads') }}" class="waves-effect"><i class="fa fa-road m-r-10" aria-hidden="true"></i>রোড সেটিংস</a>
             </li>
             <li class="">
                 <a href="{{ route('settings.holdings') }}" class="waves-effect"><i class="fa fa-home m-r-10" aria-hidden="true"></i>সকল হোল্ডিং লিস্ট</a>
             </li>
             <li class="">
                 <a href="{{ route('settings.anniversary-tax-rate') }}" class="waves-effect"><i class="fa fa-money" aria-hidden="true"></i>পঞ্চবার্ষিকী ট্যাক্স রেট সেটিংস</a>
             </li>
             <li class="">
                 <a href="{{ route('settings.fyear-holding-property-save-and-tax-generate') }}" class="waves-effect"><i class="fa fa-money" aria-hidden="true"></i>বাৎসরিক ট্যেক্স প্রস্তুতিকরন</a>
             </li>
             <li class="">
						<a href="{{ route('settings.fyear-tax-generate-records') }}" class="waves-effect">
							<i class="fa fa-list m-r-10" aria-hidden="true"></i>ট্যেক্স প্রস্তুতিকরন রেকর্ডস
						</a>
             </li>
             <li class="">
						<a href="{{ route('dashboard.tax-collection') }}" class="waves-effect">
							<i class="fa fa-money m-r-10" aria-hidden="true"></i>ট্যেক্স আদায়
						</a>
             </li>
				@if( Auth::user()->usertype == 'superadmin' )
					<li class="">
						<a href="{{ route('settings.users') }}" class="waves-effect"><i class="fa fa-users m-r-10" aria-hidden="true"></i> User Settings</a>
					</li>
				@endif
            <li class="">
               <hr>
            </li>
             <li class="">
                 <a href="#" class="waves-effect"><i class="fa fa-black-tie m-r-10" aria-hidden="true"></i>Employees</a>
                 <ul class="">
                     <li class="">
                         <a href="{{ route('settings.employees') }}" class="waves-effect"><i class="fa fa-users m-r-10" aria-hidden="true"></i>All Employees</a>
                     </li>
                     <li class="">
                         <a href="{{ route('settings.employee.add') }}" class="waves-effect"><i class="fa fa-user-plus m-r-10" aria-hidden="true"></i>Add New Employee</a>
                     </li>
                 </ul>
             </li>
            <li class="">
               <a href="#" class="waves-effect"><i class="fa fa-list m-r-10" aria-hidden="true"></i>My Projects</a>
            </li>
            <li class="">
               <a href="#" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Profile</a>
            </li>
             <!--
            <li class="">
               <a href="#" class="waves-effect"><i class="fa fa-wrench m-r-10" aria-hidden="true"></i>Settings</a>
               <ul class="">
                    <li class="">
                       <a href="{{ route('settings.zones') }}" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Zones</a>
                    </li>
                    <li class="">
                       <a href="{{ route('settings.employees') }}" class="waves-effect"><i class="fa fa-black-tie m-r-10" aria-hidden="true"></i>Employees</a>
                    </li>
               </ul>
            </li>
             -->

         </ul>
         <div class="text-center m-t-30">
            <a href="{{ url('/') }}auth/logout" class="btn btn-danger"><i class="fa fa-circle-o-notch m-r-10" aria-hidden="true"></i>LogOut</a>
         </div>
      </nav>
      <!-- End Sidebar navigation -->
   </div>
   <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
