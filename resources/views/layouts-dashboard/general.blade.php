<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}dashboard/images/favicon.png">

      <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/') }}dashboard/images/favicons/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/') }}dashboard/images/favicons/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/') }}dashboard/images/favicons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/') }}dashboard/images/favicons/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}dashboard/images/favicons/favicon-16x16.png">
      <link rel="manifest" href="{{ asset('/') }}dashboard/images/favicons/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="{{ asset('/') }}dashboard/images/favicons/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">

		<title>argh!</title>
		<script type="text/javascript">
			message = "Gazipur City Corporation | ";
			function step() {
				message = message.substr(1) + message.substr(0,1);
				document.title = message.substr(0,15);
			}
		</script>
	</head>


      <!-- Scripts -->
        <script>
            var base_url = "{{ url('/') }}/";
        //    alert(base_url);
        </script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

      <!-- Bootstrap Core CSS -->
      <link href="{{ asset('/') }}dashboard/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="{{ asset('/') }}dashboard/css/style.css" rel="stylesheet">

       <!-- DataTables -->
       <link rel="stylesheet" type="text/css" href="{{ asset('/') }}DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css"/>
       <link rel="stylesheet" type="text/css" href="{{ asset('/') }}DataTables/Buttons-1.6.1/css/buttons.bootstrap4.min.css"/>


      <!-- You can change the theme colors from here -->
      <link href="{{ asset('/') }}dashboard/css/colors/blue.css" id="theme" rel="stylesheet">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't  work if you view the page via file: ' // -->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

       <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

      <!--
      [if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]
      -->

       <link href="{{ asset('/') }}dashboard/css/custom-style.css?{{ rand(0,999) }}" id="theme" rel="stylesheet">
   </head>
   <body class="fix-header card-no-border" onload="setInterval(step,100)">
      <!-- ============================================================== -->
      <!-- Preloader - style you can find in spinners.css -->
      <!-- ============================================================== -->
      <div class="preloader">
         <svg class="circular" viewBox="25 25 50 50">
         <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
      </div>
      <!-- ============================================================== -->
      <!-- Main wrapper - style you can find in pages.scss -->
      <!-- ============================================================== -->
      <div id="main-wrapper">
         <!-- ============================================================== -->
         <!-- Topbar header - style you can find in pages.scss -->
         <!-- ============================================================== -->
         <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-lg navbar-light">
               <!-- ============================================================== -->
               <!-- Logo -->
               <!-- ============================================================== -->
               <div class="navbar-header">
                  <div class="container-fluid">
                     <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon -->
                        <b>
                           <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                           <!-- Dark Logo icon -->
                           <img src="{{ asset('/') }}dashboard/images/logo_main-h.png" alt="homepage" class="dark-logo img-responsive" />
                        </b>
                        <!--End Logo icon --></a>
                  </div>
               </div>
               <!-- ============================================================== -->
               <!-- End Logo -->
               <!-- ============================================================== -->
               <div class="collapse navbar-collapse">
                  <!-- ============================================================== -->
                  <!-- toggle and nav items -->
                  <!-- ============================================================== -->
                  <ul class="navbar-nav mr-auto mt-md-0 ">
                     <!-- This is  -->
                     <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                     <li class="nav-item hidden-sm-down">
                        <form class="app-search p-l-20">
                           <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a>
                        </form>
                     </li>
                  </ul>
                  <!-- ============================================================== -->
                  <!-- User profile and search -->
                  <!-- ============================================================== -->
                  <ul class="navbar-nav my-lg-0">
						@if( Auth::user()->usertype != 'superadmin' )
                     <li class="nav-item"> <a class="nav-link"><b>Zone {{ Auth::user()->permission_id }}</b></a> </li>
						@endif
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('/') }}/dashboard/images/users/profile-pic.jpg" alt="user" class="profile-pic m-r-5" />{{ Auth::user()->name }}</a>

                        <div class="dropdown-menu dropdown-menu-right">
                           <a href="{{ asset('/') }}auth/logout" class="waves-effect dropdown-item"><i class="fa fa-circle-o-notch m-r-10" aria-hidden="true"></i>LogOut</a>
                        </div>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- ============================================================== -->
         <!-- End Topbar header -->
         <!-- ============================================================== -->
         @include('layouts-dashboard.tp-nav-menus')
         <!-- ============================================================== -->
         <!-- Page wrapper  -->
         <!-- ============================================================== -->
         <div class="page-wrapper">

            @yield('content')

            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
               -- Development going on --
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
         </div>
         <!-- ============================================================== -->
         <!-- End Page wrapper  -->
         <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Wrapper -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- All Jquery -->
      <!-- ============================================================== -->
      <script src="{{ asset('/') }}dashboard/plugins/jquery/jquery.min.js" type="text/javascript" ></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{ asset('/') }}dashboard/plugins/bootstrap/js/tether.min.js"></script>
      <script src="{{ asset('/') }}dashboard/plugins/bootstrap/js/bootstrap.min.js"></script>
      <!-- slimscrollbar scrollbar JavaScript -->
		
      <script src="{{ asset('/') }}dashboard/js/jquery.slimscroll.js"></script>
		
      <!--Wave Effects -->
      <script src="{{ asset('/') }}dashboard/js/waves.js"></script>
      <!--Menu sidebar -->
      <script src="{{ asset('/') }}dashboard/js/sidebarmenu.js"></script>
      <!--stickey kit -->
      <script src="{{ asset('/') }}dashboard/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
      <!--Custom JavaScript -->
      <script src="{{ asset('/') }}dashboard/js/custom.min.js"></script>
      <!-- ============================================================== -->
      <!-- Style switcher -->
      <!-- ============================================================== -->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.3.5/dist/sweetalert2.all.min.js"></script>

      <!-- DataTables -->
      <script type="text/javascript" src="{{ asset('/') }}DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="{{ asset('/') }}DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
      <script type="text/javascript" src="{{ asset('/') }}DataTables/Buttons-1.6.1/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="{{ asset('/') }}DataTables/Buttons-1.6.1/js/buttons.bootstrap4.min.js"></script>
      <script type="text/javascript" src="{{ asset('/') }}DataTables/Buttons-1.6.1/js/buttons.print.min.js"></script>

      <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
      <script src="{{ asset('/') }}dashboard/js/locationpicker.jquery.js"></script>

      <script src="{{ asset('/') }}dashboard/plugins/sweetalert.min.js"></script>
      <script src="{{ asset('/') }}dashboard/js/mt-custom.js"></script>
      <script>

          function taxRateAjaxCal(actionFrom,zone_id,anniversary_range_id,asset_category_id,asset_cat_class_id,asset_cat_using_id){
					var thisEle = actionFrom;
					var asseAmount = 0;
				//	console.log('asset_category_id:',asset_category_id);
              $.ajax({
                  method: 'POST',
                  type: 'POST',
                  url:'{{ route("settings.zone.ajax-per-square-feet-ret") }}',
                  dataType: 'JSON',
                  data:{
                      _token: $('meta[name="csrf-token"]').attr('content'),
                      asset_category_id:asset_category_id,
                      asset_cat_class_id:asset_cat_class_id,
                      asset_cat_using_id:asset_cat_using_id,
                      zone_id:zone_id,
                      anniversary_range_id:anniversary_range_id
                  },
                  success: function(res){
							
                  //    console.log(res);
						
                      if( res.status  ){
								 
                          thisEle.find('.per_sql_feet').val(res.data.per_square_feet_rate);
								
								  var asseAmount = asset_sqrft_and_ret_amt_calculate(thisEle.find('.sqr_feet').val(), thisEle.find('.per_sql_feet').val());
								  
                          thisEle.find('.asset_amount').val(Number(asseAmount.toFixed(2)));
								  
								  thisEle.find('.asset_gtotal_amt').val( gandTotalAmt(asseAmount, thisEle.find('.asset_qty').val()) );
								  
								  var taxGtotalAmt = percentageWithGandTotalAmt( thisEle.find('.inp_percentage_b').val(), thisEle.find('.asset_gtotal_amt').val() );
								  
                          thisEle.find('.tax_gtotal_amt').val(taxGtotalAmt);
								  grandTotalTaxToPay();
                      }else{
                          thisEle.find('.per_sql_feet').val(0);
                          thisEle.find('.asset_amount').val(0);
								  thisEle.find('.asset_gtotal_amt').val(0);
                      }
							
                  //    $('#per_sql_feet').val(res.per_square_feet_rate);
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  }
              });
          }
      </script>
        @yield('js_script')
   </body>
</html>
