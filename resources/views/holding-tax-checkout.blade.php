@extends('layouts-front.general')

@section('content')

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
	 

  <section class="hero-banner magic-ball">
    <div class="container">
			<div class="row">

				<table class="table table-striped ">
				@foreach( $fyear_asset_records as $fyear_records_k => $fyear_records_v )
					
						<tr>
							<td>
								@foreach( $fyear_records_v['asset_records'] as $asset_k => $asset_v )
								
									<div class="card border mb-2">
									  <div class="card-body">
											<p class="h6">
												<span class="badge badge-secondary">{{ $asset_v->asset_category_title }}</span> | 
												<span class="badge badge-secondary">{{ $asset_v->asset_cat_class_title }}</span> | 
												<span class="badge badge-secondary">{{ $asset_v->asset_cat_using_title }}</span>
											</p>
											<p class="h6"> 
												স্কয়ার ফিট <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->square_feet) }}</span> | 
												প্রতি স্কয়ার ফিট মুল্যমান :  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->squarefeet_rate) }} টাকা </span> | 
												ফ্লোর/ফ্ল্যাট সংখ্যা:  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->floor_quantity) }} টি </span> 
											</p>
											<p class="h6"> 
												সর্বমোট সম্পত্তি টাকা পরিমানঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_asset_amount) }} টাকা </span> | 
												সম্পত্তির ওপর কর ধার্যকৃত শতাংশঃ  <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->percentage_rate) }} % </span>
											</p>
											<p class="h5">এই সম্পত্তির ধার্যকৃত ট্যেক্সঃ <span class="badge badge-secondary">{{ BngConv::en2bn($asset_v->total_percentage_rate_payable) }}</span> টাকা</p>
										
									  </div>
									</div>
								@endforeach
							</td>
							<td>
								<p class="h2 border text-center p-2">হোল্ডিং নং<strong class="form-control form-control-lg text-center font-weight-bold border border-dark h2" > {{ $holding->zone }} {{ $holding->ward_number }} {{ $holding->holding_no }}</strong></p>
								<p class="h3 border text-center border-dark p-2">{{ $fyear_records_v['fyear'] }}</p>
								
								<p class="h5"> মোট ধার্যকৃত ট্যেক্সঃ <span class="badge badge-primary">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount']) }}</span> টাকা</p>
								<p class="h6"> মোট পরিশোধিত <span class="badge badge-success">{{ BngConv::en2bn($fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা</p>
								<p class="h6"> মোট অপরিশোধিত <span class="badge badge-danger">{{ BngConv::en2bn($fyear_records_v['fyear_total_tax_amount'] - $fyear_records_v['fyear_tax_total_payment']) }}</span> টাকা</p> 
								@php
								$total_unpaid = $fyear_records_v['fyear_total_tax_amount'] - $fyear_records_v['fyear_tax_total_payment'];
								@endphp 
								<div class="progress  bg-danger">
								  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{ $fyear_records_v['payment_percentage'] }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<br>
								@if( count($fyear_records_v['fyear_tax_payment_records']) )
								<table class="table table-sm">
									<thead>
										<tr>
											<th colspan="2">ট্যেক্স পরিশোধ তালিকাঃ  </th>
										</tr>
										<tr>
											<th>সময়</th>
											<th>টাকা</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $fyear_records_v['fyear_tax_payment_records'] as $paid_k => $paid_v )
											<tr>
												<td><em>{{ $paid_v->payment_datetime }} </em> <span class="badge badge-secondary">{{ $paid_v->payment_by }} </span> </td>
												<td> {{ BngConv::en2bn(round($paid_v->payment_amount*1, 2)) }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
								
								@endif
							</td>
						</tr>
					
					{{-- print_r($fyear_records_v) --}}
					
				@endforeach
				</table>
				
					<?php 
				//	echo '<pre>';
				//	print_r($payment_history); 
				//	echo '</pre>';
					?>
					<br>
			</div>		
			<hr>
			<form method="POST" class="needs-validation" novalidate>
			 <div class="row">
				  <div class="col-md-4 order-md-2 mb-4">
						<h4 class="d-flex justify-content-between align-items-center mb-3">
							 <span class="text-muted">টাকার পরিশোধ করতেঃ </span>
						</h4>
						<ul class="list-group mb-3">
							 <li class="list-group-item d-flex justify-content-between lh-condensed">
									<input type="number" min="10" name="amount" id="total_amount" class="form-control form-control-lg text-center font-weight-bold border border-dark" required value="{{ $total_unpaid }}" />
										<div class="input-group-prepend border border-dark border-left-0 font-weight-bold">
											 <span class="input-group-text"> টাকা </span>
										</div>
							 </li>
							 <li class="list-group-item d-flex justify-content-between lh-condensed">

								 <button class="btn btn-primary btn-lg btn-block" id="sslczPayBtn" style="width: 100%;"
											token="if you have any token validation"
											postdata="your javascript arrays or objects which requires in backend"
											order="If you already have the transaction generated for current order"
											endpoint="{{ url('/pay-via-ajax') }}"> Pay Now
								 </button>
							 </li>
						</ul>
				  </div>
				  <div class="col-md-8 order-md-1">
						<h4 class="mb-3">যার পক্ষ থেকে ট্যেক্স পরিশোধ করা হচ্ছে তাঁর পরিচিতিঃ </h4>
							 <div class="row">
								  <div class="col-md-12 mb-3">
										<label for="firstName">নামঃ</label>
										<input type="text" name="customer_name" class="form-control" id="customer_name" placeholder=""
												 value="John Doe" required>
										<div class="invalid-feedback">
											 Valid customer name is required.
										</div>
								  </div>
							 </div>

							 <div class="row">
								<div class="col-md-6">
								 <div class="mb-3">
									  <label for="mobile">ফোনঃ </label>
									  <div class="input-group">
											<div class="input-group-prepend">
												 <span class="input-group-text">+88</span>
											</div>
											<input type="text" name="customer_mobile" class="form-control" id="mobile" placeholder="Mobile"
													 value="01711xxxxxx" required>
											<div class="invalid-feedback" style="width: 100%;">
												 Your Mobile number is required.
											</div>
									  </div>
								 </div>
								</div>

								<div class="col-md-6">
								 <div class="mb-3">
									  <label for="email">ইমেলঃ  <span class="text-muted">(Optional)</span></label>
									  <input type="email" name="customer_email" class="form-control" id="email"
												placeholder="you@example.com" value="you@example.com" required>
									  <div class="invalid-feedback">
											Please enter a valid email address for shipping updates.
									  </div>
								 </div>
								</div>
							 </div>

							 <div class="mb-3">
								  <label for="address">ঠিকানাঃ </label>
								  <input type="text" class="form-control" id="address" placeholder="1234 Main St"
											value="93 B, New Eskaton Road" required>
								  <div class="invalid-feedback">
										Please enter your shipping address.
								  </div>
							 </div>

							 <div class="mb-3">
								  <label for="address2">ঠিকানাঃ ২ <span class="text-muted">(Optional)</span></label>
								  <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
							 </div>

							 <div class="row">
								  <div class="col-md-5 mb-3">
										<label for="country">Country</label>
										<select class="custom-select d-block w-100" id="country" required>
											 <option value="Bangladesh">Bangladesh</option>
										</select>
										<div class="invalid-feedback">
											 Please select a valid country.
										</div>
								  </div>
								  <div class="col-md-4 mb-3">
										<label for="state">State</label>
										<select class="custom-select d-block w-100" id="state" required>
											 <option value="Gazipur">Gazipur</option>
										</select>
										<div class="invalid-feedback">
											 Please provide a valid state.
										</div>
								  </div>
								  <div class="col-md-3 mb-3">
										<label for="zip">Zip</label>
										<input type="text" class="form-control" id="zip" placeholder="" required>
										<div class="invalid-feedback">
											 Zip code required.
										</div>
								  </div>
							 </div>
							 
							 <hr class="mb-4">
				  </div>
			 </div>
		</form>
	
    </div>
  </section>
   



@endsection