$( function() {

    $('button.close').on('click', function () {
        $(this).closest('.toast').fadeOut();
    });

    setTimeout(function() {
        $('.toast').fadeOut('slow');
    }, 10000);

} );

