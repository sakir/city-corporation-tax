<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //fillable
    protected $fillable = [
        'name',
        'google_map_data',
        'created_by',
        'last_updated_by',
        'status'
    ];

    public static function where(string $string, int $int)
    {
    }
	 	 
}
