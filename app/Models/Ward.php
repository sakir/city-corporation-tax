<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    //fillable
    protected $fillable = [
        'ward_name',
        'zone_id',
        'created_at'
    ];

    public static function where(string $string, int $int)
    {
    }
}
