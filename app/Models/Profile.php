<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //fillable
    protected $fillable = [
        'user_id',
        'phone',
        'nid',
        'photo',
        'address',
        'google_map_data',
        'facebook',
        'skype',
        'whatsapp',
        'imo',
        'more_detail',
        'zone_id',
        'joining_date',
        'designation',
        'created_by',
        'status'
    ];
}
