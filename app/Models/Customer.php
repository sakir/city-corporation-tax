<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //fillable
    protected $fillable = [
        'name',
        'phone',
        'email',
        'nid',
        'photo',
        'address',
        'google_map_data',
        'location_photo',
        'zone_id',
        'occupation',
        'opening_amount',
        'opening_amount_due',
        'bill_amount',
        'connection_srv_bill',
        'connection_srv_bill_due',
        'connection_date',
        'package_id',
        'ip',
        'is_reseller',
        'reseller_id',
        'entry_by',
        'status'
    ];
}
