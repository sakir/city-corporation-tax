<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holding extends Model
{
    //fillable
    protected $fillable = [
        'holding_no',
        'owner_name',
        'owner_fathers_name',
        'yearly_amount',
        'yearly_percentage',
        'measurement_of_home',
        'phone',
        'email',
        'ward_id',
        'area_id',
        'road_id',
        'status',
        'created_at'
    ];

    public static function where(string $string, int $int)
    {
    }
}
