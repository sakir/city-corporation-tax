<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //fillable
    protected $fillable = [
        'name',
        'phone',
        'email',
        'nid',
        'photo',
        'address',
        'google_map_data',
        'more_detail',
        'zone_id',
        'joining_date',
        'designation',
        'status'
    ];
}
