<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //fillable
    protected $fillable = [
        'pkg_title',
        'speed',
        'price',
        'duration',
        'description',
        'banner',
        'order_sl',
        'created_by',
        'status'
    ];
}
