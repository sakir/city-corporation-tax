<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WardArea extends Model
{
    //fillable
    protected $fillable = [
        'area_name',
        'ward_id',
        'created_at'
    ];

    public static function where(string $string, int $int)
    {
    }
}
