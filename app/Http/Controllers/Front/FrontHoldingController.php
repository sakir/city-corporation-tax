<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Dashboard\BasicController;

class FrontHoldingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['zones'] = Zone::all();

        return view('front.welcome', $data);
    }

    public function wards_under_zone($zone_id) {
        $data['zone'] = DB::table('zones')
                ->select('*')
                ->where('id', '=', $zone_id)
                ->first();
        $data['wards'] = DB::table('wards')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('wards.*', 'zones.name as zone')
                ->where('wards.zone_id', '=', $zone_id)
                ->get();
        //	dd($data['wards']);
        return view('front.wards-under-zone', $data);
    }

    public function holding_search($zid, $wid = null) {

        $data['roads'] = DB::table('roads')
                ->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('roads.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('wards.ward_id', '=', $wid)
                ->get();
        //	dd($data['roads']);
        $data['areas'] = DB::table('ward_areas')
                ->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('ward_areas.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('ward_areas.ward_id', '=', $wid)
                ->get();
        $data['selected_zone_no'] = $zid;
        $data['selected_ward_no'] = '';
        if ($wid != null) {
				$the_ward = DB::table('wards')
                ->where('ward_id', '=', $wid)
                ->first();
            $data['selected_ward_no'] = $the_ward->ward_number;
        }

        return view('front.holdings', $data);
    }



    public function per_square_feet_ret( $data = [
        'year' => 0,
        'zone_id' => 0,
        'asset_category_id' => 0,
        'asset_cat_class_id' => 0,
        'asset_cat_using_id' => 0
    ] ){

        $data_ret = DB::table('zone_tax_rate_settings')
            ->select(
                'zone_tax_rate_settings.*',
                'asset_category.asset_category_title',
                'asset_category.asset_category_slug',
                'asset_category_class.asset_cat_class_title',
                'asset_category_class.asset_cat_class_slug',
                'asset_category_of_using.asset_cat_using_title',
                'asset_category_of_using.asset_cat_using_slug',
                )
            ->join('zones', 'zone_tax_rate_settings.zone_id', '=', 'zones.id')
            ->join('asset_category', 'zone_tax_rate_settings.asset_category_id', '=', 'asset_category.asset_category_id')
            ->join('asset_category_class', 'zone_tax_rate_settings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
            ->join('asset_category_of_using', 'zone_tax_rate_settings.asset_cat_using_id', '=', 'asset_category_of_using.asset_cat_using_id')
            ->where('zone_tax_rate_settings.year', '=', $data['year'])
            ->where('zone_tax_rate_settings.zone_id', '=', $data['zone_id'])
            ->where('zone_tax_rate_settings.asset_category_id', '=', $data['asset_category_id'])
            ->where('zone_tax_rate_settings.asset_cat_class_id', '=', $data['asset_cat_class_id'])
            ->where('zone_tax_rate_settings.asset_cat_using_id', '=', $data['asset_cat_using_id'])
            ->first();
    //    dd($data);
        return $data_ret;
    }


    public function holding_details($id) {
		$basicCont = new BasicController();
      $data['zones'] = Zone::all()->sortByDesc("id");
		$holdings_total_tax_rec = $basicCont->holdings_total_due($id);
		$data['fyear_asset_records'] = $holdings_total_tax_rec['fyear_asset_records'];
		$data['holding'] = DB::table('holdings')
															->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
															->join('roads', 'holdings.ward_id', '=', 'roads.road_id')
															->join('zones', 'wards.zone_id', '=', 'zones.id')
															->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
															->select(
																		'holdings.*', 
																		'zones.name as zone',
																		'wards.ward_number as ward_number',
																		'ward_areas.area_name as area_name',
																		'roads.road_name',
																		)
															->where('holdings.id', '=', $id)
															->first();
															
        //	  		  */
        return view('front.holding-details', $data);
    }
	 
	 
	 
    public function holding_tax_checkout($holding_id) {
        $single_zone = Zone::find($holding_id);
        //    dd($single_zone->name);
        $data['the_zone'] = $single_zone;

        $data['holding'] = DB::table('holdings')
                ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->join('asset_category', 'holdings.asset_category_id', '=', 'asset_category.asset_category_id')
                ->join('asset_category_class', 'holdings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                ->join('anniversary_asset_cat_class_rate', 'holdings.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
                ->join('anniversary_asset_cat_percentage', 'holdings.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
                ->select(
                        'holdings.*',
                        'asset_category.*',
                        'anniversary_asset_cat_percentage.*',
                        'asset_category_class.*',
                        'anniversary_asset_cat_class_rate.*',
                        'holdings.id as holding_id',
                        'wards.ward_number',
                        'zones.name as zone_name',
                        'zones.id as zone_id',
                        'ward_areas.area_name',
                        'roads.road_name'
                )
                ->where('holdings.id', '=', $holding_id)
                ->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
                ->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
                ->first();

        return view('front.tax-checkout', $data);
    }

    public function holding_taxpay_success() {
        $data = [''];

        $val_id = urlencode($_POST['val_id']);
        $store_id = urlencode("allex5e13792d31125");
        $store_passwd = urlencode("allex5e13792d31125@ssl");
        $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=" . $val_id . "&store_id=" . $store_id . "&store_passwd=" . $store_passwd . "&v=1&format=json");

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $requested_url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

        $result = curl_exec($handle);

        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($code == 200 && !( curl_errno($handle))) {

            # TO CONVERT AS ARRAY
            # $result = json_decode($result, true);
            # $status = $result['status'];
            # TO CONVERT AS OBJECT
            $result = json_decode($result);
            /*
              # TRANSACTION INFO
              $status = $result->status;
              $tran_date = $result->tran_date;
              $tran_id = $result->tran_id;
              $val_id = $result->val_id;
              $amount = $result->amount;
              $store_amount = $result->store_amount;
              $bank_tran_id = $result->bank_tran_id;
              $card_type = $result->card_type;

              # EMI INFO
              $emi_instalment = $result->emi_instalment;
              $emi_amount = $result->emi_amount;
              $emi_description = $result->emi_description;
              $emi_issuer = $result->emi_issuer;

              # ISSUER INFO
              $card_no = $result->card_no;
              $card_issuer = $result->card_issuer;
              $card_brand = $result->card_brand;
              $card_issuer_country = $result->card_issuer_country;
              $card_issuer_country_code = $result->card_issuer_country_code;

              # API AUTHENTICATION
              $APIConnect = $result->APIConnect;
              $validated_on = $result->validated_on;
              $gw_version = $result->gw_version;
             */
            DB::table('tax_payments')->insert(
                    [
                        'holding_id' => isset($_GET['holding_id']) ?? 0,
                        'payment_amount' => ($result->store_amount * 1) - 0.2,
                        'sslcz_tran_date' => $result->tran_date,
                        'sslcz_tran_id' => $result->tran_id,
                        'sslcz_val_id' => $result->val_id,
                        'sslcz_amount' => $result->amount,
                        'sslcz_store_amount' => $result->store_amount,
                        'sslcz_response_json' => json_encode($result),
                        'payment_datetime' => date('Y-m-d H:i:s'),
                        'received_by' => 'sslcommerz.com'
                    ]
            );
            $data['result'] = $result;
        } else {

            echo "Failed to connect with SSLCOMMERZ";
        }

        return view('front.taxpay-success', $data);
    }
	 
}
