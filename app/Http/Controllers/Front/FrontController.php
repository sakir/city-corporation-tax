<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;

class FrontController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['zones'] = Zone::all();

        return view('front.welcome', $data);
    }

    public function wards_under_zone($zone_id) {
        $data['zone'] = DB::table('zones')
                ->select('*')
                ->where('id', '=', $zone_id)
                ->first();
        $data['wards'] = DB::table('wards')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('wards.*', 'zones.name as zone')
                ->where('wards.zone_id', '=', $zone_id)
                ->get();
        //	dd($data['wards']);
        return view('front.wards-under-zone', $data);
    }

    public function holdings_under_zone_ward($zone_id) {
        $data['zone'] = DB::table('zones')
                ->select('*')
                ->where('id', '=', $zone_id)
                ->first();
        $data['wards'] = DB::table('wards')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('wards.*', 'zones.name as zone')
                ->where('wards.zone_id', '=', $zone_id)
                ->get();
        //	dd($data['wards']);
        return view('front.wards-under-zone', $data);
    }

    public function holdings_under_zone($id) {
        $data['selected_zone_no'] = $id;

        return view('dashboard.settings.holdings', $data);
    }

    public function holdings($zid, $wid = null) {

        $data['roads'] = DB::table('roads')
                ->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('roads.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('wards.ward_id', '=', $wid)
                ->get();
        //	dd($data['roads']);
        $data['areas'] = DB::table('ward_areas')
                ->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('ward_areas.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('ward_areas.ward_id', '=', $wid)
                ->get();
        $data['selected_zone_no'] = $zid;
        $data['selected_ward_no'] = '';
        if ($wid != null) {
				$the_ward = DB::table('wards')
                ->where('ward_id', '=', $wid)
                ->first();
            $data['selected_ward_no'] = $the_ward->ward_number;
        }

        return view('front.holdings', $data);
    }

    public function dt_ajax_all_holdings($zid = null, $wid = null) {
        if ($zid != null && $wid == null) {

            $holdings = DB::table('holdings')
                    ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                    ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                    ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                    ->join('zones', 'wards.zone_id', '=', 'zones.id')
                    ->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
                    ->where('wards.zone_id', '=', $zid)
                    ->get();
        } else if ($zid != null && $wid != null) {

            $holdings = DB::table('holdings')
                    ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                    ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                    ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                    ->join('zones', 'wards.zone_id', '=', 'zones.id')
                    ->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
                    ->where('zones.name', '=', $zid)
                    ->where('wards.ward_number', '=', $wid * 1)
                    ->get();
        } else {
            $holdings = DB::table('holdings')
                    ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                    ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                    ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                    ->join('zones', 'wards.zone_id', '=', 'zones.id')
                    ->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
                    ->get();
        }

           echo json_encode($holdings);exit;
        //    dd($holdings);
        //    echo json_encode($zones);

        $data['data'] = [];
        $rv_data = [];
        $selected_index = '';
        foreach ($holdings as $zone_k => $holding_v) {
            //	echo json_encode($holding_v);
            $selected_row = '';
            $is_active_class = 'text-body';
            $is_active_lbl = '';

            $zone['sl'] = $zone_k + 1;
            $zone['holding_no'] = '<a href="' . route('settings.holding.edit', ['holding_id' => $holding_v->id]) . '" ><b class="font-weight-bold ' . $is_active_class . ' ' . $selected_row . '">' .
                    $holding_v->holding_no .
                    '</b></a> <small>' . $is_active_lbl . '</small><br>' .
                    '<small>Area: <b>' . $holding_v->area . '</b> | Road: <b>' . $holding_v->road_name . '</b></small>';
            $zone['owner_name'] = $holding_v->owner_name;
            $zone['yearly_amount'] = 0;
            $zone['yearly_percentage'] = 0 . '%';
            $zone['ward_zone'] = 'W:<b>' . $holding_v->ward . "</b> &nbsp; | &nbsp; Z:<b>" . $holding_v->zone . "</b>";
            $zone['action'] = '<a href="' . route('holding.details', ['id' => $holding_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Edit">' .
                    ' &nbsp; <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; ' .
                    '</a> <a href="' . route('settings.holding.tax-pay', ['id' => $holding_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Pay">' .
                    'Pay' .
                    '</a>';

            array_push($data['data'], $zone);
        }

        $data['select'] = $selected_index;

        //    echo '<pre>';  print_r($data); echo '</pre>';
        echo json_encode($data);
    }


    public function per_square_feet_ret( $data = [
        'year' => 0,
        'zone_id' => 0,
        'asset_category_id' => 0,
        'asset_cat_class_id' => 0,
        'asset_cat_using_id' => 0
    ] ){

        $data_ret = DB::table('zone_tax_rate_settings')
            ->select(
                'zone_tax_rate_settings.*',
                'asset_category.asset_category_title',
                'asset_category.asset_category_slug',
                'asset_category_class.asset_cat_class_title',
                'asset_category_class.asset_cat_class_slug',
                'asset_category_of_using.asset_cat_using_title',
                'asset_category_of_using.asset_cat_using_slug',
                )
            ->join('zones', 'zone_tax_rate_settings.zone_id', '=', 'zones.id')
            ->join('asset_category', 'zone_tax_rate_settings.asset_category_id', '=', 'asset_category.asset_category_id')
            ->join('asset_category_class', 'zone_tax_rate_settings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
            ->join('asset_category_of_using', 'zone_tax_rate_settings.asset_cat_using_id', '=', 'asset_category_of_using.asset_cat_using_id')
            ->where('zone_tax_rate_settings.year', '=', $data['year'])
            ->where('zone_tax_rate_settings.zone_id', '=', $data['zone_id'])
            ->where('zone_tax_rate_settings.asset_category_id', '=', $data['asset_category_id'])
            ->where('zone_tax_rate_settings.asset_cat_class_id', '=', $data['asset_cat_class_id'])
            ->where('zone_tax_rate_settings.asset_cat_using_id', '=', $data['asset_cat_using_id'])
            ->first();
    //    dd($data);
        return $data_ret;
    }


    public function holding_details($id) {
        $data['zones'] = Zone::all()->sortByDesc("id");


        $data['asset_category'] = DB::table('asset_category')
                ->join('anniversary_asset_cat_percentage', 'asset_category.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
                ->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->select('asset_category.*', 'anniversary_asset_cat_percentage.*', 'anniversary_range.*')
                ->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
                ->get();

        $data['asset_category_class'] = DB::table('asset_category_class')
                ->join('anniversary_asset_cat_class_rate', 'asset_category_class.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
                ->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->select('asset_category_class.*', 'anniversary_asset_cat_class_rate.*', 'anniversary_range.*')
                ->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
                ->get();

        $data['holding'] = $holding = DB::table('holdings')
                ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->join('asset_category', 'holdings.asset_category_id', '=', 'asset_category.asset_category_id')
                ->join('asset_category_class', 'holdings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                ->join('anniversary_asset_cat_class_rate', 'holdings.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
                ->join('anniversary_asset_cat_percentage', 'holdings.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
                ->join('asset_category_of_using', 'holdings.asset_cat_using_id', '=', 'asset_category_of_using.asset_cat_using_id')
                ->select(
                        'holdings.*',
                        'asset_category.*',
                        'anniversary_asset_cat_percentage.*',
                        'asset_category_class.*',
                        'anniversary_asset_cat_class_rate.*',
                        'asset_category_of_using.*',
                        'wards.ward_id',
                        'wards.ward_number',
                        'zones.name as zone_name',
                        'zones.id as zone_id',
                        'ward_areas.area_name',
                        'roads.road_name'
                )
                ->where('holdings.id', '=', $id)
                ->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
                ->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
                ->first();
					 

        $data['last_tax_rate_updated_year'] = $last_year_tax_rate_settings = DB::table('zone_tax_rate_settings')
            ->select('year')
            ->orderBy('year','desc')
            ->groupBy('year')
            ->first();
		//	dd( $data['last_tax_rate_updated_year']);
			$data['holding_current_percentage'] = 	DB::table('zone_tax_asset_cat_percentage')
						->select('*')
                ->where('zone_id', '=', $holding->zone_id)
                ->where('year', '=', $last_year_tax_rate_settings->year)
                ->where('asset_category_id', '=', $holding->asset_category_id)
                ->first();
					 
		//	dd($data['holding_current_percentage']);
        $data['per_square_feet_ret'] = $this->per_square_feet_ret(
                                            [
                                                'year' => $last_year_tax_rate_settings->year,
                                                'zone_id' => $holding->zone_id,
                                                'asset_category_id' => $holding->asset_category_id,
                                                'asset_cat_class_id' => $holding->asset_cat_class_id,
                                                'asset_cat_using_id' => $holding->asset_cat_using_id
                                            ]
                                        );
			//	dd($data['holding']);
			$data['roads'] = DB::table('roads')
                ->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('roads.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('wards.ward_id', '=', $data['holding']->ward_id)
                ->get();
			//	dd($data['roads']);
			$data['areas'] = DB::table('ward_areas')
                ->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->select('ward_areas.*', 'wards.ward_number as ward', 'zones.name as zone')
                ->where('ward_areas.ward_id', '=', $data['holding']->ward_id)
                ->get();
			//	dd($data['areas']);
			//	$data['zone'] = $data['holding']->;
			//	$data['ward'] = $ward;
			//   dd($single_ward);



			$holdings_tax_per_fyear = DB::table('holdings_tax_of_per_year')
                ->select('*')
                ->where('holding_id', $id)
                ->orderBy('fiscal_year', 'desc')
                ->get();
			/*
			echo '<pre>';
			print_r($holdings_tax_per_fyear);
			exit;
			*/
		//	/*		
			$pay_year = '';
			$tax_payment_history = [];
			foreach( $holdings_tax_per_fyear as $tax_per_fyear_k => $tax_per_fyear_v ){
			//	print_r($tax_per_fyear_v->fiscal_year);
				$pay_year = $tax_per_fyear_v->fiscal_year;
				$pay_year = explode('-',$pay_year);
			//	$pay_year = $pay_year[1];
			//	print_r($pay_year);
			//	/*
				$tax_pay_history_per_fyear = DB::table('tax_payments')
						 ->select('*')
						 ->where('holding_id', $id)
						 ->whereBetween('sslcz_tran_date', [$pay_year[0] . '-07-01', $pay_year[1] . '-06-30'])
						 ->orderBy('sslcz_tran_date', 'desc')
						 ->get();
					//	 ->toSql();
			//	*/
				$tax_per_fyear_v->payment_history = $tax_pay_history_per_fyear;
				$tax_payment_history[$tax_per_fyear_k] = $tax_per_fyear_v;
				
				/*
				echo '<pre>';
				print_r($holdings_tax_per_fyear);
			//	print_r($pay_year[0] . '-07-01');
				echo '<br>';
			//	print_r($pay_year[1] . '-06-30');
				echo '</pre><br>';
				*/
			}
			
			$data['tax_payment_history'] = $tax_payment_history = array_reverse($tax_payment_history);
		//	/*
		//	echo '<pre>';
		//	print_r($data['tax_payment_history']);
		//	print_r(array_reverse($data['tax_payment_history']));
		//	krsort($data['tax_payment_history']);
		//	print_r($data['tax_payment_history']);
		//	exit;
		//	*/
		//	dd($fiscal_years);

		$total_pyed = 0;
		foreach($tax_payment_history as $tph_k => $tph_v){
			foreach($tph_v->payment_history as $ph_k => $ph_v){
			  $total_pyed += $ph_v->payment_amount * 1;
			}
		}
		$data['total_pyed'] = $tax_amount_due = $total_pyed;
		foreach($tax_payment_history as $tph_k => $tph_v){
			$tax_amount = $tph_v->tax_amount * 1;
			$tax_amount_due = $tax_amount_due - $tax_amount;
			$tph_v->tax_amount_due = $tax_amount_due;
		}
		$data['tax_payment_history'] = array_reverse($tax_payment_history);
		/*
			echo '<pre>';
			print_r($tax_payment_history);
			exit;
		*/
        $data['payment_history'] = [];

        $fiscal_years = DB::table('tax_payments')
                ->select('tax_payments.id', 'tax_payments.holding_id', 'tax_payments.fiscal_year')
                ->where('tax_payments.holding_id', $id)
                ->groupBy('tax_payments.fiscal_year')
                ->orderBy('tax_payments.fiscal_year', 'desc')
                ->get();
        //	dd($fiscal_years);
        //	/*
        foreach ($fiscal_years as $fiscal_year) {
            $data['payment_history'][$fiscal_year->fiscal_year] = DB::table('tax_payments')
                    ->select('*')
                    ->join('holdings', 'tax_payments.holding_id', '=', 'holdings.id')
                    ->where('tax_payments.holding_id', '=', $id)
                    ->where('tax_payments.fiscal_year', '=', $fiscal_year->fiscal_year)
                    ->orderBy('tax_payments.payment_datetime', 'desc')
                    ->get();
        }
        //	dd($data['payment_history']);
        //	  		  */
        return view('front.holding-details', $data);
    }

    public function holding_tax_pay($hid, $amt, $f_year) {
        echo '<pre>';
        print_r($hid);
        echo '</pre>';
        echo '<pre>';
        print_r($amt);
        echo '</pre>';
        echo '<pre>';
        print_r($f_year);
        echo '</pre>';
        return view('front.holding-topay', $data);
    }

    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'zone_name' => 'required|unique:zones,name|max:255|min:1'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $data = [
            'name' => trim($request->input('zone_name')),
            'google_map_data' => json_encode([
                'lat' => $request->input('latitude'),
                'lon' => $request->input('longitude'),
                'zoom' => $request->input('zoom')
            ]),
            'created_by' => Auth::user()->id,
            'last_updated_by' => date('Y-m-d H')
        ];

        try {
            $zone_id = Zone::create($data)->id;
            session()->flash('success', 'Successfully new zone added <b><a href="' . route('settings.zone.edit', ['id' => $zone_id]) . '">' . $request->input('zone_name') . '</a></b>');
            return redirect()->route('settings.zones');
        } catch (Exception $exception) {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request = null) {
        $single_zone = Zone::find($id);
        //    dd($single_zone->name);
        $data['the_zone'] = $single_zone;
        $google_map_data = json_decode($single_zone->google_map_data);

        $data['zoom'] = $google_map_data->zoom;
        $data['lat'] = $google_map_data->lat;
        $data['lon'] = $google_map_data->lon;
        return view('dashboard.settings.zone-edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Auth::user()->usertype == 'superadmin') {
            $data = [
                'name' => trim($request->input('zone_name')),
                'google_map_data' => json_encode([
                    'lat' => $request->input('latitude'),
                    'lon' => $request->input('longitude'),
                    'zoom' => $request->input('zoom')
                ]),
                'created_by' => Auth::user()->id
            ];

            $affected = DB::table('zones')
                    ->where('id', $id)
                    ->update($data);

            if ($affected) {
                session()->flash('success', 'Successfully Updated');
                session()->flash('action', 'updated');
                return redirect()->route('settings.zone.edit', ['id' => $id]);
            } else {
                session()->flash('info', 'Data not changed, So no need to update :) Thanks');
                session()->flash('action', 'updated');
                return redirect()->route('settings.zone.edit', ['id' => $id]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $single_zone = Zone::find($id);
        //    dd($single_zone->name);
        if (Auth::user()->usertype == 'superadmin') {

            //    $zone = DB::table('zones')->where('id', $id);
            $single_zone->delete();
            session()->flash('success', 'Successfully Removed Zone <b>' . $single_zone->name . '</b>');
            session()->flash('action', 'warning');
            return redirect()->back();
        }
    }

    public function __call($method, $parameters) {
        parent::__call($method, $parameters); // TODO: Change the autogenerated stub
    }

    public function dt_ajax_all_zones($id = null) {

        $zones = Zone::all()->sortByDesc("id");
        //    dd($zones);
        //    echo json_encode($zones);

        $data['data'] = [];
        $rv_data = [];
        $selected_index = '';
        foreach ($zones as $zone_k => $zone_v) {
            //	pre($pro_v);
            $selected_row = '';
            $is_active_class = 'text-body';
            $is_active_lbl = '';
            if ($zone_v->status == 0) {
                $is_active_class = 'text-black-50';
                $is_active_lbl = '<span class="badge badge-secondary">Inactive</span>';
            } elseif ($zone_v->status == 1) {
                $is_active_lbl = '<span class="badge badge-primary">A</span>';
            }

            if ($zone_v->id == $id) {
                $selected_index = $zone_k;
                $selected_row = 'selected';
            }

            $zone['sl'] = $zone_k + 1;
            $zone['name'] = '<a href="' . route('settings.zone.edit', ['id' => $zone_v->id]) . '" ><b class="font-weight-bold ' . $is_active_class . ' ' . $selected_row . '">' .
                    $zone_v->name .
                    '</b></a> <small>' . $is_active_lbl . '</small>';
            $zone['action'] = '<a href="' . route('settings.zone.edit', ['id' => $zone_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Edit">' .
                    '<i class="fa fa-eye" aria-hidden="true"></i> ' .
                    '<i class="fa fa-edit" aria-hidden="true"></i>' .
                    '</a>' .
                    ' ' .
                    '<form action="' . route('settings.zone.remove', ['id' => $zone_v->id]) . '" class="" method="POST">' .
                    '<input type="hidden" name="_token" value="' . csrf_token() . '" >' .
                    '<button type="submit" class="btn btn-sm btn-outline-danger form_zone_remove" name="zone_id" value="' . $zone_v->id . '" title="Delete">' .
                    '<i class="fa fa-trash-o" aria-hidden="true"></i>' .
                    '</button>' .
                    '</form>';

            array_push($data['data'], $zone);
        }
        $data['select'] = $selected_index;

        //    echo '<pre>';  print_r($data); echo '</pre>';
        echo json_encode($data);
    }

    public function holding_tax_checkout($holding_id) {
        $single_zone = Zone::find($holding_id);
        //    dd($single_zone->name);
        $data['the_zone'] = $single_zone;

        $data['holding'] = DB::table('holdings')
                ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
                ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
                ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                ->join('zones', 'wards.zone_id', '=', 'zones.id')
                ->join('asset_category', 'holdings.asset_category_id', '=', 'asset_category.asset_category_id')
                ->join('asset_category_class', 'holdings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                ->join('anniversary_asset_cat_class_rate', 'holdings.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
                ->join('anniversary_asset_cat_percentage', 'holdings.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
                ->select(
                        'holdings.*',
                        'asset_category.*',
                        'anniversary_asset_cat_percentage.*',
                        'asset_category_class.*',
                        'anniversary_asset_cat_class_rate.*',
                        'holdings.id as holding_id',
                        'wards.ward_number',
                        'zones.name as zone_name',
                        'zones.id as zone_id',
                        'ward_areas.area_name',
                        'roads.road_name'
                )
                ->where('holdings.id', '=', $holding_id)
                ->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
                ->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
                ->first();

        return view('front.tax-checkout', $data);
    }

    public function holding_taxpay_success() {
        $data = [''];

        $val_id = urlencode($_POST['val_id']);
        $store_id = urlencode("allex5e13792d31125");
        $store_passwd = urlencode("allex5e13792d31125@ssl");
        $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=" . $val_id . "&store_id=" . $store_id . "&store_passwd=" . $store_passwd . "&v=1&format=json");

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $requested_url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

        $result = curl_exec($handle);

        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($code == 200 && !( curl_errno($handle))) {

            # TO CONVERT AS ARRAY
            # $result = json_decode($result, true);
            # $status = $result['status'];
            # TO CONVERT AS OBJECT
            $result = json_decode($result);
            /*
              # TRANSACTION INFO
              $status = $result->status;
              $tran_date = $result->tran_date;
              $tran_id = $result->tran_id;
              $val_id = $result->val_id;
              $amount = $result->amount;
              $store_amount = $result->store_amount;
              $bank_tran_id = $result->bank_tran_id;
              $card_type = $result->card_type;

              # EMI INFO
              $emi_instalment = $result->emi_instalment;
              $emi_amount = $result->emi_amount;
              $emi_description = $result->emi_description;
              $emi_issuer = $result->emi_issuer;

              # ISSUER INFO
              $card_no = $result->card_no;
              $card_issuer = $result->card_issuer;
              $card_brand = $result->card_brand;
              $card_issuer_country = $result->card_issuer_country;
              $card_issuer_country_code = $result->card_issuer_country_code;

              # API AUTHENTICATION
              $APIConnect = $result->APIConnect;
              $validated_on = $result->validated_on;
              $gw_version = $result->gw_version;
             */
            DB::table('tax_payments')->insert(
                    [
                        'holding_id' => isset($_GET['holding_id']) ?? 0,
                        'payment_amount' => ($result->store_amount * 1) - 0.2,
                        'sslcz_tran_date' => $result->tran_date,
                        'sslcz_tran_id' => $result->tran_id,
                        'sslcz_val_id' => $result->val_id,
                        'sslcz_amount' => $result->amount,
                        'sslcz_store_amount' => $result->store_amount,
                        'sslcz_response_json' => json_encode($result),
                        'payment_datetime' => date('Y-m-d H:i:s'),
                        'received_by' => 'sslcommerz.com'
                    ]
            );
            $data['result'] = $result;
        } else {

            echo "Failed to connect with SSLCOMMERZ";
        }

        return view('front.taxpay-success', $data);
    }

}
