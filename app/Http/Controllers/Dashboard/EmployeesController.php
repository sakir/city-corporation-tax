<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Zone;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Auth;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$data['zoneadmins'] = User::where('usertype', 'zoneadmin')->get();
			//   echo json_encode($data['zones']);
			$data['zones'] = Zone::all();
			return view('dashboard.settings.employees', $data);
    }

    public function dt_ajax_all_employees()
    {
			$admins = User::where('usertype', 'zoneadmin')->get();
         
			$data['data'] = [];
			$rv_data = [];
			$selected_index = '';
        foreach ($admins as $admin_k => $admin_v) {
            //	pre($pro_v);
            $selected_row = '';
            $is_active_class = 'text-body';
            
            $zone['sl'] = $admin_k + 1;
            $zone['name'] =$admin_v->name;
            $zone['email'] = $admin_v->email;
            $zone['usertype'] = $admin_v->usertype;
            $zone['zone'] = '<b>' . $admin_v->permission_id . '</b>';
            $zone['action'] = '<a href="' . route('settings.user.edit', ['id' => $admin_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Edit">' .
                '<i class="fa fa-eye" aria-hidden="true"></i> ' .
                '<i class="fa fa-edit" aria-hidden="true"></i>' .
                '</a>' .
                ' ' .
                '<form action="' . route('settings.user.remove', ['id' => $admin_v->id]) . '" class="" method="POST">' .
                '<input type="hidden" name="_token" value="' . csrf_token() . '" >' .
                '<button type="submit" class="btn btn-sm btn-outline-danger form_ward_remove" name="user_id" value="' . $admin_v->id . '" title="Delete">' .
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' .
                '</button>' .
                '</form>';

            array_push($data['data'], $zone);
        }
        $data['select'] = $selected_index;

        //    echo '<pre>';  print_r($data); echo '</pre>';
        echo json_encode($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zones = DB::table('zones')->where('status', 1)->orderBy('name','asc')->get();
    //    dd($zones);
        $data['zones'] = $zones;
        return view('dashboard.settings.employee-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
              'name' => 'required|max:200',
              'email'   => 'email|required|unique:users',
              'password'   => 'required|required_with:conf_password|min:6'
      ]);
      $validatedData['password'] = bcrypt($request->password);
      $validatedData['usertype'] = 'zoneadmin';
      $validatedData['permission_id'] = $request->input('zone_id');
   //   prex($validatedData);
	//	echo '<pre>';  print_r($validatedData); echo '</pre>';
	//	exit;
      $userCreated = User::create($validatedData);
		if ($userCreated) {
			session()->flash('success', 'New ZoneAdmin added');
			return redirect()->route('settings.users');
		} else {
			session()->flash('info', 'ZoneAdmin saving problem to database. Please try again ... ');
			return redirect()->route('settings.users');
		}
       
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
			$data['zones'] = Zone::all();
			$data['zoneadmin'] = User::where('id', $id)->first();
			return view('dashboard.settings.employee-edit', $data);
    }


    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
              'name' => 'required|max:200',
              'email'   => 'email|required'
      ]);
		if( trim($request->password) != '' ){
				
			$validatedData = $request->validate([
					  'name' => 'required|max:200',
					  'email'   => 'email|required',
					  'password'   => 'required_with:conf_password|min:6'
			]);
			$validatedData['password'] = bcrypt($request->password);
		}
      
      $validatedData['usertype'] = 'zoneadmin';
      $validatedData['permission_id'] = $request->input('zone_id');
   //   prex($validatedData);
		
		$affected = DB::table('users')
						 ->where('id', $id)
						 ->update($validatedData);
	
	//	echo '<pre>';  print_r($id); echo '</pre>';
	//	exit;
	
		if ($affected) {
			 session()->flash('success', 'Successfully Updated');
			 session()->flash('action', 'updated');
			 return redirect()->route('settings.users');
		}else{
			 session()->flash('info', 'Data not changed, So no need to update :) Thanks');
			 session()->flash('action', 'updated');
			 return redirect()->route('settings.users');
		}
	
    }

    public function destroy($id)
    {
        //
    }
}
