<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{
	
    public function ajax_words_area(Request $request)
    {
        $ward_areas = DB::table('ward_areas')
            ->where('ward_id', '=', $request->input('ward_id'))
            ->get();
				
			if( $ward_areas ){
				
				$data['status'] = 'success';
				$data['data'] = $ward_areas;
				$data['dev_data_test'] = $request->input('ward_id');
			}else{
				
				$data['status'] = 'error';
				$data['data'] = '';
				$data['dev_data_test'] = $request->input('ward_id');
			}
        return json_encode($data);
    }

    public function holdings_load_by_ward_areas(Request $request)
    {
        $holdings = DB::table('holdings')
            ->where('area_id', '=', $request->input('wards_area_id'))
            ->get();
				
			if( $holdings ){
				
				$data['status'] = 'success';
				$data['data'] = $holdings;
				$data['dev_data_test'] = $request->input('wards_area_id');
			}else{
				
				$data['status'] = 'error';
				$data['data'] = '';
				$data['dev_data_test'] = $request->input('wards_area_id');
			}
        return json_encode($data);
    }

    public function ajax_holdings_total_due(Request $request)
    {
			$basicCont = new BasicController();
			$holding_id = $request->input('holding_id');
			$fyear = $request->input('fyear');
			
			if( $holding_id ){
			
				if( $fyear ){
					
					return json_encode( $basicCont->holdings_total_due($holding_id, $fyear) );
					
				}else{
					
					return json_encode( $basicCont->holdings_total_due($holding_id) );
				}
			}else{
				return json_encode( $basicCont->holdings_total_due() );
			}
    }

}
