<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;
use Mail;

class SettingsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['zones'] = Zone::all();
        //   echo json_encode($data['zones']);

        return view('dashboard.settings.zones');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function anniversary_tax_rate_settings() {

        $data['annivers_asset_cat_percentage'] = $data['annivers_asset_cat_class_rate'] = false;
        $data['anniversary_asset_cat_class_rate'] = DB::table('anniversary_asset_cat_class_rate')
                ->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->join('asset_category_class', 'anniversary_asset_cat_class_rate.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                ->select(
                        'anniversary_asset_cat_class_rate.*',
                        'anniversary_asset_cat_class_rate.id as cat_class_rate_id',
                        'anniversary_range.ar_title',
                        'anniversary_range.ar_from',
                        'anniversary_range.ar_to',
                        'asset_category_class.asset_cat_class_title'
                )
					 ->where('anniversary_asset_cat_class_rate.zone_id', '=', Auth::user()->permission_id)
                ->orderBy('anniversary_range.ar_id', 'desc')
                ->orderBy('asset_category_class.asset_cat_class_id', 'asc')
                ->get();

        $data['anniversary_asset_cat_percentage'] = DB::table('anniversary_asset_cat_percentage')
                ->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->join('asset_category', 'anniversary_asset_cat_percentage.asset_category_id', '=', 'asset_category.asset_category_id')
                ->select(
                        'anniversary_asset_cat_percentage.*',
                        'anniversary_asset_cat_percentage.id as cat_percentage_id',
                        'anniversary_range.ar_title',
                        'anniversary_range.ar_from',
                        'anniversary_range.ar_to',
                        'asset_category.asset_category_title'
                )
					 ->where('anniversary_asset_cat_percentage.zone_id', '=', Auth::user()->permission_id)
                ->orderBy('anniversary_range.ar_id', 'desc')
                ->orderBy('asset_category.asset_category_id', 'asc')
                ->get();
        //	dd($data['anniversary_asset_cat_class_rate']);
        $data['anniversary_range'] = DB::table('anniversary_range')
                ->select('*')
                ->orderBy('ar_id', 'desc')
                ->get();

        $data['asset_category'] = DB::table('asset_category')
                ->select('*')
                ->orderBy('asset_category_id', 'desc')
                ->get();

        return view('dashboard.settings.anniversary-tax-rate-setting', $data);
    }

    public function anniversary_tax_rate_settings_edit($action, $id = null, Request $request = null) {
        $data['annivers_asset_cat_percentage'] = $data['annivers_asset_cat_class_rate'] = false;
        if ($action == 'asset-cat-percentage' && $id != null) {
            $data['annivers_asset_cat_percentage'] = DB::table('anniversary_asset_cat_percentage')
                    ->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
                    ->join('asset_category', 'anniversary_asset_cat_percentage.asset_category_id', '=', 'asset_category.asset_category_id')
                    ->select(
                            'anniversary_asset_cat_percentage.*',
                            'anniversary_asset_cat_percentage.id as cat_percentage_id',
                            'anniversary_range.ar_title',
                            'anniversary_range.ar_from',
                            'anniversary_range.ar_to',
                            'asset_category.asset_category_title'
                    )
                    ->where('anniversary_asset_cat_percentage.id', '=', $id)
                    ->first();
        } elseif ($action == 'asset-cat-class-rate') {

            $data['annivers_asset_cat_class_rate'] = DB::table('anniversary_asset_cat_class_rate')
                    ->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
                    ->join('asset_category_class', 'anniversary_asset_cat_class_rate.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                    ->select(
                            'anniversary_asset_cat_class_rate.*',
                            'anniversary_asset_cat_class_rate.id as cat_class_rate_id',
                            'anniversary_range.ar_title',
                            'anniversary_range.ar_from',
                            'anniversary_range.ar_to',
                            'asset_category_class.asset_cat_class_title'
                    )
                    ->where('anniversary_asset_cat_class_rate.id', '=', $id)
                    ->first();
        }

        $data['anniversary_asset_cat_class_rate'] = DB::table('anniversary_asset_cat_class_rate')
                ->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->join('asset_category_class', 'anniversary_asset_cat_class_rate.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
                ->select(
                        'anniversary_asset_cat_class_rate.*',
                        'anniversary_asset_cat_class_rate.id as cat_class_rate_id',
                        'anniversary_range.ar_title',
                        'anniversary_range.ar_from',
                        'anniversary_range.ar_to',
                        'asset_category_class.asset_cat_class_title'
                )
					 ->where('anniversary_asset_cat_class_rate.zone_id', '=', Auth::user()->permission_id)
                ->orderBy('anniversary_range.ar_id', 'desc')
                ->orderBy('asset_category_class.asset_cat_class_id', 'asc')
                ->get();

        $data['anniversary_asset_cat_percentage'] = DB::table('anniversary_asset_cat_percentage')
                ->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
                ->join('asset_category', 'anniversary_asset_cat_percentage.asset_category_id', '=', 'asset_category.asset_category_id')
                ->select(
                        'anniversary_asset_cat_percentage.*',
                        'anniversary_asset_cat_percentage.id as cat_percentage_id',
                        'anniversary_range.ar_title',
                        'anniversary_range.ar_from',
                        'anniversary_range.ar_to',
                        'asset_category.asset_category_title'
                )
					 ->where('anniversary_asset_cat_percentage.zone_id', '=', Auth::user()->permission_id)
                ->orderBy('anniversary_range.ar_id', 'desc')
                ->orderBy('asset_category.asset_category_id', 'asc')
                ->get();
        //	dd($data['anniversary_asset_cat_class_rate']);
        $data['anniversary_range'] = DB::table('anniversary_range')
                ->select('*')
                ->orderBy('ar_id', 'desc')
                ->get();

        $data['asset_category'] = DB::table('asset_category')
                ->select('*')
                ->orderBy('asset_category_id', 'desc')
                ->get();

        return view('dashboard.settings.anniversary-tax-rate-setting', $data);
    }

    function anniversary_tax_cat_percentage_update(Request $request) {

        $cat_percentage_update = DB::table('anniversary_asset_cat_percentage')
                ->where('id', $request->input('percentage'))
                ->update($data);

        if ($cat_percentage_update) {
            session()->flash('success', 'পারসেন্টেজ আপডেট সম্পন্য হয়েছে');
            session()->flash('action', 'updated');
            return redirect()->route('settings.anniversary-tax-rate');
        } else {
            session()->flash('info', 'Data not changed, :) Thanks');
            session()->flash('action', 'updated');
            return redirect()->route('settings.anniversary-tax-rate');
        }
    }

    function anniversary_tax_cat_rate_update(Request $request) {
        //	dd(44);
        $cat_percentage_update = DB::table('anniversary_asset_cat_percentage')
                ->where('id', $request->input('percentage'))
                ->update($data);

        if ($cat_percentage_update) {
            session()->flash('success', 'পারসেন্টেজ আপডেট সম্পন্য হয়েছে');
            session()->flash('action', 'updated');
            return redirect()->route('settings.anniversary-tax-rate');
        } else {
            session()->flash('info', 'Data not changed, :) Thanks');
            session()->flash('action', 'updated');
            return redirect()->route('settings.anniversary-tax-rate');
        }
    }

    function fyear_holding_property_save_and_tax_generate( $fyear = null ) {
       
        //	dd($data['settings_years']);
		  $data = [];
		  if( $fyear != null ){
						
				$zoneID = Auth::user()->permission_id;
				$zone_holders = DB::table('holdings')
							 ->select('holdings.id')
							 ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
							 ->where('wards.zone_id', $zoneID)
							 ->get();
				$fYear_ar = explode('-',$fyear);
				$fYear1 = $fYear_ar[0];
				$fYear2 = $fYear_ar[1];
				
				$anniversary_range_id = 0;
				$anniversary_range_title = '';
				$anniversary_range = DB::table('anniversary_range')->get();
				foreach( $anniversary_range as $range_k => $range_v ){
					$range_year_from = date('Y', strtotime($range_v->ar_from));
					$range_year_to = date('Y', strtotime($range_v->ar_to));
					
					if($fYear1 >= $range_year_from && $fYear2 <= $range_year_to+1){
						$anniversary_range_id = $range_v->ar_id;
						$anniversary_range_title = $range_v->ar_title;
					}
				}
				$detected_holdings = [];
				foreach( $zone_holders as $zholding ){
					array_push($detected_holdings, $zholding->id);
				}
				
				$holding_tax_bill_generated_record = DB::table('annual_holding_tax_bill_generate_records')
							 ->where('zone_id', $zoneID)
							 ->where('anniversary_range_id', $anniversary_range_id)
							 ->where('fiscal_year', $fyear)
							 ->first();
			//	echo '<pre>';
			//	print_r($holding_tax_bill_generated_record);
			//	echo '</pre>';
				
				$prev_record_info = '';
				if( $holding_tax_bill_generated_record ){
					if( $holding_tax_bill_generated_record->done_process == '100%' ){
						$prev_record_info	= 'পুর্বে এই অর্থবছরের ট্যেক্স বিল সম্পুর্ন ভাবে প্রস্তুতকরন করা হয়েছিলো  ' . $holding_tax_bill_generated_record->datetime . ' তারিখে। পুনরায় প্রস্তুত করা যেতে পারে।';
					}else if( $holding_tax_bill_generated_record->done_process == '0%' ){
						DB::table('annual_holding_tax_bill_generate_records')->where('id', $holding_tax_bill_generated_record->id)->update(['detected_holdings'=>json_encode($detected_holdings)]);
						$prev_record_info	= 'এই অর্থবছরের ট্যেক্স বিল এখনো প্রস্তুত করা হয়নি, এখন করা যেতে পারে।';
					}else{
						DB::table('annual_holding_tax_bill_generate_records')->where('id', $holding_tax_bill_generated_record->id)->update(['detected_holdings'=>json_encode($detected_holdings)]);
						$prev_record_info	= "ইতিমধ্যে এই অর্থবছরের ট্যেক্স বিল প্রস্তুতকরন করা হয়েছিলো  " . $holding_tax_bill_generated_record->datetime . " তারিখে, যা অসম্পূর্ণ ছিলো, এখন পুনরায় করতে হবে।";
					}
				}else{
					DB::table('annual_holding_tax_bill_generate_records')->insert(
						[
							'zone_id' => $zoneID,
							'anniversary_range_id' => $anniversary_range_id,
							'fiscal_year' => $fyear,
							'datetime' => date('Y-m-d H:i:s'),
							'done_process' => '0%',
							'detected_holdings' => json_encode($detected_holdings)
						]
					);
					$prev_record_info	= 'এই অর্থবছরের ট্যেক্স বিল এখনো প্রস্তুত করা হয়নি, এখন করা যেতে পারে।';
				}
				
				$data['holding_tax_bill_generated_record'] = DB::table('annual_holding_tax_bill_generate_records')
							 ->where('zone_id', $zoneID)
							 ->where('anniversary_range_id', $anniversary_range_id)
							 ->where('fiscal_year', $fyear)
							 ->first();
				/*
				if( $fYear2 < 2013 ){
					
				}
				echo '<pre>';
			//	print_r($fYear1);
				print_r(count($zone_holders));
				echo '</pre>';
				exit;
				*/
				
				$data['selected_holding_data'] = [
														'prev_record_info'=> $prev_record_info,
														'anniversary_range_id'=> $anniversary_range_id,
														'anniversary_range_title'=> $anniversary_range_title,
														'selected_zone_count'=>count($zone_holders),
														'detected_holdings'=>$detected_holdings
													];
				$data['fyear'] = $fyear;
		  }
        return view('dashboard.settings.fyear-holding-property-save-and-tax-generate', $data);
    }
	 
    function fyear_holding_property_save_and_tax_gen_ready_req( Request $request ) {

		$fYear = 	$request->input('fyear'); 
	//	$data['selected_zone_data'] = ['selected_zone_count'=>count($zone_holders)];
		
      return redirect()->route('settings.fyear-holding-property-save-and-tax-generate', ['fyear'=>$fYear]);
	 }
		
    function fyear_holding_property_save_and_tax_gen_ready_confirm( Request $request ) {
		$tax_bill_generate_record_id = $request->input('tax_gen_confirm');
		$get_selected_holdings = DB::table('annual_holding_tax_bill_generate_records')->where('id', $tax_bill_generate_record_id )->first();
		
	//	return json_encode($selected_holding_count);exit;
	
		if( Auth::user()->permission_id == $get_selected_holdings->zone_id ){
			
			$selected_all_holding_ids = json_decode( $get_selected_holdings->detected_holdings );
			$selected_holding_count = count($selected_all_holding_ids);
			$targeted_holding_index = $request->input('targeted_holding_index');
			
			if( $targeted_holding_index *1 == 0 ){
			//	return json_encode($selected_holding_count);exit;
				DB::table('annual_holding_tax_bill_generate')
                ->where('zone_id', '=', $get_selected_holdings->zone_id)
                ->where('fiscal_year', '=', $get_selected_holdings->fiscal_year)
                ->delete();
			}
			
			$selected_holding_id = $selected_all_holding_ids[ $targeted_holding_index ];
			
				$holding_asst_detail = DB::table('holding_assets')->where('holding_id', $selected_holding_id)->get();
				
				foreach( $holding_asst_detail as $hold_asst_k =>  $hold_asst_v){
						
					$json_data = response()->json($hold_asst_v, 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
					
					$holdings_tax_of_per_year_entry = DB::table('annual_holding_tax_bill_generate')->insert(
						[
							'zone_id' => Auth::user()->permission_id,
							'holding_id' => $selected_holding_id,
							'fiscal_year' => $get_selected_holdings->fiscal_year,
							'anniversary_range_id' => $get_selected_holdings->anniversary_range_id,
							'asset_category_id' => $hold_asst_v->asset_category_id,
							'asset_category_class_id' => $hold_asst_v->asset_cat_class_id,
							'asset_category_of_using_id' => $hold_asst_v->asset_cat_using_id,
							'square_feet' => $hold_asst_v->square_feet,
							'floor_quantity' => $hold_asst_v->quantity,
							'detail_info_json' => $json_data->getContent(),
							'datetime' => date('Y-m-d H:i:s'),
						]
					);
					
				}
				// return json_encode($get_selected_holdings);exit;
				$emailHTMLbody_dy = 'Gaziput City corporation Annual Tax bill has generated ';
				
			//	return json_encode($holding_asst_detail->email);exit;
			/*
				$email_sending_to_holder = Mail::send([], [], function ($message) use($holding_asst_detail, $emailHTMLbody_dy){
						
				  $message->to( $holding_asst_detail->email )
					 ->subject('NexGen to Film Maker.')
					 // here comes what you want
					 ->setBody($emailHTMLbody_dy, 'text/html'); // for HTML rich messages
				});
				*/
				$done_percentage = ( ($targeted_holding_index + 1) * 100 ) / $selected_holding_count;
				
				$done_process_update_on_db = DB::table('annual_holding_tax_bill_generate_records')
                ->where('id', $tax_bill_generate_record_id)
                ->update( ['done_process' => $done_percentage . '%'] );
				
			//	if( $targeted_holding_index == $selected_holding_count - 1 ){
			//		return json_encode($selected_all_holding_ids[ $targeted_holding_index + 1 ] ?? 'end');exit;
			//	}
			//	return json_encode($done_process_update_on_db);exit;
				return json_encode([
										'status' => 'success',
										'done_holding_index' => $targeted_holding_index,
										'done_holding_id' => $selected_holding_id,
										'next_holding_id' => $selected_all_holding_ids[ $targeted_holding_index + 1 ] ?? 'end',
										'done_percentage' => $done_percentage,
										'incomplete_item_count' => $done_percentage
									]);
		}else{
				return json_encode(['status' => 'error']);
		}
		
	//	echo '<pre>';
	//	print_r($get_selected_holdings);
	//	echo '</pre>';
	//	exit;
		
   // return redirect()->route('settings.fyear-holding-property-save-and-tax-generate', ['fyear'=>$fYear]);
	 }
		
		
    function fyear_holding_property_save_and_tax_gen_ajax( $fyear = null ) {
       
        //	dd($data['settings_years']);
		  $data = [];
		  if( $fyear != null ){
					  
				$zoneID = Auth::user()->permission_id;
				$zone_holders = DB::table('holdings')
							 ->select('*')
							 ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
							 ->where('wards.zone_id', $zoneID)
							 ->get();
								
				$fYear_ar = explode('-',$fyear);
				$fYear1 = $fYear_ar[0];
				$fYear2 = $fYear_ar[1];
					
				$anniversary_range_id = 0;
				$anniversary_range = DB::table('anniversary_range')->get();
				foreach( $anniversary_range as $range_k => $range_v ){
					$range_year_from = date('Y', strtotime($range_v->ar_from));
					$range_year_to = date('Y', strtotime($range_v->ar_to));
					
					if($fYear1 >= $range_year_from && $fYear2 <= $range_year_to+1){
						$anniversary_range_id = $range_v->ar_id;
					}
				}
				
				$holding_tax_bill_generated_record = DB::table('annual_holding_tax_bill_generate_records')
							 ->where('zone_id', $zoneID)
							 ->where('anniversary_range_id', $anniversary_range_id)
							 ->where('fiscal_year', $fyear)
							 ->first();
				if( $holding_tax_bill_generated_record ){
						
					DB::table('annual_holding_tax_bill_generate_records')->insert(
						[
							'zone_id' => $zoneID,
							'anniversary_range_id' => $anniversary_range_id,
							'fiscal_year' => $fyear,
							'datetime' => date('Y-m-d H:i:s'),
							'done_process' => '0%',
						]
					);
				}else{
					
				}
				/*
				if( $fYear2 < 2013 ){
					
				}
				echo '<pre>';
			//	print_r($fYear1);
				print_r(count($zone_holders));
				echo '</pre>';
				exit;
				*/
					
				foreach($zone_holders as $hold_k => $hold_v){
					
					$json_data = response()->json($hold_v, 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
					DB::table('annual_holding_tax_bill_generate')->insert(
						[
							'zone_id' => $zoneID,
							'holding_id' => $hold_v->id,
							'fiscal_year' => $fyear,
							'anniversary_range_id' => $anniversary_range_id,
							'asset_category_id' => $hold_v->asset_category_id,
							'asset_category_class_id' => $hold_v->asset_cat_class_id,
							'asset_category_of_using_id' => $hold_v->asset_cat_using_id,
							'square_feet' => $hold_v->sqr_feet,
							'floor_quantity' => $hold_v->asset_qty,
							'detail_info_json' => $json_data->getContent(),
							'datetime' => date('Y-m-d H:i:s'),
						]
					);
				}
				$data['selected_holding_data'] = [
														'anniversary_range_id'=> $anniversary_range_id,
														'selected_zone_count'=>count($zone_holders)
													];
				$data['fyear'] = $fyear;
		  }
        return view('dashboard.settings.fyear-holding-property-save-and-tax-generate', $data);
    }
	 
	 
	 
    function fyear_holding_property_save_and_tax_gen_pro( Request $request ) {
		 
      $data['specific_zone_holders'] = DB::table('holdings')
                ->select('*')
					 ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
                ->where('wards.zone_id', $request->input('zone_id'))
                ->get();
		
	//	dd($data['specific_zone_holders']);
		foreach( $data['specific_zone_holders'] as $hold_k => $hold_v ){
						
			$tax_rate = DB::table('zone_tax_rate_settings')
					->select('*')
					->where('zone_id', $request->input('zone_id'))
					->where('year', $request->input('tax_rate_year'))
					->where('asset_category_id', $hold_v->asset_category_id)
					->where('asset_cat_class_id', $hold_v->asset_cat_class_id)
					->where('asset_cat_using_id', $hold_v->asset_cat_using_id)
					->first();
			
			$tax_percentage = DB::table('zone_tax_asset_cat_percentage')
					->select('*')
					->where('zone_id', $request->input('zone_id'))
					->where('year', $request->input('tax_rate_year'))
					->where('asset_category_id', $hold_v->asset_category_id)
					->first();
					
			$asset_value = ($tax_rate->per_square_feet_rate*1) * ($hold_v->sqr_feet*1) * ($hold_v->asset_qty*1);
			$tax_percent_amt = (($tax_percentage->percentage*1) * ($asset_value*1)) / 100;
			/*
			echo '<pre>';
			print_r($tax_rate);
			echo '</pre>';
			echo '<pre>';
			print_r($asset_value);
			echo '</pre>';
			echo '<pre>';
			print_r($tax_percentage);
			echo '</pre>';
			*/
		//	$hold_v->owner_name = utf8_decode($hold_v->owner_name);
		//	$hold_v->owner_fathers_name = utf8_decode($hold_v->owner_fathers_name);
		//	$hold_v->description = utf8_decode($hold_v->description);
		
			$json_data = response()->json($hold_v, 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
			/*
			echo '<pre>';
			print_r((array) json_decode(json_encode($hold_v)));
			echo '</pre><br>';
			*/
		//	/*
			DB::table('annual_holding_tax_bill_generate')->insert(
				 [
					'zone_id' => $request->input('zone_id'),
					'holding_id' => $hold_v->id,
					'fiscal_year' => $request->input('fyear'),
					'tax_amount' => $tax_percent_amt,
					'detail_info_json' => $json_data->getContent(),
					'datetime' => date('Y-m-d H:i:s'),
				]
			);
		//	*/
		}
		//	exit;
		return redirect()->route('settings.fyear-holding-property-save-and-tax-generate');
    }
	 
	 public function tax_percent_and_tax_rate( $param = [
						'zone_id' => '',
						'anniversary_range_id' => '',
						'asset_category_id' => '',
						'asset_cat_class_id' => '',
						'asset_cat_using_id' => ''
					 ]
	 ){
		
		$zone_id = $param['zone_id'];
		$anniversary_range_id = $param['anniversary_range_id'];
		$asset_category_id = $param['asset_category_id'];
		$asset_cat_class_id = $param['asset_cat_class_id'];
		$asset_cat_using_id = $param['asset_cat_using_id'];
		
		if( $param['zone_id'] != '' && 
				$param['anniversary_range_id'] != '' &&
				$param['asset_category_id'] != '' &&
				$param['asset_cat_class_id'] != '' &&
				$param['asset_cat_using_id'] != '' 
		){
				
		//	echo '<pre>fyear_tax_gener_record<br>'; print_r($fyear_tax_v); echo '</pre>';
			
			$asset_cat_percentage = DB::table('anniversary_asset_cat_percentage')
														->where('zone_id','=', $zone_id)
														->where('anniversary_range_id','=',$anniversary_range_id)
														->where('asset_category_id','=',$asset_category_id)
														->first();
															
			$asset_cat_percent = $asset_cat_percentage->percentage;										
		//	echo '<pre>asset_cat_percent<br>'; print_r($asset_cat_percent); echo '</pre><br>';
							
			$tax_rate = DB::table('zone_tax_rate_settings')
														->where('zone_id','=', $zone_id)
														->where('anniversary_range_id','=',$anniversary_range_id)
														->where('asset_category_id','=',$asset_category_id)
														->where('asset_cat_class_id','=',$asset_cat_class_id)
														->where('asset_cat_using_id','=',$asset_cat_using_id)
														->first();
			$tax_rate->asset_cat_percentage = $asset_cat_percent;
		//	echo '<pre>tax_rate<br>'; print_r($tax_rate); echo '</pre><br><hr>';
			return $tax_rate;
		}else{
			 return false;
		}
	 }
	 
    function fyear_tax_generate_records() {
		
		$data['fyear_tax_gener_records'] = [];
      $fyear_tax_gener_records = DB::table('annual_holding_tax_bill_generate_records')
																->where('done_process','100%')
																->where('zone_id', Auth::user()->permission_id)
																->orderBy('id','desc')
																->get();
		
		//	echo '<pre>'; print_r($fyear_tax_gener_records); echo '</pre><br>';exit;
		
		foreach( $fyear_tax_gener_records as $genrec_k => $genrec_v ){
			$fyear_tax_gener_record = DB::table('annual_holding_tax_bill_generate')
																->where('zone_id', Auth::user()->permission_id)
																->where('fiscal_year', $genrec_v->fiscal_year)
																->select('zone_id', 'holding_id', 'fiscal_year', 'anniversary_range_id', 'asset_category_id', 'asset_category_class_id', 'asset_category_of_using_id', 'square_feet', 'floor_quantity', 'datetime')
																->get();
			//	echo '<pre>'; print_r($fyear_tax_gener_record); echo '</pre><br>';exit;
			/* ================== Each asset =============== */
			
			$tax_percent_amt_total = 0;
			foreach( $fyear_tax_gener_record as $fyear_tax_k => $fyear_tax_v ){
				
				$tax_percent_and_tax_rate = $this->tax_percent_and_tax_rate(
																[
																	'zone_id' 					=> Auth::user()->permission_id,
																	'anniversary_range_id' 	=> $fyear_tax_v->anniversary_range_id,
																	'asset_category_id' 		=> $fyear_tax_v->asset_category_id,
																	'asset_cat_class_id'		=> $fyear_tax_v->asset_category_class_id,
																	'asset_cat_using_id' 	=> $fyear_tax_v->asset_category_of_using_id
																]
															);
				$asset_value = ($tax_percent_and_tax_rate->per_square_feet_rate*1) * ($fyear_tax_v->square_feet*1) * ($fyear_tax_v->floor_quantity*1);
				$tax_percent_amt = (($tax_percent_and_tax_rate->asset_cat_percentage*1) * ($asset_value*1)) / 100;
			//	echo '<pre>'; print_r($asset_value); echo '</pre><br>';
			//	echo '<pre>'; print_r($tax_percent_amt); echo '</pre><hr>';
				$tax_percent_amt_total = $tax_percent_amt_total + $tax_percent_amt;
			}
			$genrec_v->tax_percent_amt_total = $tax_percent_amt_total;
			array_push($data['fyear_tax_gener_records'], $genrec_v);  
		//	exit;
		//	echo '<hr><br>';
		//	echo '<hr><br>';
		}
		//	exit;
		return view('dashboard.settings.fyear-tax-generate-records', $data);
    }
	
	
    function row_script_runn() {
			
			$tax_payments_ssl = DB::table('tax_payments_detail_sslc')->get();
			
			foreach($tax_payments_ssl as $tax_pay){
				DB::table('tax_payments')
                ->where('id', $tax_pay->id)
                ->update(['fiscal_year'=> $tax_pay->fiscal_year]);
					 
				$tax_payments = DB::table('tax_payments')->where('id', $tax_pay->id)->first();
				echo '<pre>'; print_r($tax_payments); echo '</pre><br>';
			}
	 }
	 
	 
	 
}



