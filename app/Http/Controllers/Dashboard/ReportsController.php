<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Dashboard\SettingsController;

class ReportsController extends Controller
{
	
    public function fiscal_year_total_taxamount_data($fyear)
	 {

      $fyear_tax_gener_records = DB::table('annual_holding_tax_bill_generate_records')
																->where('done_process','100%')
																->where('fiscal_year',$fyear)
																->where('zone_id', Auth::user()->permission_id)
																->first();
		$fyear_tax_gener_record = DB::table('annual_holding_tax_bill_generate')
															->where('zone_id', Auth::user()->permission_id)
															->where('fiscal_year', $fyear)
															->select('zone_id', 'holding_id', 'fiscal_year', 'anniversary_range_id', 'asset_category_id', 'asset_category_class_id', 'asset_category_of_using_id', 'square_feet', 'floor_quantity', 'datetime')
															->get();
		//	echo '<pre>'; print_r($fyear_tax_gener_record); echo '</pre><br>';exit;
		/* ================== Each asset =============== */
		
		$tax_percent_amt_total = 0;
		foreach( $fyear_tax_gener_record as $fyear_tax_k => $fyear_tax_v ){
			$SettingsController = new SettingsController;
			$tax_percent_and_tax_rate = $SettingsController->tax_percent_and_tax_rate(
															[
																'zone_id' 					=> Auth::user()->permission_id,
																'anniversary_range_id' 	=> $fyear_tax_v->anniversary_range_id,
																'asset_category_id' 		=> $fyear_tax_v->asset_category_id,
																'asset_cat_class_id'		=> $fyear_tax_v->asset_category_class_id,
																'asset_cat_using_id' 	=> $fyear_tax_v->asset_category_of_using_id
															]
														);
			$asset_value = ($tax_percent_and_tax_rate->per_square_feet_rate*1) * ($fyear_tax_v->square_feet*1) * ($fyear_tax_v->floor_quantity*1);
			$tax_percent_amt = (($tax_percent_and_tax_rate->asset_cat_percentage*1) * ($asset_value*1)) / 100;
		//	echo '<pre>'; print_r($asset_value); echo '</pre><br>';
		//	echo '<pre>'; print_r($tax_percent_amt); echo '</pre><hr>';
			$tax_percent_amt_total += $tax_percent_amt;
		}
		
		$fyear_tax_gener_records->tax_percent_amt_total = $tax_percent_amt_total;
		
		return $fyear_tax_gener_records;
	 }
	 
	 
    public function fiscal_year_payment_detail($fyear)
	 {
		// $tax_payments = DB::table('tax_payments')->where('id', $tax_pay->id)->first();
		 
		//	echo '<pre>'; print_r($this->fiscal_year_total_taxamount($fyear)); echo '</pre><hr>';
		//	exit;
		$fiscal_year_total_taxamount_data = $this->fiscal_year_total_taxamount_data($fyear);
		$data['tax_percent_amt_total'] = $fiscal_year_total_taxamount_data->tax_percent_amt_total;
		 $tax_payments = DB::table('tax_payments')
											->join('holdings', 'tax_payments.holding_id', '=', 'holdings.id')
											->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
											->select('tax_payments.*', 'holdings.holding_no', 'holdings.ward_id', 'wards.ward_number')
											->where('tax_payments.fiscal_year', $fyear)
											->orderBy('payment_datetime', 'desc')
											->get();
		$total_payments_amount = 0;
		foreach ($tax_payments as $tp_k => $tp_v) {
			$total_payments_amount += $tp_v->payment_amount;
		}
		$data['total_payments_amount'] = $total_payments_amount;
		
		$data['fyear'] = $fyear;
		return view('dashboard.reports.fyear-detail', $data);
	 }
	 
	 
    public function dt_ajax_fiscal_year_payment_list($fyear)
	 {
		 $tax_payments = DB::table('tax_payments')
											->join('holdings', 'tax_payments.holding_id', '=', 'holdings.id')
											->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
											->select('tax_payments.*', 'holdings.holding_no', 'holdings.ward_id', 'wards.ward_number')
											->where('tax_payments.fiscal_year', $fyear)
											->orderBy('payment_datetime', 'desc')
											->get();

		$data['data'] = [];
		$rv_data = [];
		$selected_index = '';
		foreach ($tax_payments as $tp_k => $tp_v) {
			//	pre($pro_v);
			
			$tax_payment['sl'] = $tp_k + 1;
			$tax_payment['ward_no'] ='W<b>: ' . $tp_v->ward_number . '</b>';
			$tax_payment['holding_no'] ='H<b>: ' . $tp_v->holding_no . '</b>';
			$tax_payment['payment_amount'] = '<b>' . round($tp_v->payment_amount, 2) . '</b>';
			$tax_payment['payment_datetime'] = $tp_v->payment_datetime;
			$tax_payment['payment_by'] = $tp_v->payment_by;

			array_push($data['data'], $tax_payment);
		}
	
	  //    echo '<pre>';  print_r($data); echo '</pre>';
	  echo json_encode($data);
	 }
}
