<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ward;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;

class WardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['wards'] = Ward::all();
         //  echo json_encode($data['wards']);exit;
        return view('dashboard.settings.wards');
    }

    public function wards_under_zone($zone_id)
    {
			$data['zone'] = DB::table('zones')
								->select('*')
								->where('id', '=', $zone_id)
								->first();
			$data['wards'] = DB::table('wards')
								->join('zones', 'wards.zone_id', '=', 'zones.id')
								->select('wards.*', 'zones.name as zone')
								->where('wards.zone_id', '=', $zone_id)
								->get();
		//	dd($data['wards']);
        return view('dashboard.settings.wards-under-zone', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ward_number' => 'required|max:4|min:1'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'ward_number' => str_replace(' ', '', $request->input('ward_number')),
            'ward_title' => $request->input('ward_title'),
            'google_map_data' => json_encode([
                'lat' => $request->input('latitude'),
                'lon' => $request->input('longitude'),
                'zoom' => $request->input('zoom')
            ]),
            'created_by' => Auth::user()->id,
            'last_updated_by' => date('Y-m-d H')
        ];

        try {
            $ward_id = Ward::create($data)->id;
            session()->flash('success', 'Successfully new ward added <b><a href="' . route('settings.ward.edit', ['id' => $ward_id]) . '">' . $request->input('ward_number') . '</a></b>');
            return redirect()->route('settings.wards');
        } catch (Exception $exception) {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request = null)
    {
        $single_ward = DB::table('wards')->where('ward_id', $id)->first();
      
         //   dd($single_ward);
        $data['the_ward'] = $single_ward;
		  
        return view('dashboard.settings.ward-edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->usertype == 'superadmin') {
            $data = [
					'ward_number' => str_replace(' ', '', $request->input('ward_number')),
					'ward_title' => $request->input('ward_title'),
                'google_map_data' => json_encode([
                    'lat' => $request->input('latitude'),
                    'lon' => $request->input('longitude'),
                    'zoom' => $request->input('zoom')
                ]),
                'created_by' => Auth::user()->id
            ];

            $affected = DB::table('wards')
                ->where('id', $id)
                ->update($data);

            if ($affected) {
                session()->flash('success', 'Successfully Updated');
                session()->flash('action', 'updated');
                return redirect()->route('settings.ward.edit', ['id' => $id]);
            }else{
                session()->flash('info', 'Data not changed, So no need to update :) Thanks');
                session()->flash('action', 'updated');
                return redirect()->route('settings.ward.edit', ['id' => $id]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $single_ward = Ward::find($id);
    //    dd($single_zone->name);
        if (Auth::user()->usertype == 'superadmin') {

        //    $zone = DB::table('zones')->where('id', $id);
            $single_ward->delete();
            session()->flash('success', 'Successfully Removed Zone <b>' . $single_ward->ward_number . '</b>');
            session()->flash('action', 'warning');
            return redirect()->back();
        }
    }


    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters); // TODO: Change the autogenerated stub
    }


    public function dt_ajax_all_wards($id = null)
    {
			$wards = DB::table('wards')
						->join('zones', 'wards.zone_id', '=', 'zones.id')
						->select('wards.*', 'zones.name as zone')
						->get();
         //   echo json_encode($wards);exit;
        //    dd($zones);
        //    echo json_encode($zones);

        $data['data'] = [];
        $rv_data = [];
        $selected_index = '';
        foreach ($wards as $zone_k => $zone_v) {
            //	pre($pro_v);
            $selected_row = '';
            $is_active_class = 'text-body';
            $is_active_lbl = '';
            if ($zone_v->status == 0) {
                $is_active_class = 'text-black-50';
                $is_active_lbl = '<span class="badge badge-secondary">Inactive</span>';
            }elseif($zone_v->status == 1){
                $is_active_lbl = '<span class="badge badge-primary">A</span>';
            }

            if ($zone_v->ward_id == $id) {
                $selected_index = $zone_k;
                $selected_row = 'selected';
            }

            $zone['sl'] = $zone_k + 1;
            $zone['ward_number'] = '<a href="' . route('settings.ward.edit', ['ward_id' => $zone_v->ward_id]) . '" ><b class="font-weight-bold ' . $is_active_class . ' ' . $selected_row . '">' .
                $zone_v->ward_number .
                '</b></a> <small>' . $is_active_lbl . '</small>';
            $zone['ward_title'] = $zone_v->ward_title;
            $zone['zone'] = $zone_v->zone;
            $zone['action'] = '<a href="' . route('settings.ward.edit', ['id' => $zone_v->ward_id]) . '" class="btn btn-sm btn-outline-primary" title="Edit">' .
                '<i class="fa fa-eye" aria-hidden="true"></i> ' .
                '<i class="fa fa-edit" aria-hidden="true"></i>' .
                '</a>' .
                ' ' .
                '<form action="' . route('settings.ward.remove', ['id' => $zone_v->ward_id]) . '" class="" method="POST">' .
                '<input type="hidden" name="_token" value="' . csrf_token() . '" >' .
                '<button type="submit" class="btn btn-sm btn-outline-danger form_ward_remove" name="ward_id" value="' . $zone_v->ward_id . '" title="Delete">' .
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' .
                '</button>' .
                '</form>';

            array_push($data['data'], $zone);
        }
        $data['select'] = $selected_index;

        //    echo '<pre>';  print_r($data); echo '</pre>';
        echo json_encode($data);

    }
}
