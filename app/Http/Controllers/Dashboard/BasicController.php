<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;

class BasicController extends Controller
{
	public function fiscal_years(){
		
		$year_start = 2000;
		$year_end = date('Y');
		$years_arr = [];
		for($years = date('Y'); $years >= $year_start; $years--){
			array_push($years_arr, $years . '-' . ($years +1) );
		}
		return $years_arr;
	}
	
	
	public function get_anniversary_range_by_fyear( $fyear ){
		
		$fYear_ar = explode('-',$fyear);
		$fYear1 = $fYear_ar[0];
		$fYear2 = $fYear_ar[1];

		$ret_anniversary_range['id'] = 0;
		$ret_anniversary_range['title'] = '';
		$anniversary_range = DB::table('anniversary_range')->get();
		foreach( $anniversary_range as $range_k => $range_v ){
			$range_year_from = date('Y', strtotime($range_v->ar_from));
			$range_year_to = date('Y', strtotime($range_v->ar_to));
			
			if($fYear1 >= $range_year_from && $fYear2 <= $range_year_to+1){
				$ret_anniversary_range['id'] = $range_v->ar_id;
				$ret_anniversary_range['title'] = $range_v->ar_title;
			}
		}
		return $ret_anniversary_range;
	}
	
	/* 
	*
	* 	Annual holding tax bill generated records query by holding ID and FYear
	*/
	public function get_holding_fyear_asset_records( $holding_id, $fyear ){
		
		$data = [];
		if( $holding_id != null && $fyear != null ){
			$annual_holding_tax_bill_generated = DB::table('annual_holding_tax_bill_generate')
						->join('asset_category', 'annual_holding_tax_bill_generate.asset_category_id', '=', 'asset_category.asset_category_id')
						->join('asset_category_class', 'annual_holding_tax_bill_generate.asset_category_class_id', '=', 'asset_category_class.asset_cat_class_id')
						->join('asset_category_of_using', 'annual_holding_tax_bill_generate.asset_category_of_using_id', '=', 'asset_category_of_using.asset_cat_using_id')
						->where('annual_holding_tax_bill_generate.holding_id', '=', $holding_id)
						->where('annual_holding_tax_bill_generate.fiscal_year', '=', $fyear)
						->get();
						
			$annual_holding_asset_rec = [];
			foreach( $annual_holding_tax_bill_generated as $annual_holding_asset_rec_k => $annual_holding_asset_rec_v ){
				
				/* ===> For squarefeet_rate <=== */
				$asset_cat_class_rate = DB::table('zone_tax_rate_settings')
													->select('zone_tax_rate_settings.per_square_feet_rate')
													->where('zone_tax_rate_settings.zone_id', '=', $annual_holding_asset_rec_v->zone_id)
													->where('zone_tax_rate_settings.anniversary_range_id', '=', $annual_holding_asset_rec_v->anniversary_range_id)
													->where('zone_tax_rate_settings.asset_category_id', '=', $annual_holding_asset_rec_v->asset_category_id)
													->where('zone_tax_rate_settings.asset_cat_class_id', '=', $annual_holding_asset_rec_v->asset_category_class_id)
													->where('zone_tax_rate_settings.asset_cat_using_id', '=', $annual_holding_asset_rec_v->asset_category_of_using_id)
													->first();
				$annual_holding_asset_rec_v->squarefeet_rate = $asset_cat_class_rate->per_square_feet_rate * 1;	
				
				/* ===> For percentage rate <=== */
				$asset_cat_percentage = DB::table('anniversary_asset_cat_percentage')
													->select('anniversary_asset_cat_percentage.percentage')
													->where('anniversary_asset_cat_percentage.zone_id', '=', $annual_holding_asset_rec_v->zone_id)
													->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', $annual_holding_asset_rec_v->anniversary_range_id)
													->where('anniversary_asset_cat_percentage.asset_category_id', '=', $annual_holding_asset_rec_v->asset_category_id)
													->first();
				$annual_holding_asset_rec_v->percentage_rate = $asset_cat_percentage->percentage;	
				$total_squarefeet = $annual_holding_asset_rec_v->square_feet * $annual_holding_asset_rec_v->floor_quantity;
				$annual_holding_asset_rec_v->total_asset_squarefeet = $total_squarefeet;	
				$annual_holding_asset_rec_v->total_asset_amount = $annual_holding_asset_rec_v->squarefeet_rate * $total_squarefeet;	
				$annual_holding_asset_rec_v->total_percentage_rate_payable = ($annual_holding_asset_rec_v->total_asset_amount * $annual_holding_asset_rec_v->percentage_rate) / 100;	
				
				array_push($annual_holding_asset_rec, $annual_holding_asset_rec_v);
			}
			
			$total_payable_tax = 0;
			foreach( $annual_holding_asset_rec as $asset_records_k => $asset_records_v ){
				$total_payable_tax += $asset_records_v->total_percentage_rate_payable;
			}
			$data['fyear'] = $fyear;
			$data['asset_records'] = $annual_holding_asset_rec;
			$data['fyear_total_tax_amount'] = $total_payable_tax;
				
			$tax_payments = DB::table('tax_payments')
						->where('tax_payments.holding_id', '=', $holding_id)
						->where('tax_payments.fiscal_year', '=', $fyear)
						->orderBy('payment_datetime','desc')
						->get();
			$total_payment = 0;
			foreach( $tax_payments as $payment_rec_k => $payment_rec_v ){
				$total_payment += $payment_rec_v->payment_amount;
			}
			$data['fyear_tax_payment_records'] = $tax_payments;
			$data['fyear_tax_total_payment'] = $total_payment;
			
			$total_payable_tax_divided_100 = $total_payable_tax/100;
			if($total_payable_tax_divided_100 == 0){
				$total_payable_tax_divided_100 = 1;
			}
			
			$data['payment_percentage'] = round($total_payment / $total_payable_tax_divided_100);
		} 
		return $data;
	}
	

    public function holdings_total_due($holding_id, $fyear = null)
    {
		 
		 if( $holding_id ){
			
			$data['status'] = 'success';
		//	$data['anniversary_range'] = $basicCont->get_anniversary_range_by_fyear($fyear);
		//	return json_encode($data); exit;
			
			$fyear_asset_records = [];
			if( $fyear != null ){
				
				array_push($fyear_asset_records, $this->get_holding_fyear_asset_records($holding_id, $fyear));
				
				$data['data_inputs']['fyear'] = $fyear;
			}else{
				
				$annual_holding_tax_bill_generated_y = DB::table('annual_holding_tax_bill_generate')
					->where('annual_holding_tax_bill_generate.holding_id', '=', $holding_id)
					->groupBy('fiscal_year')
					->get();
				
				foreach( $annual_holding_tax_bill_generated_y as $fy_k => $fy_v ){
					array_push($fyear_asset_records, $this->get_holding_fyear_asset_records($holding_id, $fy_v->fiscal_year));
				}
				
				$data['data_inputs']['fyear'] = '';
			}
			
			$data['status'] = 'success';
			$data['data_inputs']['holding_id'] = $holding_id;
			$data['fyear_asset_records'] = $fyear_asset_records;
			
		 }else{
			$data['status'] = 'error';
			$data['data_inputs']['holding_id'] = '';
			$data['data_inputs']['fyear'] = '';
		 }
		 return $data;
    }
	
	
}
	
	
	
	