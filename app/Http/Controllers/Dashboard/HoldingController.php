<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Zone;
use App\Models\Holding;
use Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Validator;

class HoldingController extends Controller
{
    /* *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$data['selected_zone_no'] = '';
			$data['selected_ward_no'] = '';
      //  $data['holgings'] = Holding::all();
         //  echo json_encode($data['wards']);exit;
        return view('dashboard.settings.holdings', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_holding_under_z_w($zid = null, $ward = null)
    {
		$data['zone'] = $zid;
		$data['ward'] = $ward;

		$data['asset_category'] = DB::table('asset_category')
						->join('anniversary_asset_cat_percentage', 'asset_category.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
						->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
						->select('asset_category.*', 'anniversary_asset_cat_percentage.*', 'anniversary_range.*')
						->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
						->get();

		$data['asset_category_class'] = DB::table('asset_category_class')
						->join('anniversary_asset_cat_class_rate', 'asset_category_class.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
						->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
						->select('asset_category_class.*', 'anniversary_asset_cat_class_rate.*', 'anniversary_range.*')
						->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
						->get();
		//dd($data['asset_category_class']);

	//	if( $ward != null ){

			$data['wards'] = DB::table('wards')
						->select('*')
						->where('wards.zone_id', '=', $zid)
						->get();

				//   dd($data['wards']);

	//	}

        return view('dashboard.settings.holding-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			
			$asset_category = $request->input('asset_category');
			foreach ($asset_category  as $key => $asset_cat) {
				$fields['asset_cat'.$key] = $asset_cat;
				$rules['asset_cat'.$key] = 'required|not_in:0';
			}
			
			$asset_cat_class = $request->input('asset_cat_class');
			foreach ($asset_cat_class  as $key => $asset_cat_cls) {
				$fields['asset_cat_class'.$key] = $asset_cat_cls;
				$rules['asset_cat_class'.$key] = 'required|not_in:0';
			}
			
			$asset_cat_using_id = $request->input('asset_cat_using_id');
			foreach ($asset_cat_using_id  as $key => $asset_cat_use_id) {
				$fields['asset_cat_using_id'.$key] = $asset_cat_use_id;
				$rules['asset_cat_using_id'.$key] = 'required|not_in:0';
			}
			
			$sqr_feet = $request->input('sqr_feet');
			foreach ($sqr_feet  as $key => $sqr_feet_v) {
				$fields['sqr_feet'.$key] = $sqr_feet_v;
				$rules['sqr_feet'.$key] = 'required|not_in:0';
			}
			
			$asset_qty = $request->input('asset_qty');
			foreach ($asset_qty  as $key => $asset_qty_v) {
				$fields['asset_qty'.$key] = $asset_qty_v;
				$rules['asset_qty'.$key] = 'required|not_in:0';
			}
			
			$validator = Validator::make($fields, $rules);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator)->withInput();
			}
		//	/*
				echo '<pre>';
				print_r($request->input());
				echo '</pre>';exit;
		//	*/
			
        $validator = Validator::make($request->all(), [
            'ward_id' => 'required',
            'holding_no' => 'required',
            'owner_name' => 'required',
            'asset_category' => 'required',
            'asset_cat_class' => 'required',
            'sqr_feet' => 'required',
            'tax_gtotal_amt' => 'required',
        ]);



        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'ward_id' => trim($request->input('ward_name')),
            'holding_no' => trim($request->input('ward_name')),
            'owner_name' => trim($request->input('owner_name')),
            'owner_fathers_name' => trim($request->input('owner_fathers_name')),
            'phone' => trim($request->input('phone')),
            'email' => trim($request->input('email')),
            'nid' => trim($request->input('nid')),
            'description' => $request->input('description'),
            'google_map_data' => json_encode([
                'lat' => $request->input('latitude'),
                'lon' => $request->input('longitude'),
                'zoom' => $request->input('zoom')
            ]),
            'created_by' => Auth::user()->id,
            'last_updated_by' => date('Y-m-d H')
        ];
		//  dd($data);

        try {
				DB::table('holdings')->insert( $data );
				
            session()->flash('success', 'Successfully new ward added <b><a href="' . route('settings.ward.edit', ['id' => $ward_id]) . '">' . $request->input('ward_name') . '</a></b>');
            return redirect()->route('settings.wards');
        } catch (Exception $exception) {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function per_square_feet_ret( $data = [
        'anniversary_range_id' => 0,
        'zone_id' => 0,
        'asset_category_id' => 0,
        'asset_cat_class_id' => 0,
        'asset_cat_using_id' => 0
    ] ){
	//	 return $data;exit;
		$data_ret = false;
		
		$zone_tax_rate_settings = DB::table('zone_tax_rate_settings')
            ->select(
                'zone_tax_rate_settings.*',
                'asset_category.asset_category_title',
                'asset_category.asset_category_slug',
                'asset_category_class.asset_cat_class_title',
                'asset_category_class.asset_cat_class_slug',
                'asset_category_of_using.asset_cat_using_title',
                'asset_category_of_using.asset_cat_using_slug',
                )
            ->join('zones', 'zone_tax_rate_settings.zone_id', '=', 'zones.id')
            ->join('asset_category', 'zone_tax_rate_settings.asset_category_id', '=', 'asset_category.asset_category_id')
            ->join('asset_category_class', 'zone_tax_rate_settings.asset_cat_class_id', '=', 'asset_category_class.asset_cat_class_id')
            ->join('asset_category_of_using', 'zone_tax_rate_settings.asset_cat_using_id', '=', 'asset_category_of_using.asset_cat_using_id')
            ->where('zone_tax_rate_settings.anniversary_range_id', '=', $data['anniversary_range_id'])
            ->where('zone_tax_rate_settings.zone_id', '=', $data['zone_id'])
            ->where('zone_tax_rate_settings.asset_category_id', '=', $data['asset_category_id'])
            ->where('zone_tax_rate_settings.asset_cat_class_id', '=', $data['asset_cat_class_id'])
            ->where('zone_tax_rate_settings.asset_cat_using_id', '=', $data['asset_cat_using_id'])
            ->first();
		//    dd($data);
		if( $zone_tax_rate_settings ){
			$data_ret = $zone_tax_rate_settings;
			
			$data_ret->tax_percentage = 0;
			$tax_percentage = DB::table('anniversary_asset_cat_percentage')
												->where('zone_id', '=', $data['zone_id'])
												->where('anniversary_range_id', '=', $data['anniversary_range_id'])
												->where('asset_category_id', '=', $data['asset_category_id'])
												->first();
			
			if( $tax_percentage ){
				$data_ret->tax_percentage = $tax_percentage->percentage;
			}
		}
      
		
      return $data_ret;
    }
		
    function ajax_per_square_feet_ret( Request $request ){
        $ret = $this->per_square_feet_ret(
            [
                'anniversary_range_id' => $request->input('anniversary_range_id'),
                'zone_id' => $request->input('zone_id'),
                'asset_category_id' => $request->input('asset_category_id'),
                'asset_cat_class_id' => $request->input('asset_cat_class_id'),
                'asset_cat_using_id' => $request->input('asset_cat_using_id')
            ]
        );
   //     echo json_encode($ret); exit;
        if( $ret ){
            $data['status'] = true;
            $data['data'] = $ret;
        }else{
            $data['status'] = false;
        }
        echo json_encode($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function edit($id)
    {
        $data['zones'] = Zone::all()->sortByDesc("id");

        $data['last_tax_rate_updated_year'] = DB::table('zone_tax_rate_settings')
            ->select('year')
            ->orderBy('year','desc')
            ->groupBy('year')
            ->get();

        $data['asset_category'] = DB::table('asset_category')
            ->join('anniversary_asset_cat_percentage', 'asset_category.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
            ->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
            ->select('asset_category.*', 'anniversary_asset_cat_percentage.*', 'anniversary_range.*')
            ->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
            ->get();

        $data['asset_category_class'] = DB::table('asset_category_class')
            ->join('anniversary_asset_cat_class_rate', 'asset_category_class.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
            ->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
            ->select('asset_category_class.*', 'anniversary_asset_cat_class_rate.*', 'anniversary_range.*')
            ->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
            ->get();

        $data['asset_category_of_using'] = DB::table('asset_category_of_using')
            ->select('*')
            ->get();

        $data['holding'] = DB::table('holdings')
            ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
            ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
            ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select(
                'holdings.*',
                'wards.ward_name',
                'zones.name as zone_name',
                'zones.id as zone_id',
                'ward_areas.area_name',
                'roads.road_name'
            )
            ->where('holdings.id', '=', $id)
            ->first();


        $data['roads'] = DB::table('roads')
            ->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select('roads.*', 'wards.ward_name as ward', 'zones.name as zone')
            ->where('wards.ward_id', '=', $data['holding']->ward_id)
            ->get();
        //	dd($data['roads']);
        $data['areas'] = DB::table('ward_areas')
            ->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select('ward_areas.*', 'wards.ward_name as ward', 'zones.name as zone')
            ->where('ward_areas.ward_id', '=', $data['holding']->ward_id)
            ->get();
        //	dd($data['areas']);
        //	$data['zone'] = $data['holding']->;
        //	$data['ward'] = $ward;


        //   dd($single_ward);

        return view('dashboard.settings.holding-edit', $data);
    }
    */
    public function edit($id)
    {
        $data['holding'] = $holding = DB::table('holdings')
            ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
         //   ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
            ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select(
                'holdings.*',
                'wards.ward_number',
                'zones.name as zone_name',
                'zones.id as zone_id',
                'ward_areas.area_name',
             //   'roads.road_name'
            )
            ->where('holdings.id', '=', $id)
            ->first();
				
        $data['anniversary_range'] = $anniversary_range = DB::table('anniversary_range')->orderBy('ar_id','desc')->get();
        $data['holding_assets'] = DB::table('holding_assets')
            ->where('holding_id', '=', $id)
            ->get();
	//	dd($holding);
		$data['zones'] = Zone::all()->sortByDesc("id");
		
        $data['last_tax_rate_updated_year'] = DB::table('zone_tax_rate_settings')
            ->select('year')
            ->orderBy('year','desc')
            ->groupBy('year')
            ->get();
			
        $last_year_tax_percentage_set = DB::table('zone_tax_asset_cat_percentage')
            ->select('year')
            ->orderBy('year','desc')
            ->groupBy('year')
            ->first();
				
		if( isset($_GET['anniversary_range']) && $_GET['anniversary_range'] < $anniversary_range[0]->ar_id ){
			$anniversary_range_id = $_GET['anniversary_range'];
		}else{
			$anniversary_range_id = $anniversary_range[0]->ar_id;
		}
		// dd($anniversary_range_id);
		
		$data['asset_category'] = DB::table('asset_category')
											->join('anniversary_asset_cat_percentage','asset_category.asset_category_id','=', 'anniversary_asset_cat_percentage.asset_category_id')
											->where('anniversary_asset_cat_percentage.zone_id', '=', $holding->zone_id)
											->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', $anniversary_range_id)
											->get();
											
      //  dd($data['asset_category']);
		$data['asset_category_class'] = DB::table('asset_category_class')->get();

        $data['asset_category_of_using'] = DB::table('asset_category_of_using')
            ->select('*')
            ->get();

        $last_year_tax_rate_settings = DB::table('zone_tax_rate_settings')
            ->select('year')
            ->orderBy('year','desc')
            ->groupBy('year')
            ->first();

        $data['per_square_feet_ret'] = $this->per_square_feet_ret(
                                            [
                                                'anniversary_range_id' => $anniversary_range_id,
                                                'zone_id' => $holding->zone_id,
                                                'asset_category_id' => $holding->asset_category_id,
                                                'asset_cat_class_id' => $holding->asset_cat_class_id,
                                                'asset_cat_using_id' => $holding->asset_cat_using_id
                                            ]
                                       );
														
																
													
      //  dd($data['per_square_feet_ret']);
		$data['roads'] = DB::table('roads')
					->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
					->join('zones', 'wards.zone_id', '=', 'zones.id')
					->select('roads.*', 'wards.ward_number as ward', 'zones.name as zone')
					->where('wards.ward_id', '=', $data['holding']->ward_id)
					->get();
	//	dd($data['roads']);
		$data['areas'] = DB::table('ward_areas')
					->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
					->join('zones', 'wards.zone_id', '=', 'zones.id')
					->select('ward_areas.*', 'wards.ward_number as ward', 'zones.name as zone')
					->where('ward_areas.ward_id', '=', $data['holding']->ward_id)
					->get();
	//	dd($data['areas']);
	//	$data['zone'] = $data['holding']->;
	//	$data['ward'] = $ward;


         //   dd($single_ward);

        return view('dashboard.settings.holding-edit', $data);
    }

    public function holdings_under_zone($id)
    {
        $data['selected_zone_no'] = $id;

        return view('dashboard.settings.holdings', $data);
    }

    public function holdings_under_zone_ward($zid, $wid = null)
    {
			$data['selected_zone_no'] = $zid;
			$data['selected_ward_no'] = '';
			if( $wid != null ){
				$data['selected_ward_no'] = $wid;
			}

        return view('dashboard.settings.holdings', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$asset_category = $request->input('asset_category');
			foreach ($asset_category  as $key => $asset_cat) {
				$fields['asset_cat'.$key] = $asset_cat;
				$rules['asset_cat'.$key] = 'required|not_in:0';
			}
			
			$asset_cat_class = $request->input('asset_cat_class');
			foreach ($asset_cat_class  as $key => $asset_cat_cls) {
				$fields['asset_cat_class'.$key] = $asset_cat_cls;
				$rules['asset_cat_class'.$key] = 'required|not_in:0';
			}
			
			$asset_cat_using_id = $request->input('asset_cat_using_id');
			foreach ($asset_cat_using_id  as $key => $asset_cat_use_id) {
				$fields['asset_cat_using_id'.$key] = $asset_cat_use_id;
				$rules['asset_cat_using_id'.$key] = 'required|not_in:0';
			}
			
			$sqr_feet = $request->input('sqr_feet');
			foreach ($sqr_feet  as $key => $sqr_feet_v) {
				$fields['sqr_feet'.$key] = $sqr_feet_v;
				$rules['sqr_feet'.$key] = 'required|not_in:0';
			}
			
			$asset_qty = $request->input('asset_qty');
			foreach ($asset_qty  as $key => $asset_qty_v) {
				$fields['asset_qty'.$key] = $asset_qty_v;
				$rules['asset_qty'.$key] = 'required|not_in:0';
			}
			
			$validator = Validator::make($fields, $rules);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator)->withInput();
			}
			/*
				echo '<pre>';
				print_r($request->input());
				echo '</pre>';exit;
			*/
			
        $validator = Validator::make($request->all(), [
            'ward_id' => 'required',
            'holding_no' => 'required',
            'owner_name' => 'required',
            'asset_category' => 'required',
            'asset_cat_class' => 'required',
            'sqr_feet' => 'required',
            'tax_gtotal_amt' => 'required',
        ]);



        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'ward_id' => trim($request->input('ward_id')),
            'area_id' => trim($request->input('area_id')),
            'road_id' => trim($request->input('road_id')),
            'holding_no' => trim($request->input('holding_no')),
            'owner_name' => trim($request->input('owner_name')),
            'owner_fathers_name' => trim($request->input('owner_fathers_name')),
            'phone' => trim($request->input('phone')),
            'email' => trim($request->input('email')),
            'nid' => trim($request->input('nid')),
            'description' => $request->input('description'),
            'google_map_data' => json_encode([
                'lat' => $request->input('latitude'),
                'lon' => $request->input('longitude'),
                'zoom' => $request->input('zoom')
            ])
        ];
		//  dd($data);

        try {
				DB::table('holdings')->where('id', $id)->update( $data );
								
				DB::table('holding_assets')
                ->where('holding_id', $id)
                ->update(['status'=>0]);
					
				foreach( $request->input('asset_category') as $asset_category_k => $asset_category_v ){
					
					$asset_data = [
						'holding_id' => $id,
						'asset_category_id' => $asset_category_v,
						'asset_cat_class_id' => $request->input('asset_cat_class')[$asset_category_k],
						'asset_cat_using_id' => $request->input('asset_cat_using_id')[$asset_category_k],
						'square_feet' => $request->input('sqr_feet')[$asset_category_k],
						'quantity' => $request->input('asset_qty')[$asset_category_k],
						'datetime' => date('Y-m-d H:i:s'),
						'status' => 1,
					];
					DB::table('holding_assets')->insert( $asset_data );
				}
				
				DB::table('holding_assets')
                ->where('holding_id', $id)
                ->where('status', 0)
                ->delete();
					
            session()->flash('success', 'Successfully Updated</b>');
            return redirect()->route('settings.holding.edit', ['id'=>$id]);
        } catch (Exception $exception) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $single_ward = Ward::find($id);
    //    dd($single_zone->name);
        if (Auth::user()->usertype == 'superadmin') {

        //    $zone = DB::table('zones')->where('id', $id);
            $single_ward->delete();
            session()->flash('success', 'Successfully Removed Zone <b>' . $single_ward->ward_name . '</b>');
            session()->flash('action', 'warning');
            return redirect()->back();
        }
    }


    public function __call($method, $parameters)
    {
        parent::__call($method, $parameters); // TODO: Change the autogenerated stub
    }


    public function dt_ajax_all_holdings($zid = null, $wid = null)
    {
			if( $zid != null && $wid == null ){

				$holdings = DB::table('holdings')
							->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
							->join('roads', 'holdings.road_id', '=', 'roads.road_id')
							->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
							->join('zones', 'wards.zone_id', '=', 'zones.id')
							->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
							->where('wards.zone_id', '=', $zid)
							->get();
			}else if( $zid != null && $wid != null ){

				$holdings = DB::table('holdings')
							->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
							->join('roads', 'holdings.road_id', '=', 'roads.road_id')
							->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
							->join('zones', 'wards.zone_id', '=', 'zones.id')
							->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
							->where('zones.name', '=', $zid)
							->where('wards.ward_number', '=', $wid * 1)
							->get();
			}else{
				$holdings = DB::table('holdings')
							->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
							->join('roads', 'holdings.road_id', '=', 'roads.road_id')
							->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
							->join('zones', 'wards.zone_id', '=', 'zones.id')
							->select('holdings.*', 'wards.ward_number as ward', 'zones.name as zone', 'ward_areas.area_name as area', 'roads.road_name as road_name')
							->get();
			}
         //   echo json_encode($wards);exit;
			//    dd($holdings);
			//    echo json_encode($zones);

        $data['data'] = [];
        $rv_data = [];
        $selected_index = '';
        foreach ($holdings as $zone_k => $holding_v) {
            //	echo json_encode($holding_v);
            $selected_row = '';
            $is_active_class = 'text-body';
            $is_active_lbl = '';

            $zone['sl'] = $zone_k + 1;
            $zone['holding_no'] = '<a href="' . route('settings.holding.edit', ['holding_id' => $holding_v->id]) . '" ><b class="font-weight-bold ' . $is_active_class . ' ' . $selected_row . '">' .
                $holding_v->holding_no .
                '</b></a> <small>' . $is_active_lbl . '</small><br>' .
                '<small>Area: <b>'. $holding_v->area .'</b> | Road: <b>'. $holding_v->road_name .'</b></small>';
            $zone['owner_name'] = $holding_v->owner_name;
            $zone['ward_zone'] = 'W:<b>'.$holding_v->ward . "</b> &nbsp; | &nbsp; Z:<b>" . $holding_v->zone . "</b>";
            $zone['action'] = '<a href="' . route('settings.holding.edit', ['id' => $holding_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Edit">' .
                '<i class="fa fa-eye" aria-hidden="true"></i> &nbsp; ' .
                '<i class="fa fa-edit" aria-hidden="true"></i>' .
                '</a> <a href="' . route('settings.holding.tax-pay', ['id' => $holding_v->id]) . '" class="btn btn-sm btn-outline-primary" title="Pay">' .
                'Pay' .
                '</a>';

            array_push($data['data'], $zone);
        }

        $data['select'] = $selected_index;

        //    echo '<pre>';  print_r($data); echo '</pre>';
        echo json_encode($data);
    }


    public function holding_tax_pay($id = null,$action=null) {

		$data['zones'] = Zone::all()->sortByDesc("id");
		
		$data['asset_category'] = DB::table('asset_category')
						->join('anniversary_asset_cat_percentage', 'asset_category.asset_category_id', '=', 'anniversary_asset_cat_percentage.asset_category_id')
						->join('anniversary_range', 'anniversary_asset_cat_percentage.anniversary_range_id', '=', 'anniversary_range.ar_id')
						->select('asset_category.*', 'anniversary_asset_cat_percentage.*', 'anniversary_range.*')
						->where('anniversary_asset_cat_percentage.anniversary_range_id', '=', 3)
						->get();

		$data['asset_category_class'] = DB::table('asset_category_class')
						->join('anniversary_asset_cat_class_rate', 'asset_category_class.asset_cat_class_id', '=', 'anniversary_asset_cat_class_rate.asset_cat_class_id')
						->join('anniversary_range', 'anniversary_asset_cat_class_rate.anniversary_range_id', '=', 'anniversary_range.ar_id')
						->select('asset_category_class.*', 'anniversary_asset_cat_class_rate.*', 'anniversary_range.*')
						->where('anniversary_asset_cat_class_rate.anniversary_range_id', '=', 3)
						->get();

		$data['holding'] = DB::table('holdings')
					->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
					->join('roads', 'holdings.road_id', '=', 'roads.road_id')
					->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
					->join('zones', 'wards.zone_id', '=', 'zones.id')
					->select(
								'holdings.*',
								'wards.ward_name',
								'zones.name as zone_name',
								'zones.id as zone_id',
								'ward_areas.area_name',
								'roads.road_name'
								)
					->where('holdings.id', '=', $id)
					->first();


		$data['roads'] = DB::table('roads')
					->join('wards', 'roads.ward_id', '=', 'wards.ward_id')
					->join('zones', 'wards.zone_id', '=', 'zones.id')
					->select('roads.*', 'wards.ward_name as ward', 'zones.name as zone')
					->where('wards.ward_id', '=', $data['holding']->ward_id)
					->get();
	//	dd($data['roads']);
		$data['areas'] = DB::table('ward_areas')
					->join('wards', 'ward_areas.ward_id', '=', 'wards.ward_id')
					->join('zones', 'wards.zone_id', '=', 'zones.id')
					->select('ward_areas.*', 'wards.ward_name as ward', 'zones.name as zone')
					->where('ward_areas.ward_id', '=', $data['holding']->ward_id)
					->get();
	//	dd($data['areas']);
	//	$data['zone'] = $data['holding']->;
	//	$data['ward'] = $ward;


         //   dd($single_ward);

        return view('dashboard.settings.holding-tax-pay', $data);
    }

    public function holding_payment_history($id = null) {

        $data['holding'] = $holding = DB::table('holdings')
            ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
            ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
            ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select(
                'holdings.*',
                'wards.ward_name',
                'zones.name as zone_name',
                'zones.id as zone_id',
                'ward_areas.area_name',
                'roads.road_name'
            )
            ->where('holdings.id', '=', $id)
            ->first();


        $data['payment_history'] = [];

        $fiscal_years = DB::table('tax_payments')
            ->select('*')
            ->where('tax_payments.holding_id', '=', $id)
            ->orderBy('tax_payments.fiscal_year', 'desc')
            ->groupBy('tax_payments.fiscal_year')
            ->get();

        //	prex($fiscal_years);
        foreach( $fiscal_years as $fiscal_year ){
            $data['payment_history'][$fiscal_year->fiscal_year] = DB::table('tax_payments')
                ->select('*')
                ->join('holdings', 'tax_payments.holding_id','=','holdings.id')
                ->where('tax_payments.holding_id', '=', $id)
                ->where('tax_payments.fiscal_year', '=', $fiscal_year->fiscal_year)
                ->orderBy('tax_payments.payment_datetime', 'desc')
                ->get();
        }


        return view('dashboard.settings.holding-payment-history', $data);
    }

    public function holding_tax_history($id = null) {

        $data['holding'] = $holding = DB::table('holdings')
            ->join('ward_areas', 'holdings.area_id', '=', 'ward_areas.area_id')
            ->join('roads', 'holdings.road_id', '=', 'roads.road_id')
            ->join('wards', 'holdings.ward_id', '=', 'wards.ward_id')
            ->join('zones', 'wards.zone_id', '=', 'zones.id')
            ->select(
                'holdings.*',
                'wards.ward_name',
                'zones.name as zone_name',
                'zones.id as zone_id',
                'ward_areas.area_name',
                'roads.road_name'
            )
            ->where('holdings.id', '=', $id)
            ->first();


        $data['payment_history'] = [];

        $fiscal_years = DB::table('tax_payments')
            ->select('*')
            ->where('tax_payments.holding_id', '=', $id)
            ->orderBy('tax_payments.fiscal_year', 'desc')
            ->groupBy('tax_payments.fiscal_year')
            ->get();

        //	prex($fiscal_years);
        foreach( $fiscal_years as $fiscal_year ){
            $data['payment_history'][$fiscal_year->fiscal_year] = DB::table('tax_payments')
                ->select('*')
                ->join('holdings', 'tax_payments.holding_id','=','holdings.id')
                ->where('tax_payments.holding_id', '=', $id)
                ->where('tax_payments.fiscal_year', '=', $fiscal_year->fiscal_year)
                ->orderBy('tax_payments.payment_datetime', 'desc')
                ->get();
        }


        return view('dashboard.settings.holding-payment-history', $data);
    }


}
