<?php
function pre($data){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}
function prex($data){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
	exit;
}


class BngConv {
    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    
    public static function bn2en($number) {
        return str_replace(self::$bn, self::$en, $number);
    }
    
    public static function en2bn($number) {
        return str_replace(self::$en, self::$bn, $number);
    }
}	/* ====== Example using ====== 
	echo BngConv::bn2en("This is ২০১৬");
	echo BngConv::en2bn("42 আমার প্রিয় সংখ্যা");
====== Example using ====== */


