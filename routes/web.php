<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'Front\FrontHoldingController@index')->name('homepage');
Route::get('/zone/{zid}/wards', 'Front\FrontHoldingController@wards_under_zone')->name('wards.under.zone');
Route::get('/zone/{zid}/{wid}', 'Front\FrontHoldingController@holding_search')->name('holdings.under.zone-ward');

Route::get('/holding/{id}/details', 'Front\FrontHoldingController@holding_details')->name('holding.details');
Route::get('/holding/{holding_id}/tax-checkout', 'Front\FrontHoldingController@holding_tax_checkout')->name('holding.tax-checkout');
Route::post('/holding/tax-payment-success', 'Front\FrontHoldingController@holding_taxpay_success')->name('holding.taxpay-success');

Route::get('/holding/{id}/pay/{amount}/{fiscal_year}', 'Front\FrontHoldingController@holding_tax_pay')->name('to.pay');

//	Route::get('/dt-ajax-all-holdings', 'Front\FrontController@dt_ajax_all_holdings')->name('holdings.dt-ajax-all-holdings');
//	Route::get('/dt-ajax-all-holdings/{id}', 'Front\FrontController@dt_ajax_all_holdings')->name('holdings.dt-ajax-all-holdings');
//	Route::get('/dt-ajax-all-holdings/{zid}/{wid}', 'Front\FrontController@dt_ajax_all_holdings')->name('holdings.dt-ajax-all-holdings');


// SSLCOMMERZ Start
Route::get('/holding-tax-checkout/{holding_id}/{fyear}', 'SslCommerzPaymentController@holding_tax_checkout');
Route::get('/example2', 'SslCommerzPaymentController@exampleHostedCheckout');

Route::post('/pay', 'SslCommerzPaymentController@index');
Route::post('/pay-via-ajax', 'SslCommerzPaymentController@payViaAjax');

Route::post('/success', 'SslCommerzPaymentController@success');
Route::post('/fail', 'SslCommerzPaymentController@fail');
Route::post('/cancel', 'SslCommerzPaymentController@cancel');

Route::post('/ipn', 'SslCommerzPaymentController@ipn');
//SSLCOMMERZ END

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



/* ===> Zone <=== */
Route::get('/settings/zones', 'Dashboard\ZoneController@index')->name('settings.zones');
Route::get('/settings/dt-ajax-all-zones', 'Dashboard\ZoneController@dt_ajax_all_zones')->name('settings.zones.dt-ajax-all-zones');
Route::get('/settings/dt-ajax-zone-select/{id}', 'Dashboard\ZoneController@dt_ajax_all_zones')->name('settings.zones.dt-ajax-zone-select');
Route::post('settings/zones/post', 'Dashboard\ZoneController@store')->name('settings.zones.post');

Route::get('/settings/zone/{id}/edit', 'Dashboard\ZoneController@edit')->name('settings.zone.edit');

Route::post('/settings/zone/{id}/update', 'Dashboard\ZoneController@update')->name('settings.zone.update');
Route::post('/settings/zone/{id}/remove', 'Dashboard\ZoneController@destroy')->name('settings.zone.remove');
Route::get('/settings/zone/{zid}/wards', 'Dashboard\ZoneController@zone_wards')->name('settings.wards.under-zone');

Route::get('dashboard/tax-collection', 'Dashboard\ZoneController@tax_collection')->name('dashboard.tax-collection');
Route::post('reports/tax-collection-post', 'Dashboard\ZoneController@tax_collection_post')->name('reports.tax-collection-post');
Route::get('dashboard/tax-collection-detail/{holding_id}/{fyear?}', 'Dashboard\ZoneController@tax_collection_detail')->name('dashboard.tax-collection-detail');


Route::post('/ajax/words-area', 'Dashboard\AjaxController@ajax_words_area')->name('ajax.words-area');
Route::post('/ajax/holdings-load-by-ward-areas', 'Dashboard\AjaxController@holdings_load_by_ward_areas')->name('ajax.holdings-load-by-ward-areas');




/* ===> Ward <=== */
Route::get('/settings/wards', 'Dashboard\WardController@index')->name('settings.wards');
Route::get('/settings/dt-ajax-all-wards', 'Dashboard\WardController@dt_ajax_all_wards')->name('settings.wards.dt-ajax-all-wards');
Route::get('/settings/dt-ajax-ward-select/{id}', 'Dashboard\WardController@dt_ajax_all_wards')->name('settings.wards.dt-ajax-ward-select');
Route::post('settings/wards/post', 'Dashboard\WardController@store')->name('settings.wards.post');

Route::get('/settings/ward/{id}/edit', 'Dashboard\WardController@edit')->name('settings.ward.edit');
Route::post('/settings/ward/{id}/update', 'Dashboard\WardController@update')->name('settings.ward.update');
Route::post('/settings/ward/{id}/remove', 'Dashboard\WardController@destroy')->name('settings.ward.remove');

/* ===> Areas <=== */
Route::get('/settings/areas', 'Dashboard\WardAreaController@index')->name('settings.areas');
Route::get('/settings/dt-ajax-all-areas', 'Dashboard\WardAreaController@dt_ajax_all_areas')->name('settings.areas.dt-ajax-all-areas');
Route::get('/settings/dt-ajax-area-select/{id}', 'Dashboard\WardAreaController@dt_ajax_all_areas')->name('settings.areas.dt-ajax-area-select');
Route::post('settings/areas/post', 'Dashboard\WardAreaController@store')->name('settings.areas.post');

Route::get('/settings/area/{id}/edit', 'Dashboard\WardAreaController@edit')->name('settings.area.edit');
Route::post('/settings/area/{id}/update', 'Dashboard\WardAreaController@update')->name('settings.area.update');
Route::post('/settings/area/{id}/remove', 'Dashboard\WardAreaController@destroy')->name('settings.area.remove');

/* ===> Roads <=== */
Route::get('/settings/roads', 'Dashboard\RoadController@index')->name('settings.roads');
Route::get('/settings/dt-ajax-all-roads', 'Dashboard\RoadController@dt_ajax_all_roads')->name('settings.roads.dt-ajax-all-roads');
Route::get('/settings/dt-ajax-road-select/{id}', 'Dashboard\RoadController@dt_ajax_all_roads')->name('settings.roads.dt-ajax-road-select');
Route::post('settings/roads/post', 'Dashboard\RoadController@store')->name('settings.roads.post');

Route::get('/settings/road/{id}/edit', 'Dashboard\RoadController@edit')->name('settings.road.edit');
Route::post('/settings/road/{id}/update', 'Dashboard\RoadController@update')->name('settings.road.update');
Route::post('/settings/road/{id}/remove', 'Dashboard\RoadController@destroy')->name('settings.road.remove');

/* ===> Holdings <=== */
Route::get('/settings/holdings', 'Dashboard\HoldingController@index')->name('settings.holdings');
Route::get('/settings/dt-ajax-all-holdings', 'Dashboard\HoldingController@dt_ajax_all_holdings')->name('settings.holdings.dt-ajax-all-holdings');
Route::get('/settings/dt-ajax-all-holdings/{id}', 'Dashboard\HoldingController@dt_ajax_all_holdings')->name('settings.holdings.dt-ajax-all-holdings');
Route::get('/settings/dt-ajax-all-holdings/{zid}/{wid}', 'Dashboard\HoldingController@dt_ajax_all_holdings')->name('settings.holdings.dt-ajax-all-holdings');
Route::post('settings/holdings/store', 'Dashboard\HoldingController@store')->name('settings.holdings.addnew');
Route::post('/settings/zone/ajax_per_square_feet_ret', 'Dashboard\HoldingController@ajax_per_square_feet_ret')->name('settings.zone.ajax-per-square-feet-ret');

Route::get('/settings/holdings/z/{id}/', 'Dashboard\HoldingController@holdings_under_zone_ward')->name('settings.holdings.under.zone');
Route::get('/settings/holdings/z/{zid}/w/{wid}', 'Dashboard\HoldingController@holdings_under_zone_ward')->name('settings.holding.under.zone-ward');
Route::get('/settings/zone/{zid}/ward/{wid}', 'Dashboard\HoldingController@holdings_under_zone_ward')->name('settings.holdings.under-zone-ward');
Route::get('/settings/holdings/add-new-holding/{zid?}/{ward?}', 'Dashboard\HoldingController@create_holding_under_z_w')->name('settings.holding.add.under.z.w');

Route::get('/settings/holding/{id}/edit', 'Dashboard\HoldingController@edit')->name('settings.holding.edit');
Route::get('/settings/holding/{id}/tax-pay', 'Dashboard\HoldingController@holding_tax_pay')->name('settings.holding.tax-pay');
Route::get('/settings/holding/{id}/payment-history', 'Dashboard\HoldingController@holding_payment_history')->name('settings.holding.payment-history');
Route::post('/settings/holding/{id}/update', 'Dashboard\HoldingController@update')->name('settings.holding.update');
Route::post('/settings/holding/{id}/remove', 'Dashboard\HoldingController@destroy')->name('settings.holding.remove');
Route::get('/dashboard/holding/{id}/tax-history', 'Dashboard\HoldingController@holding_tax_history')->name('dashboard.holding.tax-history');
Route::get('/dashboard/fyear', 'Dashboard\HoldingController@holding_tax_history')->name('dashboard.holding.tax-history');


/* ===> anniversary_tax_rate <=== */
Route::get('/settings/anniversary-tax-rate', 'Dashboard\SettingsController@anniversary_tax_rate_settings')->name('settings.anniversary-tax-rate');
Route::get('/settings/anniversary-tax-rate/{action}/{id}', 'Dashboard\SettingsController@anniversary_tax_rate_settings_edit')->name('settings.anniversary-tax-rate-edit');

Route::post('/settings/anniversary-tax-rate/cat-class-rate/{id}/update', 'Dashboard\SettingsController@anniversary_tax_cat_rate_update')->name('settings.anniversary-tax-rate-edit.cat-class-rate.update');
Route::post('/settings/anniversary-tax-rate/category-percentage/{id}/update', 'Dashboard\SettingsController@anniversary_tax_cat_percentage_update')->name('settings.anniversary-tax-rate-edit.category-percentage.update');

Route::get('/settings/fyear-holding-property-save-and-tax-generate/{fyear?}', 'Dashboard\SettingsController@fyear_holding_property_save_and_tax_generate')->name('settings.fyear-holding-property-save-and-tax-generate');
Route::post('/settings/fyear-holding-property-save-and-tax-generate-ready-req', 'Dashboard\SettingsController@fyear_holding_property_save_and_tax_gen_ready_req')->name('settings.fyear-holding-property-save-and-tax-gen-ready-req');
Route::post('/settings/fyear-holding-property-save-and-tax-generate-ready-confirm', 'Dashboard\SettingsController@fyear_holding_property_save_and_tax_gen_ready_confirm')->name('settings.fyear-holding-property-save-and-tax-gen-ready-confirm');
Route::get('/settings/fyear-holding-property-save-and-tax-generate-ready/{fyear}', 'Dashboard\SettingsController@fyear_holding_property_save_and_tax_gen_ready')->name('settings.fyear-holding-property-save-and-tax-gen-ready');
Route::get('/settings/fyear-tax-generate-records/{fyear?}', 'Dashboard\SettingsController@fyear_tax_generate_records')->name('settings.fyear-tax-generate-records');

/* ===> User Settings <=== */
Route::get('/settings/users', 'Dashboard\EmployeesController@index')->name('settings.users');
/* ===> Employee <=== */
Route::get('/settings/employees', 'Dashboard\EmployeesController@index')->name('settings.employees');
Route::get('/settings/dt-ajax-all-employees', 'Dashboard\EmployeesController@dt_ajax_all_employees')->name('settings.employees.dt-ajax-all-employees');
Route::get('/settings/dt-ajax-employee-select/{id}', 'Dashboard\EmployeesController@dt_ajax_all_employees')->name('settings.employees.dt-ajax-employee-select');
Route::get('settings/user/add', 'Dashboard\EmployeesController@create')->name('settings.employee.add');
Route::post('settings/user/add/post', 'Dashboard\EmployeesController@store')->name('settings.employee.add.post');

Route::get('settings/user/{id}/edit', 'Dashboard\EmployeesController@edit')->name('settings.user.edit');
Route::post('settings/user/{id}/edit/post', 'Dashboard\EmployeesController@update')->name('settings.user.edit.post');

Route::post('settings/user/remove', 'Dashboard\EmployeesController@store')->name('settings.user.remove');

/*
Route::get('/settings/employee/{id}/edit', 'Dashboard\ZoneController@edit')->name('settings.employee.edit');
Route::post('/settings/employee/{id}/update', 'Dashboard\ZoneController@update')->name('settings.employee.update');
Route::post('/settings/employee/{id}/remove', 'Dashboard\ZoneController@destroy')->name('settings.employee.remove');
*/

Route::get('reports/fiscal-year/{fyear}', 'Dashboard\ReportsController@fiscal_year_payment_detail')->name('reports.fiscal-year');
Route::get('reports/dt_ajax_fiscal_year_payment_list/{fyear}', 'Dashboard\ReportsController@dt_ajax_fiscal_year_payment_list')->name('reports.dt_ajax_fiscal_year_payment_list');

Route::get('reports/fiscal-year/{fyear}/user-wise', 'Dashboard\ReportsController@fiscal_year_payment_detail_user_wise')->name('reports.fiscal-year-user-wise');

Route::post('reports/holdings_total_due', 'Dashboard\AjaxController@ajax_holdings_total_due')->name('reports.holdings_total_due');


Route::get('settings/row-script-runn', 'Dashboard\SettingsController@row_script_runn');


//Route::post('/ajax/holdings-load-by-ward-areas', 'Dashboard\AjaxController@ajax_holdings_total_due')->name('ajax.holdings-load-by-ward-areas');



